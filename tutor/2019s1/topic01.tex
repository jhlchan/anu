\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

\title{Assignment Topic: Mathematical Induction}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Pattern}
A proof by mathematical induction is all about \textbf{pattern}.
The induction pattern can be distilled from $B2$ slides $6, 9, 14$:
\begin{table}[h]
\begin{tabular}{ll}
(To prove):  & (a formula involing $n$)\\
\textbf{Basis Step}: & (prove the case $n = 0$, or $1$, or $\dots$)\\
\textbf{Induction Step}: & \textbf{Assume formula is true for some fixed $n$, then}\\
            & (prove the case $n+1$) \\
(Conclude): & \textbf{By mathematical induction, the formula is true for all $n$}.\\
\end{tabular}
\end{table}

\noindent
You must include all the words in bold for a complete proof --- don't be lazy!
In the slides, the conclusion appears as the declaration at the start.

\section{Example}
$B2$ slide $32$ asks you to verify this by induction:\\
\textbf{Claim}: $\forall{r} \in \mathbb{N}\;\; T_{r} = r 2^{r}$ where
\[
\begin{cases}
      T_{1} = 2\\
      T_{r} = 2^{r} + 2 T_{r-1}\; \forall r \in \mathbb{N}\setminus\{ 1 \}
    \end{cases}
\]
\paragraph{Proof}
By induction on $r$.\\
Basis Step: when $r = 1$,
\[
\begin{array}{lll}
  & T_{1}\\
= & 2  & \mbox{by definition}\\
= & 1\times{2^{1}} & \mbox{by arithmetic}\\  
\end{array}
\]
The formula is true for $r = 1$.\\
Induction Step: assume the formula is true for some fixed $r \in \mathbb{N}$. Then
\[
\begin{array}{lll}
  & T_{r+1}\\
= & 2^{r+1} + 2 T_{r} & \mbox{by definition}\\
= & 2^{r+1} + 2 (r 2^{r}) & \mbox{by induction assumption}\\
= & 2^{r+1} + r 2^{r+1} & \mbox{by arithmetic}\\  
= & (r+1) 2^{r+1} & \mbox{by arithmetic}\\  
\end{array}
\]
Thus the formula is true for $r + 1$.\\
By mathematical induction, the formula is true for all $r \in \mathbb{N}$.
$\Box$

\section{Reasoning}
What have we proved? Let $P(r)$ be the following predicate (a sentence with a variable that becomes a statement when the variable has value, $A1$ slide $45$):
\[
P(r) = (\forall{r} \in \mathbb{N}\;\; T_{r} = r 2^{r})
\]
\begin{itemize}
\item Basis step proves $P(1)$ is true.
\item Induction step proves this implication: $P(r) \Rightarrow P(r+1)$  is true (recall \emph{assume} $\dots$ \emph{then} $\dots$). Neither $P(r)$ nor $P(r+1)$ is proved, but this gives the bridge to go from $r$ to its next: \textbf{if} $P(r)$ is true, \textbf{then} $P(r+1)$ is true.
\item Conclusion (which you should write every time, like a citation) means:
\begin{itemize}
\item since $P(1)$ is true, $P(2)$ is true by implication,
\item since $P(2)$ is true, $P(3)$ is true by implication,
\item $\dots$
\item I can go on forever, but you get the idea.
\end{itemize}
Without the conclusion, you are missing an important piece of reasoning!
\end{itemize}
All these fit into the pattern outlined in $B2$ slide $5$.
There are many ways to write the conclusion (or use a declaration as in the slides).
The shortest (but I don't recommend) is just say, ``This completes the (mathematical) induction.''

\section{Basis}
You need to read carefully to determine the starting value for the basis (not \emph{basic}) step.
In the previous example, the definition starts with $T_{1}$ (hence the ugly $ \mathbb{N}\setminus\{ 1 \}$ for $T_{r}$, which is correct).
However, for Friday Assignment $4, Q2$, the definition is given as:
\[
\begin{cases}
      T_{0} = 0\\
      T_{r} = 2^{r} + 2 T_{r-1}\; \forall r \in \mathbb{N}
    \end{cases}
\]
Using the same pattern for induction, you should be able to assert, in this case:
\begin{itemize}
\item the formula $T_{r} = r 2^{r}$ is to be true for all $r \in \mathbb{N}^{*} = \mathbb{N} \cup \{ 0 \}$.
\item the basis step starts with $r = 0$.
\item the starting line of the induction step involves the definition.
\item the inductive assumption makes use of the formula to go from $r$ to $r+1$.
\end{itemize}

\section{In Exam}
Suppose we are in an exam, and we meet Friday Assignment $4, Q3(a)$.
\begin{itemize}
\item You read \emph{binomial coefficients} then \emph{Pascal's identity}. You panic.
\item I read \emph{using mathetmaical induction on $n$}. I know the pattern. I'll try.
\end{itemize}
It's an exam. I can't Google \emph{binomial} nor \emph{Pascal}. I don't know them, but I know I'm going to use induction on $n$. 
$Q3(a)$ wants me to prove: 
\[
\displaystyle{\forall n, r \in \mathbb{N}^{*}\; \sum_{m=0}^{n}{m\choose{r}} = {{n + 1}\choose{r + 1}}}
\]
Too many distracting symbols, I want to concentrate just on $n$, the induction variable. So I define:
\[
\displaystyle{f(n) = \sum_{m=0}^{n}{m\choose{r}}, \qquad g(n) = {{n + 1}\choose{r + 1}}}
\]
I recall the pattern:
\begin{table}[h]
\begin{tabular}{ll}
(To prove): & $\forall{n} \in \{ 0, \dots \}\; f(n) = g(n)$\\
\textbf{Basis Step}: & prove $f(0) = g(0)$\\
\textbf{Induction Step}: & \textbf{Assume $f(n) = g(n)$ for some fixed $n$, then}\\
            & prove $f(n+1) = g(n+1)$ \\
(Conclude): & \textbf{By mathematical induction, this is true for all $n \in \mathbb{N}^{*}$}.\\
\end{tabular}
\end{table}

\noindent
One way to prove an equality is to go from LHS to RHS, so I start to scribble:
Basis Step: when $n = 0$,
\[
\displaystyle{f(0) = \sum_{m=0}^{0}{m\choose{r}} = {0\choose{r}} =\; ???}
\]
Oops! Another way to prove an equality is to go from RHS to LHS, so I try:
\[
\displaystyle{g(0) = {{0 + 1}\choose{r + 1}} = {1\choose{r+1}} =\; ???}
\]
Get stuck again. Time is not on my side. I move on.\\
Induction Step: assuming the formula for $n \in \mathbb{N}^{*}$, then
\[
\begin{array}{lll}
  & f(n+1)\\
= & \displaystyle{\sum_{m=0}^{n+1}{m\choose{r}}} \\
= & \displaystyle{\sum_{m=0}^{n}{m\choose{r}} + {{n+1}\choose{r}}} & \mbox{by splitting sum (Worksheet $4, Q3, Q4$)}\\ 
= & \displaystyle{{{n+1}\choose{r+1}} + {{n+1}\choose{r}}} & \mbox{by inductive assumption (that is, $f(n) = g(n)$)}\\ 
= & \displaystyle{{{(n+1) + 1}\choose{r+1}}} & \mbox{by Pascal's identity (replacing both $n$ and $r$ by next)}\\
= & g(n+1) & \mbox{Yay!}\\
\end{array}
\]
That's clever and easy. I recite the conclusion.
But I haven't worked out the starting point: the basis step.
Does $\displaystyle{{0\choose{r}} = {1\choose{r+1}}}$ ?
The $(r+1)$ bugs me, I've only seen $(r-1)$ in Pascal's identity:
\[
\displaystyle{{{n}\choose{r-1}} + {{n}\choose{r}} = {{n+1}\choose{r}}}
\]
But hey, if I replace $(r-1)$ by $r$, then the original $r$ will be $(r+1)$ (this I've already done in the induction step above):
\[
\displaystyle{{{n}\choose{r}} + {{n}\choose{r+1}} = {{n+1}\choose{r+1}}}
\]
With $n = 0$ this is:
$\displaystyle{{{0}\choose{r}} + {{0}\choose{r+1}} = {{0+1}\choose{r+1}}}$,
and $\displaystyle{{0\choose{r+1}} = 0}$ by $(r+1) > 0$. Voli{\`a}!

Of course, you can gather all these information and write up a decent, clean proof by induction of $Q3(a)$. You don't need to use $f(n)$ or $g(n)$ explicitly. After a while, they are in your head.

We all get stuck at math. The point is to move on, especially in an exam. If you cannot work out the basis case, you'll lose $1$ mark. Better to get more marks elsewhere.

\section{Be Smart}
There are different ways to tackle when you get stuck.
The little trick mentioned in the basis step of $Q3(a)$ actually came from one of you.
Others worked out that $r$ can be zero or nonzero.
If $r = 0$, both binomials (they are not \emph{fractions}) are $1$.
If $r > 0$, both binomials are $0$. Hence they are always equal.

Now we meet $Q3(b)$ in the exam: to prove another formula by induction on $k$.
Try the same induction pattern: always go \emph{via} definition from LHS of formula to RHS.
This time though, you'll find the basis step pretty easy, but probably get stuck at the induction step.
Work out a way around to solve the problem.
Be sure to recite the conclusion before you're done.

\section{Notes}
A few words about the $B2$ slides $6, 9, 14$.
The induction step says ``Assume the formula is true for \emph{up to and including} some fixed $n$''. This is more general and will be applicable for advanced problems involving complete induction.

Given a formula, you may be able to prove it by mathemtical induction, based on a bunch of definitions. Without the formula, just given the definitions, you'll go nowhere.

So how does the formula come about? Later, when you learn more math, you may be able to discover (or derive) the formula by some advanced method.

At this stage, you can only make an educated guess. Some of these formula can be subtle (like Friday Assignment $4, Q2(f)$).
Nevertheless, once it is given, don't be afraid of trying the pattern of induction to prove the formula. 

\end{document}

% pdflatex topic01.tex