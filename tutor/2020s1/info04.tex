\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
\usetikzlibrary{matrix} % for matrix nodes
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
% \usepackage{ifthen} % has \ifthenelse, but PGF Math Error: unknown o or of
\usepackage{todonotes} % has \ifthenelse !

% special marks
\usepackage{amssymb} % http://ctan.org/pkg/amssymb, for \because and \therefore
\usepackage{pifont}% http://ctan.org/pkg/pifont, for \ding
\newcommand{\cmark}{\ding{51}}% check tick
\newcommand{\xmark}{\ding{55}}% cross X

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

\title{Counting}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

% the text cross-mark
\newcommand{\txmark}{\text{\xmark}} % opposite of \checkmark, after begin{document}


Did you enjoy the movie \emph{The Man Who Knew Infinity (2015)} ?
You actually know something about infinity from the lectures,
especially Worksheet 5 Q3: counting all fractions, the set $\mathbb{Q}$!

\section{Bijection}
Let's start from the beginning: counting by fingers is sweet (C2 slide 7).
A set $S$ is \textbf{countable} when you can find a bijection between $S$
and $\mathbb{N} = \{1,2,3,\dots\}$, the natural numbers. Examples are:
\begin{itemize}
\item $\mathbb{N}$ is countable: the bijection is $i$, the identity function (A1 slide 19), and its inverse is itself, $i^{-1} = i$.
\item $2\mathbb{N}$, the doubles (similar to C2 slide 9), is countable.
The bijection is $\phi: \mathbb{N} \rightarrow 2\mathbb{N}$ with $\phi(n) = 2n$,
and its inverse $\phi^{-1}(m) = m/2$.
\item $S = \{n^{2}, n \in \mathbb{N}\}$, the squares, is countable.
The bijection is $\psi: \mathbb{N} \rightarrow S$ with $\psi(n) = n^{2}$,
and its inverse $\psi^{-1}(s) = \sqrt{s}$.
\item $\mathbb{Z}$, the integers, is countable (C1 slide 9).
The bijection is $f: \mathbb{N} \rightarrow \mathbb{Z}$ with:
\begin{equation*} % use * to remove equation number
    f(n) =
    \begin{cases}
      (n-1)/2   & \text{if odd } n\\
      -n/2      & \text{if even } n\\
    \end{cases}
\end{equation*}
\label{q:ex-1}%
\textbf{Exercise 1}. Can you specify the inverse $f^{-1}$?
% See C1 slide 9, example 2, but a twist.
\end{itemize}

\section{Ordering}
When a set $S$ has a bijection with $\mathbb{N}$,
the elements of $S$ form a queue, so that you can count along the queue.
The queue for the squares $S$ is obvious:
\[
\begin{array}{lclclclclclc}
            & \psi(1) &  & \psi(2) & & \psi(3) & & \psi(4) & & \psi(5) & \\
S: & 1 & \rightarrow & 4 & \rightarrow & 9 & \rightarrow & 16 & \rightarrow & 25 & \rightarrow & \dots\\
\end{array}
\]
as the squares are ordered by the usual less-than ($<$).
However, the queue for the integers $\mathbb{Z}$ is less obvious:
\[
\begin{array}{lclclclclclc}
            & f(1) &  & f(2) & & f(3) & & f(4) & & f(5) & \\
\mathbb{Z}: & 0 & \rightarrow & -1 & \rightarrow & 1 & \rightarrow & -2 & \rightarrow & 2 & \rightarrow & \dots\\
\end{array}
\]
What is the ordering?
Recall the absolute value $|z|$ of an integer $z \in \mathbb{Z}$,
You can see that the queue is ordered by absolute values,
and within integers of the same absolute value, the positive is before the negative.
We can invent a new ordering, say $\triangleleft$-order, defined by:
\begin{equation*} % use * to remove equation number
    z_{1} \triangleleft z_{2} \Leftrightarrow
    \begin{cases}
      |z_{1}| < |z_{2}|          & \text{or}\\
      |z_{1}| = |z_{2}| \text{ and } z_{1} < z_{2}\\
    \end{cases}
\end{equation*}
Thus a \emph{well-ordered} set (elements in a queue) is countable (C1 slide 10).

The quotients $\mathbb{Q}$, also called fractions or rationals, can be put into a queue,
by one of your classmates (based on the question of Worksheet 5 Q3):
\begin{enumerate}[label=(\alph*)] 
\item Let $q \in \mathbb{Q}$, reduce $q = \frac{a}{b}$ in lowest terms, with $a \in \mathtt{Z}$, and $b \in \mathbb{N}$ (that is, the sign is only in the numerator, leave the denominator always positive).
\item define $\displaystyle{\mathtt{h}\left(\frac{a}{b}\right) = |a| + b}$, the height of the fraction.
\item order fractions by height, and for equal heights, compare their signed numerators.
\end{enumerate}
This gives the following ordering:
\begin{equation*} % use * to remove equation number
    \frac{a_{1}}{b_{1}} \ll \frac{a_{2}}{b_{2}} \Leftrightarrow
    \begin{cases}
      a_{1} = 0       & \text{or}\\
      |a_{1}| + b_{1} < |a_{2}| + b_{1}  & \text{or}\\
      |a_{1}| + b_{1} = |a_{2}| + b_{1} \text{ and } a_{1} < a_{2}  & \text{otherwise}\\
    \end{cases}
\end{equation*}
and define: $g: \mathbb{N} \rightarrow \mathbb{Q}, g(n) = q_{n}$ by $\ll$-ordering.\\
\label{q:ex-2}%
\textbf{Exercise 2}. Construct the queue of fractions by $\ll$-ordering.
% 0 -> -1/1 -> 1/1 -> -2/1 -> -1/2 -> 1/2 -> 2/1 <- ...

\bigskip
\noindent
This $\ll$-ordering can be marked on a 2D-grid as:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale, no effect due to matrix sep.
\matrix(m)[matrix of math nodes,column sep=8pt,row sep=8pt]{
%    s_{11} & s_{12} & s_{13} & s_{14} & \cdots \\
       3/1  & 3/2   &  \txmark &   3/4    & \cdots \\
%    s_{21} & s_{22} & s_{23} & s_{24} & \cdots \\
       2/1  & \txmark &  2/3 &  \txmark   & \cdots \\
%    s_{31} & s_{32} & s_{33} & s_{34} & \cdots \\
       1/1  & 1/2     &  1/3    & 1/4     & \cdots \\
%    s_{41} & s_{42} & s_{43} & s_{44} & \cdots \\
       0    & \txmark & \txmark & \txmark & \cdots \\
%    s_{51} & s_{52} & s_{53} & s_{44} & \cdots \\
       -1/1 & -1/2    & -1/3 &    -1/4    & \cdots \\
%    s_{61} & s_{62} & s_{63} & s_{64} & \cdots \\
       -2/1 & \txmark & -2/3    & \txmark & \cdots \\
%    s_{71} & s_{72} & s_{73} & s_{74} & \cdots \\
       -3/1 &  -3/2   & \txmark & -3/4    & \cdots \\
};

\draw[->, thick]
         (m-4-1)edge(m-5-1)
         (m-5-1)edge[bend right](m-3-1)
         (m-3-1)edge[bend right](m-6-1)
         (m-6-1)edge[bend right](m-5-2)
         (m-5-2)edge(m-3-2)
         (m-3-2)edge[bend right](m-2-1)
         (m-2-1)edge[bend right](m-7-1)
         (m-7-1)edge(m-5-3)
         (m-5-3)edge(m-3-3)
         (m-3-3)edge(m-1-1);
\end{tikzpicture}

\noindent
This shows that the quotients $\mathbb{Q}$ are countable.
However, the real numbers $\mathbb{R}$ (with nonrecurring decimals) is uncountable (C1 slide 11). The proof by Cantor was very clever, but not for our MATH1005/MATH6005 exam.

\bigskip
Finite sets are always countable. 
For finite sets $S \subset T$, there cannot be a bijection between them,
as $S$ is a proper subset of $T$, so that $|S| < |T|$.

However, when infinity comes into play, intuition is thrown to the wind.
It is a characteristic of infinite sets that
a bijection can exist between $S$ and $T$, even though $S \subset T$.
When this happens, the sets $S$ and $T$ have the same cardinality.

% History of counting using bijection
% Feature of infinite set: N → 2N, N → N², even N → NxN by Cantor diagonal trace.
% Which sets are countable?
% C1 slide 11: Georg Cantor: ‘Small’ and ‘Big’ infinities (Non-assessable for MATH1005)

\section*{A Puzzle}
Arrange $6$ coins in a cross shape, see the picture below.\\
Can you move a single coin to obtain two lines of $4$ coins each?

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
    disc/.style={circle,inner sep=0pt,minimum size=20pt,draw,fill=pink!30}] 
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 5);
\node at (0,5.5) {}; % dummy
% center
\node[disc] (C) at (6,3) {};
\node[disc, left of = C] (A) {};
\node[disc, right of = C] (E) {};
\node[disc, above of = C] (B) {};
\node[disc, below of = C] (D) {};
\node[disc, below of = D] (F) {};
\end{tikzpicture}


\section{Arrangements}
% Number of combinations:
% Pascal triangle
Let's go back to consider a queue: now the check-out queue with $n$ people:
\begin{itemize}
\item There are $n!$ arrangements for the queue (C1 slide 21).
\item If the queue is cut to length $k \le n$, the number of $k$-queues from $n$ people is $P(n,k) = \displaystyle\frac{n!}{(n-k)!}$ (C1 slide 22).
\item If a sample of size $k \le n$ is chosen, the number of $k$-sets from $n$ people is $C(n,k) = \displaystyle{{n \choose k} = \frac{n!}{k!(n-k)!}}$ (C1 slide 23). 
\end{itemize}
Permutation for ordered queues, combination for unordered sets.
So far so good, but this order/unordered business can get messy.
\begin{itemize}
\item People in the queue are either male $\mathtt{M}$ or female $\mathtt{F}$.
Of the $n$ people in the queue, let $k$ of them be female.
The queue thus form a word, like this: $\mathtt{FMFFM}$, for $n = 5, k = 3$.
How many such words can be formed?
Answer: $\displaystyle{{n \choose k} = \frac{n!}{k!(n-k)!}}$  (C1 slide 26).
\item You decide to put color labels for the customers in the queue. There are $x$ red labels, $y$ blue labels, $z$ green labels, all fit into the queue with $n$ people, so $n = x + y + z$. How many color words can be formed?
Answer: $\displaystyle{\frac{n!}{x!y!z!}}$  (C1 slide 26).
Obviously, this can be generalised.
\item Imagine the queue consists of all your favourite pop-stars, $n$ of them. You want to serve them drinks, so you get $m$ bartenders. You don't want to distinguish your stars, and the bartenders look the same in uniform. In how many ways can the `bars' mingle with the `stars' along the queue?
Answer: $\displaystyle{C(n+m,m) = \frac{(n + m)!}{n!m!}}$  (C1 slide 27).
\end{itemize}
\label{q:ex-3}%
\textbf{Exercise 3}. In the last example, suppose $m = 2$. In how many ways can the `bars' mingle with the `stars' with at least one star between bars?
% Treat bar-and-bar as a big bar, there are C(n+1,1) to be removed,
% In general, count = C(n+2,2) - C(n+1,1) = C(n+1,2) by Pascal's Identity.
% That is, treat one bar-star as a new-bar, one less star, 2 bars, so C((n-1)+2,2) = C(n+1,2).
% For n = 3, C(4,2) = 4x3/2 = 6
% For n = 5, C(6,2) = 6x5/2 = 15
% 3 stars, 2 bars, C(3+2,2) = C(5,2) = 5x4/2 = 10. 4 cases of 2-bars together
% Treat bar-and-bar as a big bar, there are C(3+1,1) = C(4,1) = 4 to be removed,
% Hence count = C(5,2) - C(4,1) = 10 - 4 = 6.

\section{Pascal's Triangle}
The combinations are binomial coefficients (C1 slide 24), because:
\begin{itemize}
\item During the polynomial expansion of $(x + y)^{n}$,
each term has the form $x^{k}y^{n-k}$, where $k = 0, 1, \dots, n$.
\item Such a term corresponds to a word ($xxyyx$ for $n = 5, k = 3$),
and there are $\displaystyle{{n \choose k}}$ such words.
\item Terms of the same form are collected, its coefficient is the number of such words. 
\end{itemize}
In \emph{The Man Who Knew Infinity}, Ramanujan said, ``Math is like a painting, with colors you cannot see.'' We look for patterns, the following you are familiar:
\[
\arraycolsep=5.2pt\def\arraystretch{1.5}
\begin{array}{l@{\;=\;}c}
(x + y)^{0} & 1\\
(x + y)^{1} & x + y\\
(x + y)^{2} & x^{2} + 2xy + y^{2}\\
(x + y)^{3} & x^{3} + 3x^{2}y + 3xy^{2} + y^{3}\\
(x + y)^{4} & x^{4} + 4x^{3}y + 6x^{2}y^{2} + 4xy^{3} + y^{4}\\
(x + y)^{5} & x^{5} + 5x^{4}y + 10x^{3}y^{2} + 10x^{2}y^{3} + 5xy^{4} + y^{5}\\
\end{array}
\]
Indeed, this pattern is known from antiquity (count bars for numerals):

\begin{center}
\includegraphics[scale=1.0]{YangHuiTriangle.jpg}
\end{center}

% Binomial Theorem in ancient India
% https://insa.nic.in/writereaddata/UpLoadedFiles/IJHS/Vol01_1_8_AKBag.pdf

\noindent
\label{q:ex-4}%
\textbf{Exercise 4}. Express as factorials, $\displaystyle{{n \choose k} = {n \choose {n  - k}}}$ (C1 slide 25). We can also show this by counting. Let $|S| = n$, and define two sets of its subsets as follows:
\[
\begin{array}{ll}
A = \{t \subseteq S, |t| = k\} & \text{where }k \le n\\
B = \{t \subseteq S, |t| = n - k \}\\
\end{array}
\]
Then by definition, $|A| = C(n, k)$,
and $|B| = C(n, n - k)$. Can you give a bijection $\varphi$ between $A$ and $B$ to show that $C(n, k) = C(n, n - k)$ ?
% Let phi(t) = S \ t. This is a bijection with phi^-1 = phi. Compare with the proof in C1 slide 25.

\bigskip
\noindent
\label{q:ex-5}%
\textbf{Exercise 5}. Prove Pascal's identity (C1 slide 25),
\[
{n \choose k} = {{n - 1} \choose k} + {{n - 1} \choose {k - 1}}
\]
using definition $\displaystyle{{n \choose k} = \frac{n!}{k!(n-k)!}}$ (C1 slide 24).\\
In particular, $\displaystyle{{n \choose k} = 0}$ whenever $k > n$,
and $\displaystyle{{n \choose 0} = {n \choose n} = 1}$ for all $n \in \mathbb{N}^{*}$.

\bigskip
\noindent
This identity $C(n,k) = C(n-1,k) + C(n-1,k-1)$ is the construction rule for the Pascal's triangle, as $C(n-1,\star)$ is above $C(n,\star)$, and $C(\star,k), C(\star,k-1)$ are neighbours. Given $C(1,1) = 1 = C(n,0) = C(n,n)$, we have:
\[
\begin{array}{r@{\;=\;}l}
C(2,1) = [\text{up}]C(1,1) + [\text{left}]C(1,0) & 1 + 1 = 2\\
C(3,1) = [\text{up}]C(2,1) + [\text{left}]C(2,0) & 2 + 1 = 3\\
C(3,2) = [\text{up}]C(2,2) + [\text{left}]C(2,1) & 1 + 2 = 3\\
\end{array}
\]
This \emph{sum of parents} rule can be visually shown:

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
    disc/.style={circle,inner sep=0pt,minimum size=20pt,draw}] 
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 6);
% \node at (0,3.5) {}; % dummy
% center
\node[disc] (C00) at (6,5) {1};
\node[disc, below of = C00, fill=pink!30] (C10) {1};
\node[disc, right of = C10, fill=pink!30] (C11) {1};
\node[disc, below of = C10] (C20) {1};
\node[disc, right of = C20, fill=pink!30] (C21) {2};
\node[disc, right of = C21] (C22) {1};
\node[disc, below of = C20] (C30) {1};
\node[disc, right of = C30] (C31) {3};
\node[disc, right of = C31] (C32) {3};
\node[disc, right of = C32] (C33) {1};
\draw[-,thick] (C10) -- (C11);
\draw[->,thick] (C11) -- (C21);
\end{tikzpicture}

\noindent
The binomial expression is usually expressed as:
$\displaystyle{(1 + x)^{n} = \sum_{k=0}^{n} C(n,k)x^{k}}$.
From this we can derive:
\begin{itemize}
\item Put $x = 1$, then the row sum of the Pascal's triangle is:
$\displaystyle{\sum_{k=0}^{n} C(n,k) = 2^{n}}$.
\item Put $x = -1$, then the alternating row sum of the Pascal's triangle is:
$\displaystyle{\sum_{k=0}^{n} (-1)^{k}C(n,k) = 0}$.
\end{itemize}
Verify both these sums in the Pascal's triangle by working out the row sum on the right,
and the alternating row sum on the left.\\
\label{q:ex-6}%
\textbf{Exercise 6}. Can you figure out this sum \quad 
$\displaystyle{\sum_{k=0}^{n} \frac{C(n,k)}{2^{k}}}$ ?
% Put x = 1/2, so the sum is (1+1/2)^n = (3/2)^n.
% Wolfram Alpha: sum of C(n,k)/2^k from 0 to n.

\section{PINs}
% Number of multisets (stars and bars):
% Use Prep07Sols.pdf, QSet C1, Q8.
% A PIN is a number with 4 decimal digits.
% For 3 digits: 0, 1, 2, there are only 3^3 = 27 3-PINs, or 3^4 = 81 PINs.
% Reuse worksheet 05?
Let us rework QSet C1, Q8: a PIN is a number with $4$ decimals digits.
If we simplify this to binary-PINs, a number with $4$ bits,
we can verify all calculations, since there are only $2^{4} = 16$ binary-PINs:
\[
\begin{array}{l}
\mathtt{0000\;0001\;0010\;0011\;0100\;0101\;0110\;0111}\\
\mathtt{1000\;1001\;1010\;1011\;1100\;1101\;1110\;1111}\\
\end{array}
\]

\noindent
\emph{How many binary-PINs contain at least one $0$?}\\
The no-$0$ is $\mathtt{1111}$, count by $1^{4} = 1$.
The required count is: $2^{4} - 1^{4} = 15$.

\bigskip
\noindent
\emph{How many binary-PINs contain at least one even and at least one odd?}\\
The no-even is $\mathtt{1111}$, the no-odd is $\mathtt{0000}$,
hence the count is: $2^{4} - 2(1^{4}) = 14$.

\bigskip
\noindent
\emph{How many binary-PINs start or end with $0$ or both?}\\
First count those starts with $0$: $1\times{2^{3}} = 8$, the top eight.
Then count those ends with $0$: $2^{3}\times{1} = 8$, the even binary-PINs.
Starting and ending with $0$ has: $1\times{2^{2}}\times{1} = 4$,
which are $\mathtt{0000\;0010\;0100\;0110}$.
Inclusion-exclusion gives $2^{3} + 2^{3} - 2^{2} = 8 + 8 - 4 = 12$.

On the other hand, we can apply complementary counting,
which excludes these: $\mathtt{1001\;1011\;1101\;1111}$,
count by $1\times{2^{2}}\times{1} = 4$.
Hence the required count is $2^{4} - 2^{2} = 12$.\\
\label{q:ex-7}%
\textbf{Exercise 7}. Use complementary counting to find how many decimal PINs start or end with $0$ or both.
% For decimal PINs, 10^4 - 9x10^2x9 = 10^2(100 - 81) = 1900

\bigskip
\noindent
\emph{How many binary-PINs have their digits in nowhere-decreasing order?}\\
Typical exampes are: $\mathtt{0011\;0111}$, how to count them?
This is hard, because you need to use encoding:
\begin{itemize}
\item Treat the bits as symbols (not numbers),
and encode $\mathtt{0111}$ as $\mathbb{\fbox{$\mathtt{0}$}}^{1}\mathbb{\fbox{$\mathtt{1}$}}^{3}$.
\item Use stars-in-the-sky for exponents: $\mathtt{0}^{\star}\mathtt{1}^{\star\star\star}$.
\item Put stars in boxes:
$\overbrace{\fbox{$\star$}}^{\mathtt{0}}\overbrace{\fbox{$\star\star\star$}}^{\mathtt{1}}$.
\item Think stars-and-bars: ${\star} \big\bracevert {\star\star\star}$.
\end{itemize}
Therefore, $\mathtt{0111}$ encodes as ${\star} \big\bracevert {\star\star\star}$,
and ${\star\star} \big\bracevert {\star\star}$ decodes to $\mathtt{0011}$ --
see, another bijection!
To count binary-PINs in non-decreasing order is to count the codes of stars-and-bars.
Using $2 - 1 = 1$ bar and $4$ stars, the count is $C(4+1,4) = C(5,4) = C(5,1) = 5$,
which are $\mathtt{0000\;0001\;0011\;0111\;1111}$.

\bigskip
\noindent
\emph{How many binary-PINs have digit sum $2$?}\\
For binary bits, there is only one way to form any digit sum.
With more digits, this is actually a problem about breaking down the sum.
Let us think like this. Let the $4$ bits be $\mathtt{abcd}$,
then we need $\mathtt{a+b+c+d = 2}$, where $\mathtt{a, b, c, d}$ can be zero or non-zero.
For bits, typical solutions are $\mathtt{1010\;0110}$. How to count them?
This time, we put boxes around the unknown, and encode the value of the digit as stars: $1 = \star, 2 = \star\star$, \emph{etc.}
Thus $\mathtt{1010\;0110}$ becomes
$\fbox{$\star$}\fbox{\color{white}{$\star$}}\fbox{$\star$}\fbox{\color{white}{$\star$}}$ and $\fbox{\color{white}{$\star$}}\fbox{$\star$}\fbox{$\star$}\fbox{\color{white}{$\star$}}$.
Using stars-and-bars, we
encode $\mathtt{0110}$ as $\big\bracevert {\star} \big\bracevert {\star} \big\bracevert$,
and decode ${\star} \big\bracevert\big\bracevert {\star} \big\bracevert$  as $\mathtt{1010}$ --- another bijection.
There are $C(3+2,2) = C(5,2) = 10$ such stars-and-bars,
but some have two stars together (like $\big\bracevert\big\bracevert {\star} {\star} \big\bracevert$) which represents an invalid bit.
Treating this as a `big-star' gives $C(3+1,1) = C(4,1) = 4$.
Thus the required count is $10 - 4 = 6$,
which are $\mathtt{0011\;0101\;0110\;1001\;1010\;1100}$.

\bigskip
\noindent
\emph{How many binary-PINs involve only two different digits?}\\
% C(10,2)×(2^4 −2), my case: C(2,2)x(2^4 - 2) = 1x14, exlude 0000, 1111. yes!
For binary bits, this is easy to see. Let's think more generally, say $d$ digits.
There are $C(d,2)$ choices for the two digits.
Each choice gives $2^{4}$ PINs (recall the truth-table of $4$ symbols),
$2$ of which use only one digit (usually the first and last row of truth-table).
Hence the final count is $C(d,2)(2^{4} - 2) = 14C(d,2)$.
Putting $d = 2$ for binary-PINs, the count is $14$,
excluding $\mathtt{0000\;1111}$.

\bigskip
\noindent
\emph{How many (different) PINs involve only two different digits, each used twice?}
% C(10,2)× C(4,2), my case: C(2,2)xC(4,2) = 1x6, only 0011, 0101, 0110, 1001, 1010, 1100
Again, let's think more generally, say $d$ digits.
There are $C(d,2)$ choices for the two digits, say $x$ and $y$.
Since each is used twice in $4$ positions, each choice gives $C(4,2)$ such PINs.
Hence the final count is $C(d,2)C(4,2) = 6C(d,2)$.
Putting $d = 2$ for binary-PINs, the count is $6$,
only $\mathtt{0011\;0101\;0110\;1001\;1010\;1100}$.

\bigskip
\noindent
Compare these counts of binary-PINs with those of decimal-PINs given in the solution (Prep07Sols.pdf). The method is more important than the solution. When you don't know how to count, simplify the problem appropriately, so that you can count and verify. Then you'll be confident with your counting techniques.

\section{Tips}
Some observations from experience:
\begin{itemize}
\item If possible, avoid overlap counting --- when cases are disjoint, you can simply add results.
\item If not possible, do overlap counting but remember to apply the Inclusion-Exclusion principle.
\item Use encoding/decoding to decide how many stars and how many bars.
\end{itemize}


\newpage
\section{Answers}
\textbf{Exercise 1} (page~\pageref{q:ex-1}). 
% Can you specify the inverse $f^{-1}$?
See C1 slide 9, example 2, but with a twist: $f^{-1}: \mathbb{Z} \rightarrow \mathbb{N}$
\begin{equation*} % use * to remove equation number
    f^{-1}(z) =
    \begin{cases}
      2z + 1     & \text{if } z \ge 0\\
      -2z        & \text{if } z  < 0\\
    \end{cases}
\end{equation*}

\noindent
\textbf{Exercise 2} (page~\pageref{q:ex-2}). 
% Construct the queue of fractions by $\ll$-ordering.
% 0 -> -1/1 -> 1/1 -> -2/1 -> -1/2 -> 1/2 -> 2/1 -> ...
Just follow the 2D-grid:
\[
0 \ll 
\underbrace{\frac{-1}{1} \ll \frac{1}{1}}_{\mathtt{h} = 2} \ll 
\underbrace{\frac{-2}{1} \ll \frac{-1}{2} \ll \frac{1}{2} \ll \frac{2}{1}}_{\mathtt{h} = 3} \ll \dots 
\]

\noindent
\textbf{Exercise 3} (page~\pageref{q:ex-3}). 
% In the last example, suppose $m = 2$. In how many ways can the `bars' mingle with the `stars' with at least one star between bars?
Treat bar-and-bar as a big bar, there are $C(n+1,1)$ to be removed,
Thus the count is $C(n+2,2) - C(n+1,1) = C(n+1,2)$ by Pascal's Identity.
This suggests another way to solve this problem: treat one bar-star as a new-bar.
With one less star, $2$ bars, the count is $C((n-1)+2,2) = C(n+1,2)$.
% For n = 3, C(4,2) = 4x3/2 = 6
% For n = 5, C(6,2) = 6x5/2 = 15
% 3 stars, 2 bars, C(3+2,2) = C(5,2) = 5x4/2 = 10. 4 cases of 2-bars together
% Treat bar-and-bar as a big bar, there are C(3+1,1) = C(4,1) = 4 to be removed,
% Hence count = C(5,2) - C(4,1) = 10 - 4 = 6.

\bigskip
\noindent
\textbf{Exercise 4} (page~\pageref{q:ex-4}). 
% Can you give a bijection $\phi$ between $A$ and $B$ to show that $C(n, k) = C(n, n - k)$ ?
Let $\varphi : A \rightarrow B$ with $\varphi(t) = S \setminus t$. This is a bijection with $\varphi^{-1} = \varphi$.
Compare this bijection proof with the verbal proof in C1 slide 25.

\bigskip
\noindent
\textbf{Exercise 5} (page~\pageref{q:ex-5}).
Prove Pascal's identity (C1 slide 25),
\[
{n \choose k} = {{n - 1} \choose k} + {{n - 1} \choose {k - 1}}
\]
using definition $\displaystyle{{n \choose k} = \frac{n!}{k!(n-k)!}}$ (C1 slide 24).\\
In particular, $\displaystyle{{n \choose k} = 0}$ whenever $k > n$,
and $\displaystyle{{n \choose 0} = {n \choose n} = 1}$ for all $n \in \mathbb{N}^{*}$.\\
% by cases: k > n, trivial. k <= n,
% C(n-1,k-1) + C(n-1, k) = (n-1)!/(k-1)!(n - k)! + (n-1)!/k!(n-k-1)!
%                        = (n-1)!(k/k!(n-k)! + (n-k)/k!(n-k)!)
%                        = n!/k!(n-k!) = C(n,k)
When $k > n$,
\[
{n \choose k} = 0 = 0 + 0 = {{n - 1} \choose k} + {{n - 1} \choose {k - 1}}.
\]
When $k \le n$,
\[
\begin{array}{r@{\;=\;}l}
\displaystyle{{{n - 1} \choose k} + {{n - 1} \choose {k - 1}}} &
\displaystyle{\frac{(n-1)!}{k!(n - k - 1)!} + \frac{(n-1)!}{(k-1)!(n-k)!}}\\[1em]
& \displaystyle{(n-1)!\left(\frac{(n-k)}{k!(n-k)!} + \frac{k}{k!(n-k)!}\right)}\\[1em]
& \displaystyle{(n-1)!\frac{n}{k!(n-k)!}}\\[1em]
& \displaystyle{\frac{n!}{k!(n - k)!} = {n \choose k}} \qquad\blacksquare\\
\end{array}
\]

\bigskip
\noindent
\textbf{Exercise 6} (page~\pageref{q:ex-6}).
% Can you figure out this sum \quad 
Put $x = \frac{1}{2}$ in the binomial identity, then
\[
\sum_{k=0}^{n} \frac{C(n,k)}{2^{k}}
= \sum_{k=0}^{n} C(n,k)\left(\frac{1}{2}\right)^{k}
= \left(1 + \frac{1}{2}\right)^{n} = \left(\frac{3}{2}\right)^{n}
\]
% Put x = 1/2, so the sum is (1+1/2)^n = (3/2)^n.
Using Wolfram Alpha: \verb|sum of C(n,k)/2^k from 0 to n|
% \begin{verbatim}
% sum of C(n,k)/2^k from 0 to n
% \end{verbatim}

\bigskip
\noindent
\textbf{Exercise 7} (page~\pageref{q:ex-7}).
% Use complementary counting to find how many decimal PINs start or end with $0$ or both.
For decimal PINs, there are $10$ digits.
The count is $10^{4} - 9\times{10^{2}}\times{9} = 10^2(100 - 81) = 1900$.


\end{document}

More Counting:

Counting Functions
Membership
PINs with consecutives
Solution to Coin Puzzle


% pdflatex info04.tex