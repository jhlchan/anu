\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes, shapes.gates.logic.US, calc, fit, positioning}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% add the following two lines to your document to get bigger arrows
\usetikzlibrary{arrows.meta}
\tikzset{>={Latex[width=2mm,length=2mm]}}

% \usepackage{fdsymbol} % for card symbols: club, diamond, spade, heart
% This changes summation sigma and product pi to something very unnatural.
% just no: \vardiamondsuit, \varheartsuit, \varclubsuit, \varspadesuit

%\usepackage{listings} % for Javscript listing
% just use \begin{verbatim}  \end{verbatim}

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

\title{Sequence, Series, Algorithm, and Sorting}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Definitions}
Another distillation of ideas from lecture slides:
\begin{itemize}
\item a \textbf{sequence} is an indexed list (B2 slide 2).
\item a \textbf{series} is the sum of a numeric sequence (B2 slide 8).
\item a \textbf{algorithm} gives the steps from input to output (B1 slide 23).
\item a \textbf{sorting} algorithm sorts an indexed sequence in order (B2 slide 20).
\end{itemize}
Sometimes (that is, not always) the sequence indices start from $0 \in \mathbb{N}^{*} = \mathbb{N} \cup \{0\}$ (B2 slide 2, B1 slide 5). Need to read the question.

\section{Sequence}
Mathematicians count from $0$, but we count our fingers from $1$.
Thus there are at least two ways for index counting.
To cover the most general situation, the lecturer takes an indexed set $I$,
a subset of $\mathbb{N}^{*}$ (B2 slide 2).
For index $j \in I$, the $j$-th term of a sequence $s$ is denoted by $s_{j}$,
an element in some set $S$ given by some rule. I use $j$ for index, to be consistent with loop index $j$ later in algorithms.

Digging into definitions, another useful viewpoint is to say that the sequence $s : I \rightarrow S$ is a function, with image of $j$ being $s(j) = s_{j}$ (B2 slide 2).
Its sequence notation is $(s_{j})_{j \in I}$.

A sequence is a \emph{list}: members can repeat (B2 slides 2, 3). Examples are:
\begin{itemize}
\item A sequence of coin tosses: $\mathtt{H\;T\;T\;H\;T\;H\;H\;T}$.
\item A sequence of card: $(\diamondsuit, \mathtt{A})\; (\spadesuit, \mathtt{8})\;(\heartsuit, \mathtt{J})\;(\clubsuit, \mathtt{3})$.
\item A sequence of card suits: $\clubsuit\;\diamondsuit\;\spadesuit\;\heartsuit\;\clubsuit\;\diamondsuit\;\spadesuit\;\heartsuit$.
\item The sequence of odd numbers: $\mathtt{1\;3\;5\;7\;9\;11\;13\;\dots}$.
\item The sequence of squares: $\mathtt{1\;4\;9\;16\;25\;36\;49\;\dots}$.
\end{itemize}
Can you give a formula (that is, specify how to compute $s_{j}$) for the last two numeric sequences? Can you describe the sequence of even numbers? You can use explicit or implicit definitions (B2 slide 4).


\section{Series}
Given a sequence $a$ of numbers, its terms can be added as a sum, or multiplied as a product (B2 slide 7):
\[
\begin{array}{l}
\displaystyle\sum_{j=k}^{m} a_{j} = a_{k} + a_{k+1} + \dots + a_{m}\\[2em]
\displaystyle\prod_{j=k}^{m} a_{j} = a_{k} \times a_{k+1} \times \dots \times a_{m}\\
\end{array}
\]
where the index $j$ runs from initial $k$ (kicks off) to final $m$ (maximum).
Only the sum is called a series (you may know this trick: the product becomes a series by taking logarithm). In most cases, we want a formula for the partial sum up to $n$ terms.
This is achieved by running index $j = 1 \dots n$, or $j = 0 \dots (n-1)$:
\[
\displaystyle{S_{n} = \sum_{j=1}^{n} a_{j} = \sum_{j=0}^{n-1} a_{j+1}}
\]
The two sums are equal due to index shifting, a very useful technique.
Note that my notation appears different from the lecture slides. In fact, they are the same, you just need to get accustomed to the differences.

\subsection{Geometric Sequence}
The daily infections of COVID-19, if uncontrolled, is a geometric sequence: it doubles every day. In general, a geometric sequence (B2 slide 8) looks like this:
\[
a, ar, ar^{2}, ar^{3}, \dots
\]
where the inital value is $a$, and successive terms are multiplied by a common ratio $r$.
If index $j$ counts from $0$, the general term $a_{j} = ar^{j}$.
The geometric series is:
\[
S_{n} = \displaystyle{\sum_{j=0}^{n-1} ar^{j} = a \sum_{j=0}^{n-1} r^{j}  }
\]
This summation is easy if you can spot that:
\[
\begin{array}{rcl}
     S_{n} & = & a (1 + r + r^2 + \dots + r^{n-1})\\
   r S_{n} & = & a (\quad\ \ r + r^2 + r^3 + \dots + r^{n})\\
\hline
   (1 - r) S_{n} & = & a(1 - r^{n})   
\end{array}
\]
We can use summation to work this out properly,
$
rS_{n} = \displaystyle{a \sum_{j=0}^{n-1} r^{j+1} = a \sum_{j=1}^{n} r^{j}}
$
by index shifting. Upon subtraction,
\[
\begin{array}{rll}
(1 - r)S_{n} = 
    & a \displaystyle{\left(\sum_{j=0}^{n-1} r^{j} - \sum_{j=1}^{n} r^{j}\right)}
               & \text{by above}\\
  = & a \displaystyle{\left(1 + \sum_{j=1}^{n-1} r^{j} - \left(\sum_{j=1}^{n-1} r^{j} + r^{n}\right)\right)}
               & \text{by splitting sum}\\
  = & a (1 - r^{n}) & \text{by simplification}\\
\end{array}
\]
If $r \ne 1$, we can solve for $S_{n}$, getting \qquad
$S_{n} = \displaystyle{\sum_{j=0}^{n-1} ar^{j} = \frac{a(1 - r^{n})}{(1 - r)}}$.\\
What happens if $r = 1$? You can fill in:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 2);
\node at (2,1) {$S_{n} = a \displaystyle{\sum_{j=0}^{n-1} (1)^{j} = }$}; 
\end{tikzpicture}

\subsection{Arithmetic Sequence}
When the common ration $r > 1$, a geometric sequence describes exponential growth.
A linear growth is described by an arithmetic sequence (B2 slide 10), which looks like:
\[
a, a + d, a + 2d, a + 3d, \dots
\]
where the inital value is $a$, and successive terms differ by a common difference $d$.
If index $j$ counts from $0$, the general term $a_{j} = a + jd$, and the arithmetic series is:
\[
S_{n} = \displaystyle{\sum_{j=0}^{n-1} (a + jd) = a \sum_{j=0}^{n-1} 1 + d \sum_{j=0}^{n-1} j}
\]
The first sum is over constant $1$, so $\displaystyle{\sum_{j=0}^{n-1} 1 = n}$.\\
The second sum is over numbers from $0$ to $(n-1)$, which is quite visual:
\noindent
\begin{tikzpicture}[scale=0.5] % using a scale
% the grid
\draw[step=1, color=black!90!white] (0, 0) grid (5, 4);
\draw[blue, ultra thick]
  (0,0) -- (1,0) -- (1,1) -- (2,1) -- (2,2) -- (3,2) -- (3,3) -- (4,3) -- (4,4) -- (5,4);
\node at (0,4.5) {}; // dummy
\node at (15,2) {This shows \qquad
$\displaystyle{\sum_{j=0}^{n-1} j = 0 + \sum_{j=1}^{n-1} j = \frac{n(n-1)}{2}}$
\qquad\qquad(1)};
\end{tikzpicture}

\noindent
Therefore \qquad 
$\displaystyle{S_{n} = \sum_{j=0}^{n-1} (a + jd) = n \left(a + \frac{(n-1)}{2}d\right)}$.

\subsection{Geometric-Arithmetic Sequence}
A geometric sequence has a common ration $r$, with $a_{0} = a,\; a_{j+1} = a_{j} r$.
An arithmetic sequence has common difference $d$, with $a_{0} = a,\; a_{j+1} = a_{j} + d$.
Mixing them, a geometric-arithmetic sequence has both, with $a_{j+1} = a_{j} r + d,\; a_{0} = a$, giving this pattern:
\[
\begin{array}{l}
    a_{0} = a\\
    a_{1} = a_{0} r + d = ar + d\\
    a_{2} = a_{1} r + d = (ar + d)r + d = ar^2 + (1 + r)d\\
    a_{3} = a_{2} r + d = (ar^2 + d(1 + r))r + d = ar^3 + (1 + r + r^2)d\\
%    a_{4} = a_{3} r + d = (ar^3 + d(1 + r + r^2))r + d = ar^4 + (1 + r + r^2 + r^3)d\\
%    \dots
\end{array}
\]
% from which it is evident that, after applying the geometric series, (B2 slide 16):
% \[
% a_{j} = \displaystyle{ar^{j} + d \sum_{k=0}^{j-1} r^j = ar^{j} + \frac{(1 - r^{j})}{(1 - r)}d}
% \]
You can work it out a formula for the general term $a_{j}$, using geometric series (B2 slide 16):

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 2);
\node at (1,1) {$a_{j} = $}; 
\end{tikzpicture}

In math, especially for this course, pictures (like the Venn diagram for sets) and patterns are not rigorous: they suggests results, but they are not formal proofs. To prove formally a suggested result, especially one involving a number $n \in \mathbb{N}^{*}$, we use mathematical induction (B2 slide 5). However, pictures and patterns can aid in understanding. For me, I think in pictures and patterns, but I prove using logic.

There is a nice story told by Gauss (1777-1855), the mathematician who introduced modular arithmetic (B1 slide 30). When he was a little boy, the math teacher once asked the students to do the sum: $1 + 2 + 3 + \dots + 100$, perhaps to keep the students quiet. Gauss thought a bit, and came up instantly with the answer: $5050$. How did he do that?

Gauss didn't tell. I suppose he computed by logic: adding the first and the last, $1 + 100 = 101$, adding the second and the second last, $2 + 99 = 101$; since there are $100/2 = 50$ pairs of $101$, the sum must be $50\times{101} = 5050$.

The general result is:
\setcounter{equation}{1}
\begin{equation}
\label{eqn:ar-sum-2}
\displaystyle{\sum_{j=1}^{n} j = \frac{n(n+1)}{2}}
\end{equation}
This is equivalent to Equation~(1), by index shift.

\bigskip
\noindent
\textbf{Think:}
Express the general term of the geometric-arithmetic sequence in the form:
$a_{j} = A + B r^{j}$,
then find its series $\displaystyle{S_{n} = \sum_{j=0}^{n-1} a_{j}}$ when $r \ne 1$.

\section{Algorithm}
Algorithm gives the steps from input to output, described by pseduocode.
The lecturer has a pattern for pseduocode description of algorithm (B1 slide 23):

\begin{table}[h]
\begin{tabular}{l}
Algorithm: to do what\\
\hline
\textbf{Input}: \dots\\
\textbf{Output}: \dots\\
\textbf{Method}:\\
Initialise: \dots (start value of index $j$, e.g. $j \leftarrow 0$, or $j \leftarrow 1$)\\
Loop: \textit{stop check} (some condition on $j$ to exit the loop)\\
\qquad\quad\textit{loop body} (need to change index $j$, e.g., $j \leftarrow j + 1$)\\
Repeat loop.\\
Finalise: what to do upon exit of loop (can be nothing)\\
\hline      
\end{tabular}
\caption{Algorithm pseduo-code}
\label{tbl:pseudo-code}  % Note: put label after caption
\end{table}

\subsection{Decimal integer to binary}
Can you express the decimal $35$ in binary? You probably proceed as follows:
\[
\begin{array}{r@{\;\dots\;}l}
       2  |\underline{35} & 1\\
       2  |\underline{17} & 1\\
       2  |\underline{8}  & 0\\
       2  |\underline{4}  & 0\\
       2  |\underline{2}  & 0\\
                      1   & 0\\
\end{array}
\]
Thus $35_{10} = 1000011_{2}$. This is just a sequence of halving:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the doubles
   \node (n0) at (0,2) {$35$}; 
   \node (n1) at (2,2) {$17$}; 
   \node (n2) at (4,2) {$8$}; 
   \node (n3) at (6,2) {$4$}; 
   \node (n4) at (8,2) {$2$}; 
   \node (n5) at (10,2) {$1$}; 
   \node (n6) at (12,2) {$0$}; 
% the bits
   \node (b0) at (0.5,3) {remainder:};
   \node (b1) at (2,3) {$1$}; 
   \node (b2) at (4,3) {$1$}; 
   \node (b3) at (6,3) {$0$}; 
   \node (b4) at (8,3) {$0$}; 
   \node (b5) at (10,3) {$0$}; 
   \node (b6) at (12,3) {$1$}; 
% the arrows
   \draw[->] (n0) -- node[above] {half} (n1);
   \draw[->] (n1) -- node[above] {half} (n2);
   \draw[->] (n2) -- node[above] {half} (n3);
   \draw[->] (n3) -- node[above] {half} (n4);
   \draw[->] (n4) -- node[above] {half} (n5);
   \draw[->] (n5) -- node[above] {half} (n6);
   \draw[->] (n1) -- (b1);
   \draw[->] (n2) -- (b2);
   \draw[->] (n3) -- (b3);
   \draw[->] (n4) -- (b4);
   \draw[->] (n5) -- (b5);
   \draw[->] (n6) -- (b6);
\end{tikzpicture}

\bigskip
\noindent
Can you describe this algorithm by pseudocode? Try:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 8);
\node at (4.8,7.5) {\textbf{Algorithm}: convert decimal integer to binary}; 
\node at (1,6.5) {\textbf{Input}:}; 
\node at (1.2,5.5) {\textbf{Output}:}; 
\node at (1.2,4.5) {\textbf{Method}:}; 
\end{tikzpicture}

\noindent
Javascript is very close to pseduocode, and you can run it online, at\\
JavaScript Shell 1.4:
\url{https://www.squarefree.com/shell/shell.html}

\noindent
You can copy and paste the following Javascript code to the Shell:
\begin{verbatim}
function binary(n) {
    var b = [];  // output bits
    var j = 0;   // loop index
    b[0] = 0;
    while (n !== 0) {    // not (n = 0) for Javascript
        b[j] = n % 2;    // n mod 2
        n = (n / 2) | 0; // n div 2
        j = j + 1;
    }
    return b.join("");   // bits to string
}
\end{verbatim}
Then you can test the code by:
\begin{verbatim}
binary(35);
binary(17);
\end{verbatim}

\subsection{Decimal fraction to binary}
If halving converts decimal number to binary,
then doubling converts decimal fraction to binary:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the doubles
   \node (n0) at (0,2) {$1/7$}; 
   \node (n1) at (3,2) {$2/7$}; 
   \node (n2) at (6,2) {$4/7$}; 
   \node (n3) at (9,2) {$8/7$}; 
   \node (n4) at (9,0.5) {$1/7$}; 
   \node (n5) at (12,2) {$2/7$}; 
   \node (n6) at (15,2) {\dots}; 
% the bits
   \node (b0) at (2,3.5) {bits:};
   \node (b1) at (3,3.5) {$0$}; 
   \node (b2) at (6,3.5) {$0$}; 
   \node (b3) at (9,3.5) {$1$}; 
   \node (b4) at (12,3.5) {$0$}; 
   \node (b5) at (14,3.5) {(repeat)}; 
% the arrows
   \draw[->] (n0) -- node[above] {double} (n1);
   \draw[->] (n1) -- node[above] {double} (n2);
   \draw[->] (n2) -- node[above] {double} (n3);
   \draw[->] (n4) -- node[below] {\quad double} (n5);
   \draw[->] (n1) -- node[right] {$< 1$} (b1);
   \draw[->] (n2) -- node[right] {$< 1$} (b2);
   \draw[->] (n3) -- node[right] {not ($< 1$)} (b3);
   \draw[->] (n3) -- node[right] {$- 1$} (n4);
   \draw[->] (n5) -- node[right] {$< 1$} (b4);
   \draw[->] (n5) -- node[above] {double} (n6);
\end{tikzpicture}

\noindent
Thus $1/7 = {0.001 001 001 \dots}_{2} = 0.\overline{001}_{2}$.
We have met this algorithm before in Worksheet 3, Q4. I modify it slightly:

\begin{table}[h]
\begin{tabular}{l}
Algorithm: decimal fraction to binary, up to specified places.\\
\hline
\textbf{Input}: fraction $x$, number of places $p$.\\
\textbf{Output}: binary bits $0.b_{1}b_{2} \dots b_{p}$.\\
\textbf{Method}:\\
Initialise: $j \leftarrow 1$\\
Loop: if $j = p + 1$ $\mathtt{STOP}$\\
\qquad\qquad $x \leftarrow 2x$\\
\qquad\qquad if ($x < 1$) then $b_{j} \leftarrow 0$ else [$b_{j} \leftarrow 1, x \leftarrow x - 1$]\\
\qquad\qquad $j \leftarrow j + 1$\\
Repeat loop.\\
\hline      
\end{tabular}
\end{table}

\noindent
You can copy and paste the following Javascript code to the Shell:
\begin{verbatim}
function frac_binary(x, p) {
    var b = [];  // output bits
    var j = 1;   // loop index
    b[0] = "0.";
    while (j <= p) {
        x = 2 * x;
        if (x < 1) { // then
           b[j] = 0;
        }
        else {
           b[j] = 1;
           x = x - 1;
        }
        j = j + 1;
    }
    return b.join(""); // bits to string
}
\end{verbatim}
Then you can test the code by:
\begin{verbatim}
frac_binary(1/7,12);
frac_binary(0.45,18);
\end{verbatim}
Check with the answers we have worked out during Workshop 3.

\subsubsection{Trace Table}
Every algorithm can be traced through its steps.
In the lectures, the trace table is introduced very late (B2 slide 23).
In Workshop 3, I have shown how to use a trace table for the binary fraction conversion.
You can try this yourself, for $x = \frac{1}{13}$ to places $p = 10$:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 3);
\node at (8.2,1.5) {
$
\begin{array}{l||l|l|l|l|l|l|l|l|l|l|l|}
  j \text{, loop index}      & 1   & 2   & \quad 3 & \quad 4 & \quad 5 &
          \quad 6 & \quad 7 & \quad 8 & \quad 9 & \quad 10 & \quad 11\\
  b_{j} \text{, blank = $0$} &     &     &     &   &     &    &     & & & &\text{stop}\\  
    x  \text{, init or next} & 1/13 & 2/13 & & & & & & & & & \\
             x \leftarrow 2x & 2/13 &  & & & & & & & & & \\
                    x \ge 1? & F  & & & & & & & & & & \\ 
\end{array}
$
}; 
\end{tikzpicture}

\subsubsection{How does this work?}
Say you want to convert $1/5$ into binary, with bits either $0$ or $1$:
\[
\begin{array}{rcll}
         1/5 & = & \mathbf{0}.b_{1}b_{2}b_{3}b_{4}b_{5} \dots &
                   \mbox{$1/5 < 1$, so $\mathbf{0}.$}\\
1/5\times{2} & = & b_{1}.b_{2}b_{3}b_{4}b_{5}b_{6} \dots & 
                   \mbox{shift out $b_{1}$ by $\times{2}$}\\
         2/5 & = & \mathbf{0}.b_{2}b_{3}b_{4}b_{5}b_{6} \dots &
                   \mbox{$2/5 < 1$, $b_{1} \leftarrow \mathbf{0}$}\\
2/5\times{2} & = & b_{2}.b_{3}b_{4}b_{5}b_{6}b_{7} \dots & 
                   \mbox{shift out $b_{2}$ by $\times{2}$}\\
         4/5 & = & \mathbf{0}.b_{3}b_{4}b_{5}b_{6}b_{7} \dots &
                   \mbox{$4/5 < 1$, $b_{2} \leftarrow \mathbf{0}$}\\
4/5\times{2} & = & b_{3}.b_{4}b_{5}b_{6}b_{7}b_{8} \dots & 
                   \mbox{shift out $b_{3}$ by $\times{2}$}\\
         8/5 & = & \mathbf{1}.b_{4}b_{5}b_{6}b_{7}b_{8} \dots &
                   \mbox{$8/5 \ge 1$, $b_{3} \leftarrow \mathbf{1}$}\\
8/5 - \mathbf{1} = 3/5 & = & 0.b_{4}b_{5}b_{6}b_{7}b_{8} \dots &
                   \mbox{remove leading bit}\\
3/5\times{2} & = & b_{4}.b_{5}b_{6}b_{7}b_{8}b_{9} \dots & 
                   \mbox{shift out $b_{4}$ by $\times{2}$}\\
         6/5 & = & \mathbf{1}.b_{5}b_{6}b_{7}b_{8}b_{9} \dots &
                   \mbox{$6/5 \ge 1$, $b_{4} \leftarrow \mathbf{1}$}\\
6/5 - \mathbf{1} = 1/5 & = & 0.b_{5}b_{6}b_{7}b_{8}b_{9} \dots &
                   \mbox{remove leading bit}\\
\end{array}
\]
We are back to $1/5$, and the process repeats:
$1/5 = 0.2_{10} = 0.00110011 \dots_{2} = 0.\overline{0011}_{2}$.
This is an example of a terminating decimal in base $10$ becoming repeating in base $2$.
Use this method to verify that $3/8 = 0.375_{10} = 0.011_{2}$.

\bigskip
\noindent
\textbf{Think}:
What fractions are terminating in base $2$?\\
\emph{Hint} What fractions are terminating in base $10 = 2\times{5}$?

\subsubsection{Other bases}
With this understanding, we can rewrite the binary fraction algorithm as:

\begin{table}[h]
\begin{tabular}{l}
Algorithm: decimal fraction to binary.\\
\hline
\textbf{Input}: fraction $x \in \mathbb{Q},\quad 0 < x < 1$.\\
\textbf{Output}: binary bits $0.b_{1}b_{2} \dots \text{in base }\mathbf{2}$.\\
\textbf{Method}:\\
Initialise: $j \leftarrow 1$\\
Loop: $\mathtt{forever}$\\
\qquad\qquad $x \leftarrow \mathbf{2}x$\\
\qquad\qquad $b_{j} \leftarrow \mathtt{integer}(x)$\\
\qquad\qquad $x \leftarrow \mathtt{fraction}(x)$\\
\qquad\qquad $j \leftarrow j + 1$\\
Repeat loop.\\
\hline      
\end{tabular}
\end{table}

\noindent
Here $\mathtt{integer}(x) = \lfloor{x}\rfloor$ is the `floor' of $x$ (see Worksheet 2, Q6, function $d(n)$),
and  $\mathtt{fraction}(x) = x - \lfloor{x}\rfloor$, dropping the integer part.
This algorithm goes on forever, but you'll stop when you detect a repeating pattern (say repeating $0$).

\bigskip
\noindent
\textbf{Think}:
Modify this algorithm to convert a fraction to any base, say base $3$.

\section{Towers of Hanoi}
This disc-tranferring puzzle is introduced in B2 slide 17.
The algorithm is recursive:

\begin{table}[h]
\begin{tabular}{l}
Algorithm: Transfer $n$ disks from left pole, via middle pole, to right pole.\\
\hline
\textbf{Input}: $n$ disks at left pole.\\
\textbf{Output}: $n$ disks at right pole.\\
\textbf{Method}:\\
\qquad if ($n = 0$) $\mathtt{STOP}$.\\
\qquad transfer top $(n-1)$ disks from left to middle, using right pole.\\
\qquad move the bottom (last) disk from left to right.\\
\qquad transfer the $(n-1)$ disks from middle to right, using left pole.\\
\hline      
\end{tabular}
\end{table}

\noindent
The Javascript code is:
\begin{verbatim}
function hanoi(n, left, middle, right) {
  if (n == 0) return;
  hanoi(n - 1, left, right, middle);
  print("move disk " + n + " from " + left + " to " + right);
  hanoi(n - 1, middle, left, right);
}
\end{verbatim}
and run it by:
\begin{verbatim}
hanoi(3, "A", "B", "C");
\end{verbatim}
To count the number of moves, use a global counter:
\begin{verbatim}
var moves = 0;
function hanoi(n, left, middle, right) {
  if (n == 0) return;
  hanoi(n - 1, left, right, middle);
  print("move disk " + n + " from " + left + " to " + right);
  moves = moves + 1; // increment counter
  hanoi(n - 1, middle, left, right);
}
function count_hanoi(n) {
  moves = 0;
  hanoi(n, "A", "B", "C");
  return "Done, in " + moves + " steps.";
}
\end{verbatim}
and run it by:
\begin{verbatim}
count_hanoi(3);
\end{verbatim}
Let $T_{n} = $ the number of moves for a pile of $n$ discs.
The algorithm shows $T_{0} = 0$ due to immediate return,
and $T(n) = T(n-1) + 1 + T(n-1) = 2T(n-1) + 1$ due to one move between two recursive calls.
We can solve for $T(n)$ by
\[
\begin{array}{rr@{\; = \;}l}
\text{adding $1$ to both sides:} & T(n) + 1 & 2T(n-1) + 2 = 2(T(n-1))\\
\text{Let $a_{n} = T(n) + 1$, then} &   a_{n} & 2 a_{n-1}\\
\text{$a_{n}$ is a geometric sequence, so} & a_{n} & 2^{n}\\
\text{and $T(n) = a_{n} - 1$, hence}   & T(n) & 2^{n} - 1
\qquad\text{(B2 slide 19)}\\
\end{array}
\]
\noindent
% For a pile of $n$ discs, the number of moves is $T_{n} = 2^{n} - 1$ (B2 slide 19).
Look up Wikipedia \url{https://en.wikipedia.org/wiki/Tower_of_Hanoi} for the legend of the Towers of Hanoi. The priests were supposed to transfer a pile of $64$ discs of increasing radius from left to right in accordance with the rules of the game. That would keep them busy until the end of time!


\section{Sorting}
Given a word of $5$ alphabets: $\mathtt{ALBUM}$, there are two ways to sort:
\begin{itemize}
\item Direct -- sort the alphabets, to give $\mathtt{ABLMU}$.
\item Indexed -- index the alphabets: $x_{j} = j$-th alphabet of $\mathtt{ALBUM}$, and sort the index, giving
$ \pi = \begin{pmatrix}
    1 & 2 & 3 & 4 & 5\\
    1 & 3 & 2 & 5 & 4 
 \end{pmatrix} $.
\end{itemize}
The index notation is given in B2 slide 21. For the example above,
\begin{itemize}[label=$\diamond$]
\item Before sorting: $x_{1} = \mathtt{A}$, $x_{2} = \mathtt{L}$, \emph{etc.},
so that the original sequence is $(x_{j})_{j \in \{1 \dots 5\}} = \mathtt{ALBUM}$.
\item After sorting: $x_{\pi(1)} = x_{1} = \mathtt{A}$,
$x_{\pi(2)} = x_{3} = \mathtt{B}$, \emph{etc.},
so that the sorted sequence by index is $(x_{\pi(j)})_{j \in \{1 \dots 5\}} = \mathtt{ABLMU}$.
\end{itemize}

\subsection{Selection Sort}
Given a sequence (of alphabets) $\mathtt{ALBUM}$, direct selection sort proceeds as follows:
\begin{itemize}
\item find $1$-st sorted element: look for the least in order of $\mathtt{ALBUM}$, it is $\mathtt{A}$, swap with $1$-st current element (happens to be the same), sorted: $\mathtt{A}$.
\item find $2$-nd sorted element: look for the least in order of $\mathtt{LBUM}$, it is $\mathtt{B}$, swap with $2$-nd current element (which is $\mathtt{L}$), sorted: $\mathtt{AB}$.
\item (and so on \dots)
\item find $5$-th sorted element: now looking at $\mathtt{U}$, no need to swap, sorted: $\mathtt{ABLMU}$.
\end{itemize}
We can trace through this direct version:
\[
\begin{array}{lll}
  j & \mathtt{ALBUM} = (x_{j})  &\\
\hline
  1 & \mathtt{(ALBUM)}  & \text{initial}\\
  2 & \mathtt{A(LBUM)}  & \text{swap }x_{1}, x_{1}\\
  3 & \mathtt{AB(LUM)}  & \text{swap }x_{2}, x_{3}\\
  4 & \mathtt{ABL(UM)}  & \text{swap }x_{3}, x_{3}\\
  5 & \mathtt{ABLM(U)}  & \text{swap }x_{4}, x_{5}\\
\text{stop} & \mathtt{ABLMU} & \text{sorted}\\
\end{array}
\]%
Put succinctly, 
$
\mathtt{(ALBUM)} \rightarrow 
\mathtt{A(LBUM)} \rightarrow
\mathtt{AB(LUM)} \rightarrow 
\mathtt{ABL(UM)} \rightarrow 
\mathtt{ABLM(U)}
$.
Each time, when sorting the sub-list $(\dots)$, the head element compares with the rest. The number of comparison in this example is: $4 + 3 + 2 + 1 + 0 = 5(4+0)/2 = 10$.

The algorithm for direct selection sort is:

\begin{table}[h]
\begin{tabular}{ll}
\textbf{Algorithm}:  & sort a sequence by selection.\\
\hline
\textbf{Input}: & a given sequence $(x_{j})_{1\dots{n}}$.\\
\textbf{Output}: & a sorted sequence $(x_{j})_{1\dots{n}}$.\\
\textbf{Method}:\\
Initialize: & $j \leftarrow 1$ \\
      Loop: & if $j = n$, $\mathtt{STOP}$\\
            & swap $x_{j}$ with least $(x_{i})_{j\dots{n}}$ \\
            & $j \leftarrow j + 1$ \\
Repeat loop.\\
\end{tabular}
\end{table}

\noindent
To find the least element, the direct version is:

\begin{table}[h]
\begin{tabular}{ll}
Algorithm:  & find the least element of a sequence.\\
\hline
\textbf{Input:} & sequence $(x_{i})_{\text{start}\dots\text{finish}}$\\
\textbf{Output:} & its least element.\\
\textbf{Method:}\\
Initialize: & $\text{mark} \leftarrow \text{start}, j \leftarrow \text{start} + 1$\\
      Loop: & if $j = \text{finish} + 1$, return $x_{\text{mark}}$. \\
            & if $x_{j} < x_{\text{mark}},\; \text{mark} \leftarrow j$ \\
            & $j \leftarrow j + 1$ \\
Repeat loop.\\
\end{tabular}
\end{table}

\noindent
It is this subroutine that does ($\text{finish} - \text{start}$) comparisions for ordering. As a result, the total number of comparisons for selection sort of $n$ elements is:
\[
T_{n} = \displaystyle{\sum_{j=1}^{n} (n - j) \overset{\text{reverse}}{\underset{\text{sum}}{\qquad = \qquad}}
 \sum_{j=0}^{n-1} j = \frac{n(n-1)}{2}} \qquad\qquad\text{by Equation~(1)}.
\]

\bigskip
\noindent
\textbf{Exercise}. Apply direct selection sort to the word $\mathtt{ABBA}$. Verify that the number of comparisons is $4\times{3}/2 = 6$.

\bigskip
The algorithm for indirect selection sort is given in B2 slide 24:

\begin{table}[h]
\begin{tabular}{ll}
Algorithm:  & sort a sequence by selection, using an index.\\
\hline
\textbf{Input}: & a given sequence $(x_{j})_{1\dots{n}}$.\\
\textbf{Output}: & an index permutation $\pi$ so that $(x_{\pi(j)})_{1\dots{n}}$ is sorted.\\
\textbf{Method}:\\
Initialize: & $j \leftarrow 1$ \\
      Loop: & if $j = n$, $\mathtt{STOP}$\\
            & swap $\pi(j)$ with $\pi$(index of least $(x_{i})_{j\dots{n}}$) \\
            & $j \leftarrow j + 1$ \\
Repeat loop.\\
\end{tabular}
\end{table}
\noindent
where the least element index is found by (B2 slide 23):

\begin{table}[h]
\begin{tabular}{ll}
Algorithm:  & find the index of the least element of a sequence.\\
\hline
\textbf{Input:} & sequence $(x_{i})_{\text{start}\dots\text{finish}}$
  and index permutation $\pi$.\\
\textbf{Output:} & index of its least element.\\
\textbf{Method:}\\
Initialize: & $\text{mark} \leftarrow \text{start}, j \leftarrow \text{start} + 1$\\
      Loop: & if $j = \text{finish} + 1$, return $\text{mark}$. \\
            & if $x_{\pi(j)} < x_{\pi(\text{mark})},\; \text{mark} \leftarrow j$ \\
            & $j \leftarrow j + 1$ \\
Repeat loop.\\
\end{tabular}
\end{table}

\newpage
\noindent
Watch how the permutation $\pi$ evolves when tracing the indirect selection sort, \emph{i.e} the top row $j$ is unchanged, while the bottom row $\pi(j)$ can change. Due to this, only $\pi(j)$ is shown in B2 slides 23, 24.
\[
\begin{array}{lllll}
  j & \text{index set}  & (x_{j}) = \mathtt{ALBUM} & \text{permutation}  & (x_{\pi})\\
\hline
  1 & \mathtt{[12345]}  & \text{initial}     &
   \pi = \begin{pmatrix} 1 & 2 & 3 & 4 & 5\\ 1 & 2 & 3 & 4 & 5 \end{pmatrix} &
   \mathtt{ALBUM}\\
  2 & \mathtt{1[2345]}  & \text{swap $\pi$ idx }1, 1   &
   \pi = \begin{pmatrix} 1 & 2 & 3 & 4 & 5\\ 1 & 2 & 3 & 4 & 5 \end{pmatrix} &
   \mathtt{ALBUM}\\
  3 & \mathtt{13[245]}  & \text{swap $\pi$ idx }2, 3   &
   \pi = \begin{pmatrix} 1 & 2 & 3 & 4 & 5\\ 1 & 3 & 2 & 4 & 5 \end{pmatrix} &
   \mathtt{ABLUM}\\
  4 & \mathtt{132[45]}  & \text{swap $\pi$ idx }3, 3   &
   \pi = \begin{pmatrix} 1 & 2 & 3 & 4 & 5\\ 1 & 3 & 2 & 4 & 5 \end{pmatrix} &
   \mathtt{ABLUM}\\
  5 & \mathtt{1325[4]}  & \mbox{swap $\pi$ idx }4, 5   &
   \pi = \begin{pmatrix} 1 & 2 & 3 & 4 & 5\\ 1 & 3 & 2 & 5 & 4 \end{pmatrix} &
   \mathtt{ABLMU}\\
\mathtt{STOP} & \mathtt{[13254]} & \text{index sorted}\\
\end{array}
\]
\noindent
The number of comparison, indirectly through index, is the same: $T_{n} = 2^{n} - 1$ for selection sort of $n$ elements.

\bigskip
\noindent
\textbf{Exercise}. Apply indirect selection sort to the word $\mathtt{ABBA}$. Trace through its steps. You can try the method in B2 slides 23, 24.


\subsection{Merge Sort}
The key to merge sort is the following observation: it is easy to merge two sorted lists.
For example,
\[
\begin{array}{rlll}
\text{Two sorted lists} & \mathtt{ACE} & \mathtt{BE} & \text{merge result}\\
\text{take out least of heads} & \mathtt{CE} & \mathtt{BE} & \mathtt{A}\\
\text{take out least of heads} & \mathtt{CE} & \mathtt{E} & \mathtt{AB}\\
\text{take out least of heads} & \mathtt{E} & \mathtt{E} & \mathtt{ABC}\\
\text{equal heads, take second (B2 slide 27)} & \mathtt{E} &  & \mathtt{ABCE}\\
\text{one is empty, just append} &  &  & \mathtt{ABCEE}\\
\end{array}
\]
Put succinctly,
\[
\mathtt{(ACE)(BE)} \rightarrow
\mathtt{A(CE)(BE)} \rightarrow
\mathtt{AB(CE)(E)} \rightarrow
\mathtt{ABC(E)(E)} \rightarrow
\mathtt{ABCE(E)()} \rightarrow
\mathtt{ABCEE}
\]
When two heads are equal, one can take either one. The choice won't affect resulting sort, but will affect the number of comparisons. In B2 slide 27, the first is taken only when it is less than the second, otherwise the second is always taken. This corresponds to the rule: when heads are equal, take the second. This is an \textbf{arbitrary rule}, maintained by the lecturer to keep a consistent count of comparisons in merge sort. 

Thus one can always merge a pair of sorted lists into a longer sorted list.
This begs the question: where do we find sorted lists to start with?
It turns out that the shortest one element list, called \emph{singleton}, is sorted!
For example,
given a sequence (of alphabets) $\mathtt{ALBUM}$, merge sort proceeds as follows:
\begin{itemize}
\item put into singletons: $\mathtt{(A)(L)(B)(U)(M)}$, each is sorted by default.
\item merge the pairs: $\mathtt{(A)(L)}$, $\mathtt{(B)(U)}$ to give longer pairs: $\mathtt{(AL)(BU)(M)}$.
\item keep merging pairs until there is no pair.
\item what remains is a single sorted list: $\mathtt{(ABLMU)}$. 
\end{itemize}
This is the algorithm for merge sort (actually equivalent to B2 slide 29):

\begin{table}[h]
\begin{tabular}{ll}
Algorithm:  & sort a sequence by merging.\\
\hline
\textbf{Input}: & a given sequence $(x_{j})_{1\dots{n}}$.\\
\textbf{Output}: & a sorted sequence $(x_{j})_{1\dots{n}}$.\\
\textbf{Method}:\\
Initialize: & treat the sequence as singleton lists. \\
      Loop: & if no pairs to merge, $\mathtt{STOP}$\\
            & merge pairs of sorted lists \\
Repeat loop.\\
\end{tabular}
\end{table}
\noindent
and this is how to merge sorted pairs (B2 slide 27):

\begin{table}[h]
\begin{tabular}{ll}
Algorithm:  & merge a pair of sorted lists.\\
\hline
\textbf{Input}: & two sorted sequences $(a_{i}), (b_{j})$.\\
\textbf{Output}: & a sorted sequence $(c_{k})$.\\
\textbf{Method}:\\
Initialize: & $k \leftarrow 1, \text{max} \leftarrow length(a) + length(b) $ \\
      Loop: & if $k = \text{max} + 1$, $\mathtt{STOP}$\\
            & if $(a)$ empty, append $(b)$ to $(c)$, $k \leftarrow \text{max}$.\\
            & else if $(b)$ empty, append $(a)$ to $(c)$, $k \leftarrow \text{max}$.\\
            & else [ both $(a), (b)$ are non-empty ] \\
            & \qquad $c_{k} \leftarrow \text{ least of }head(a), head(b)$\\
            & \qquad [ if equal, take head from $(b)$ (arbitrary rule)]\\
            & \qquad remove the least head from its list.\\
            & $k \leftarrow k + 1$\\
Repeat loop.\\
\end{tabular}
\end{table}

\noindent
This is a trace of the merge sort of our example:
\[
\begin{array}{lll}
  \text{number of lists} & \text{sequence }\mathtt{ALBUM}  &\\
\hline
  5 & \mathtt{(A)(L)(B)(U)(M)}  & \text{initial}\\
  3 & \mathtt{(AL)(BU)(M)}  & \text{merge pairs}\\
  2 & \mathtt{(ABLU)(M)}  & \text{merge pairs}\\
  1 & \mathtt{(ABLMU)}  & \text{merge pairs}\\
\mathtt{STOP} & \mathtt{ABLMU} & \text{sorted}\\
\end{array}
\]
To count the number of comparisons, examine each merge.
Note that once a list in a pair is empty, there is no more comparison.

\[
\begin{array}{ll}
\text{merge pairs, \# = number of comparisons} & \text{\#}\\
\hline
\mathtt{(A)(L)} \rightarrow \mathtt{A()(L)} \rightarrow \mathtt{AL}     &  1\\
\mathtt{(B)(U)} \rightarrow \mathtt{B()(U)} \rightarrow \mathtt{BU}     &  1\\
\mathtt{(AL)(BU)} \rightarrow \mathtt{A(L)(BU)} \rightarrow \mathtt{AB(L)(U)}
\rightarrow \mathtt{ABL()(U)} \rightarrow \mathtt{ABLU}     &  3\\
\mathtt{(ABLU)(M)} \rightarrow \mathtt{A(BLU)(M)} \rightarrow \mathtt{AB(LU)(M)}
\rightarrow \mathtt{ABL(U)(M)}
\rightarrow \mathtt{ABLM(U)()} \rightarrow \mathtt{ABLMU}     &  4\\
\hline
\multicolumn{1}{r}{\text{total:}}   & 9\\
\end{array}
\]
There is no exact formula for the number of comparisons.
Although each comparison leads to a transfer of the head from either list,
there is no more comparisons after one list is exhausted: the rest is simply transferred (\emph{e.g.}, a single copy of $3$ elements is counted as $3$ transfers).

Therefore the number of comparisons is always bounded by the number of transfers.
To merge two sorted lists of lengths $l_{1}$ and $l_{2}$, the number of transfers is equal to $l_{1} + l_{2}$.
Suppose we have a sequence of length $8 = 2^{3}$, then,
\begin{itemize}
\item start with $8$ one element lists (singletons),
\item merge into $8/2 = 4$ two element lists, each pair merges with $1 + 1 = 2$ transfers.
\item merge into $4/2 = 2$ four element lists, each pair merges with $2 + 2 = 4$ transfers.
\item merge into $2/2 = 1$ eight element list, each pair merges with $4 + 4 = 8$ transfers.
\item the one eight element list is sorted.
\end{itemize}
Thus the total number of transfers for $3$ stages is
$T_{3} = 4\times{2} + 2\times{4} + 1\times{8} = 24$.

\noindent
The pattern for the number of transfers, which is a bound for the number of comparisons in merge sort, is:
\[
\begin{array}{rll}
\text{stages}  & \text{sequence length} & \text{number of transfers}\\
\hline
1 & n = 2^{1} = 2 & T_{1} = 1\times{2} = 2\\
2 & n = 2^{2} = 4 & T_{2} = 2\times{2} + 1\times{4} = 2\times{4} = 8\\
3 & n = 2^{3} = 8 & T_{2} = 4\times{2} + 2\times{4} + 1\times{8} = 3\times{8} = 24\\
4 & n = 2^{4} = 16 & T_{2} = 8\times{2} + 4\times{4} + 2\times{8} + 1\times{16} = 4\times{16} = 64\\
\end{array}
\]
This pattern suggests that, for $r$ stages, $T_{r} = r2^{r}$ for a sequence of length $n = 2^{r}$.
This formula can be proved by mathematical induction (B2 slide 32).

\bigskip
\noindent
\textbf{Exercise}. Apply merge sort to the word $\mathtt{ABBA}$. Count carefully the number of comparisons, and beware of the arbitrary rule: when heads are equal, take the second head. The number of comparisons is at most $2\times{4} = 8$.

\bigskip
\noindent
\textbf{Think}. A merge sort results in this $8$-letter word $\mathtt{ABCDEFGH}$.
Reverse the merge sort algorithm to find the best-case example of the original word.
What is the worst-case example?

\end{document}

Extra: Javascript codes for sorting
===================================

// should be const, but Javascript Shell has a bug!
var alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// compare characters x, y
function less(x, y) {
    return alphabets.indexOf(x) < alphabets.indexOf(y);
}
// testing
less("C", "E");
less("E", "C");
less("E", "E");

// select the least in a list and swap to front
function select(x) { // x = character list
    var k = 0; // mark itself
    var j = 1; // no need to compare itself
    // locate the least to swap
    while (j < x.length) {
        if (less(x[j],x[0])) k = j; // ready for swap
        j = j + 1;
    }
    // swap x[k] with x[0]
    var c = x[0];
    x[0] = x[k];
    x[k] = c;
    return x; // front has least
}
// testing
select("ALBUM".split(""));
select("BLAUM".split(""));
// selection sort of a word
function select_sort(word) {
    var x = word.split(""); // to character list
    var y = []; // the sorted word
    while (x.length > 0) {
        x = select(x); // swap least to front
        y.push(x.shift()); // get the current least
        print(y.join("")+"("+x.join("")+")");
    }
    return y.join(""); 
}
// run as:
select_sort("ALBUM");

// or tick tock, or tik tok
// merge two strings
function merge(dum, dee) {
    var a = dum.split(""); // to character list
    var b = dee.split(""); // to character list
    var c = [];
    print("merge: ("+a.join("")+")("+b.join("")+")");
    while ((a.length !== 0) || (b.length !== 0)) {
        if (a.length == 0) {
            c.push(b.shift());
        }
        else if (b.length == 0) {
            c.push(a.shift());
        }
        else {
            if (less(a[0], b[0])) {
                c.push(a.shift());
            }
            else {
                c.push(b.shift());
            }
        }
        print(c.join("")+"("+a.join("")+")("+b.join("")+")");
    }
    return c.join("");
}
// run as:
merge("ACEF", "BE");
// merge sort of a word
function merge_sort(word) {
    var pool = word.split(""); // to character list
    pool.push(""); // mark stage end
    while (pool.length > 2) {
        if (pool[0] == "") {
            print("next");
            pool.push(pool.shift()); // put marker to back
        }
        else if (pool[1] == "") {
            print("skip");
            pool.push(pool.shift()); // put singleton to back
            pool.push(pool.shift()); // put marker to back
        }
        else { // there are two lists
            pool.push(merge(pool.shift(), pool.shift()));
        }
        print("pool: " + pool);
    }
    return pool.join(""); // eliminate marker
}
// run as:
merge_sort("ALBUM");



% pdflatex info02.tex