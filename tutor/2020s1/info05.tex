\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
\usepackage{pgfplots} % for function plotting
\pgfplotsset{compat=1.7} % for color matching

\usetikzlibrary{matrix} % for matrix nodes
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
% \usepackage{ifthen} % has \ifthenelse, but PGF Math Error: unknown o or of
\usepackage{todonotes} % has \ifthenelse !
\usepackage{textcomp} % for textdollaroldstyle

% special marks
\usepackage{amssymb} % http://ctan.org/pkg/amssymb, for \because and \therefore
\usepackage{pifont}% http://ctan.org/pkg/pifont, for \ding
\newcommand{\cmark}{\ding{51}}% check tick
\newcommand{\xmark}{\ding{55}}% cross X

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

% \title{Matrix, Vector, Linearity, Cardinality and Counting}
\title{Probability}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

% the text cross-mark
\newcommand{\txmark}{\text{\xmark}} % opposite of \checkmark, after begin{document}

For a finite sample space $\mathtt{S}$ with equally likely outcomes (C2 slide 9),
the probability of an event $\mathtt{E} \subseteq \mathtt{S}$ is:
$\mathbb{P}(\mathtt{E}) = |\mathtt{E}|/|\mathtt{S}|$.
This involves counting.
It is important to identity the sample space $\mathtt{S}$ and the event $\mathtt{E}$, both are sets.

\section{Dice Simulation}
In Worksheet 6 Q1, we have seen how Alice can use two throws of d6 $(a,b)$
to simulate a d12. There are many ways to achieve this.
One formula for the final value $v$, based on parity of the second throw $b$, is:
\begin{equation*} % use * to remove equation number
    v =
    \begin{cases}
      a        & \text{if even } b\\
      a + 6    & \text{if odd } b\\
    \end{cases}
\end{equation*}
This can also be expressed as: $v = a + 6 (b \bmod{2})$.
The expression $(b \bmod{2})$ turns the second throw of d6 into just two values $0, 1$, like a coin with $\mathtt{H}, \mathtt{T}$:
\[
\begin{array}{rr|l}
    a & (b \bmod{2}) & v = a + 6(b \bmod{2})\\
\hline    
    1 &  0 & 1\\
    2 &  0 & 2\\
    3 &  0 & 3\\
    4 &  0 & 4\\
    5 &  0 & 5\\
    6 &  0 & 6\\
    1 &  1 & 7\\
    2 &  1 & 8\\
    3 &  1 & 9\\
    4 &  1 & 10\\
    5 &  1 & 11\\
    6 &  1 & 12\\
\end{array}
\]
You could have thrown a d6 together with a coin to simulate a d12.
Using this idea, two throws of d6 can simulate a d4 correctly:
\[
\begin{array}{rr|c|l}
   (a \bmod{2}) & (b \bmod{2}) & \text{base }2 & v = 1 + 2(a \bmod{2}) + (b \bmod{2})\\
\hline
   0 & 0 & \mathtt{00} & 1\\
   0 & 1 & \mathtt{01} & 2\\
   1 & 0 & \mathtt{10} & 3\\
   1 & 1 & \mathtt{11} & 4\\             
\end{array}
\]
Compare this table with the truth-tables you are familiar with.\\

\bigskip
\noindent
\label{q:ex-1}%
\textbf{Exercise 1}. Can you simulate a d8 using d6 throws?
% Use 3 throws:  v = 1 + 4(a mod 2) + 2(b mod 2) + (c mod 2)

\bigskip
\noindent
\label{q:ex-2}%
\textbf{Exercise 2}. Can you simulate a d9 using d6 throws?
% Use 2 throws, but mod 3:  v = 1 + 3(a mod 3) + (b mod 3)


\section{God throws dice}
% Birth-related problems.
Imagine God throws a d7 dice to determine our birth weekdays: if the dice shows 2, you are born on Tuesday, \emph{etc.} In a room with $3$ students, the sample space of their birth-weekdays can be very large:
\[
\begin{array}{rrr}
\text{student}_{1} & \text{student}_{2} & \text{student}_{3}\\
\hline
        1          &  1     & 1\\
        1          &  1     & 2\\
        \dots & \dots & \dots\\        
        7          &  7     & 6\\
        7          &  7     & 7\\
\end{array}
\]
How large is the sample space $\mathtt{S}$ ? Recall that a truth-table of $n$ symbols with only $2$ variations $\mathtt{T} \text{ and } \mathtt{F}$ has $2^{n}$ rows.
Now it is $n = 3$ students each with $k = 7$ variations, thus $|\mathtt{S}| = k^{n}$.

How many of these variations have two same weekdays? This will answer the question: how likely is this event $\mathtt{E}$: among $n$ randomly chosen people at least two share the same value among $k$ variations? 

Since the opposite of `at least' is `none', we use this trick (C2 slide 10):
\[
\mathbb{P}(\mathtt{E}) = 1 - \mathbb{P}(\mathtt{E^{c}}) = 1 - \frac{P(k,n)}{k^{n}}.
\]
Note that permutation $P(k,n)$ is used since each variation is ordered: in $(1,1,2)$ the first student is born on Monday, while in $(2,1,1)$ he/she is born on Tuesday.
For birth-weekdays $k = 7$, for birth-months $k = 12$, for birth-weeks $k = 52$,
and for birthdays $k = 365$ (C2 slide 54).
Once $k$ is fixed, the probability $p(n)$ is a function of $n$, the number of people chosen:
\[
p(n) = 1 - \frac{P(k,n)}{k^{n}} = 1 - \frac{k!}{(k - n)!k^{n}}
\]
Check that $p(1) = 0$ (since $P(k,1) = k$), which is correct for a single person,
and $p(k+1) = 1$ (since $P(k, n) = 0$ for $n > k$), which is true due to Pigeonhole principle: \emph{e.g.} how likely is that among $8$ students, there is a pair with the same birth-weekday?
Thus a graph of $p(n)$ has two known end-points.
What happens as $n$ varies in between?
The curves for different values of $k$ are shown.

\bigskip
\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
    declare function={birth(\n)=1-720*7^(1-\n)/(7-\n)!;}
]
\begin{axis}[
    samples at={1,...,8},
    yticklabel style={
        /pgf/number format/fixed,
        /pgf/number format/fixed zerofill,
        /pgf/number format/precision=1
    },
    legend style={at={(1,0.87)},anchor=west}
]
\addplot[smooth,mark=*] {birth(x)};
\addplot[color=blue,thick] (x, 0.5); % even chance
\addlegendentry{$k = 7$, birth-weekday}
\end{axis}
\end{tikzpicture}
% table [N(1 - P(7,n)/7^n), {n,1 .. 8}] gives n, 1 - 720 (7)^(1-n)/(7-n)!

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
    declare function={birth(\n)=1-1925*12^(5-\n)/(12-\n)!;}
]
\begin{axis}[
    samples at={1,...,13},
    yticklabel style={
        /pgf/number format/fixed,
        /pgf/number format/fixed zerofill,
        /pgf/number format/precision=1
    },
    legend style={at={(1,0.87)},anchor=west}
]
\addplot[smooth,mark=*] {birth(x)};
\addplot[color=blue,thick] (x, 0.5); % even chance
\addlegendentry{$k = 12$, birth-month}
\end{axis}
\end{tikzpicture}
% table [N(1 - P(12,n)/12^n), {n,1 .. 13}] gives n, 1 - 1925 (12)^(5-n)/(12-n)!

\noindent
In each case, you can determine the minimal number $n$ of students to make $\mathbb{P}(\mathtt{E}) > 50\%$, better than even chance.
Note the sharp initial rise, and the quick approach towards certainty.
Wolfram Alpha can produce these graphs:
\begin{verbatim}
plot table [1 - P(k,n)/k^n, {n,1 .. k}] for k = 52
plot table [1 - P(k,n)/k^n, {n,1 .. k}] for k = 365
\end{verbatim}
% table [N(1 - P(52,n)/52^n), {n,1 .. 53}] exact form still has (52-n)!
% plot table [1 - P(52,n)/52^n, {n,1 .. 53}]
Move the cursor in the graph to get the crosshair.
To get the probabilities:
\begin{verbatim}
table [(n, 1 - P(k,n)/k^n //N), {n,1 .. k}] for k = 7
table [(n, N(1 - P(k,n)/k^n)), {n,1 .. k}] for k = 12
\end{verbatim}
The suffix \verb|//N| or the function \verb|N(...)| converts an exact fraction to decimal.

\section{Sampling}
From a population of $n$, with $s$ special members. select a random sample of size $k$.
What is the chance of the sample having at least one of the specials?

It is exam time, and I forget the formula! What shall I do?
I simplify the problem. Let $n = 5$, say the population is $\{a, b, c, d, e\}$.
Let $k = 2$, choose a random pair.
There are $|\mathtt{S}| = C(5,2) = {5 \choose 2} = 10$ pairs:
\[
\{a, b\}, \{a, c\}, \{a, d\}, \{a, e\}, \{b, c\} \{b, d\} \{b, e\}, \{c, d\}, \{c, e\}, \{d, e\}
\]
Suppose vowels, $a$ and $e$, are special, so $s = 2$. I can count, but I need a formula.

The `at least' switches my thinking to complementary:
$\mathbb{P}(\text{at least }1\text{ special}) = 1 - \mathbb{P}(\text{none special})$.
Counting how many random pairs without vowel is easy: pick the pair from consonants! 
Thus $|\mathtt{E^{c}}| = C(5-2,2) = {3 \choose 2} = 3$ pairs: $\{b, c\}, \{b, d\}, \{c, d\}$.

Thus I remember the formula for this kind of sampling problem:
\[
\mathbb{P}(\text{sample size }k\text{ from population }n\text{ with }s\text{ special}) 
= 1 - \frac{C(n-s,k)}{C(n,k)} = 1 - \frac{{{n-s} \choose k}}{{n \choose k}}
\]
Note that the sample space $\mathtt{S}$ is the set of all possible samples.
% Mark and recapture  fish population.


\section{Binomial distribution}
What is the simplest dice God can throw? It is d2, which is effectively a coin, with head $\mathtt{H}$ and tail $\mathtt{T}$.
God's coin determines your fate: `success' or `failure', and each toss is called a `trial'. Since this is God's coin, so we don't know if it is fair.
Let $p = \mathbb{P}(\text{success})$ of such a coin,
and $q = 1 - p = \mathbb{P}(\text{failure})$.

Let event $\mathtt{E} = n$ trials with $k$ success. Traditionally, head denotes success, so a typical run in the event $\mathtt{E}$ looks like: 
$\mathtt{HTTHHTTTHHTHTHTHHTTTHTHH}$. We compute $\mathbb{P}(\mathtt{E})$ as follows:
\begin{itemize}
\item There are $C(n,k) = \displaystyle{n \choose k}$ such runs (choose $k$ heads in $n$ trials),
\item Each trial is independent, so $\mathbb{P}(\text{run}) = p^{k}q^{(n-k)}$ (C2 slide 17),
\item The runs are mutually disjoint, so $\mathbb{P}(\mathtt{E}) = \displaystyle{n \choose k} \mathbb{P}(\text{run})$ (C2 slide 13).
\end{itemize}
This gives the formula for binomial probabilities:
\[
\mathbb{P}(n\text{ trials with }k\text{ success}) = {n \choose k}p^{k}(1 - p)^{n - k}
\]


\noindent
Define a random variable $X$: number of success in $n$ trials,
then $\mathtt{E} = \{X = k\}$.
The function $\mathbb{P}(\{X = k\})$ has the following picture ($n = 40$):

\smallskip
\begin{tikzpicture}[scale=0.8, % using a scale
    declare function={binom(\k,\n,\p)=\n!/(\k!*(\n-\k)!)*\p^\k*(1-\p)^(\n-\k);}
]
\begin{axis}[
    samples at={0,...,40},
    yticklabel style={
        /pgf/number format/fixed,
        /pgf/number format/fixed zerofill,
        /pgf/number format/precision=1
    },
    ybar=0pt, bar width=1,
    legend style={at={(1,0.87)},anchor=west}
]
\addplot [fill=orange, fill opacity=0.5] {binom(x,40,0.2)}; \addlegendentry{$p=0.2$}
\addplot [fill=blue, fill opacity=0.5] {binom(x,40,0.5)}; \addlegendentry{$p=0.5$}
\addplot [fill=green, fill opacity=0.5] {binom(x,40,0.7)}; \addlegendentry{$p=0.7$}
\end{axis}
\end{tikzpicture}

\noindent
For a range of successes in $n$ trials,
\[
\begin{array}{rl}
 & \mathbb{P}(X: \text{more than }a\text{ but less than }b\text{ sixes from }n\text{ throws of a standard dice})\\
= & \mathbb{P}(\{a < X < b\})
= \displaystyle{\sum_{a < k < b} {n \choose k}\mathbb{P}(\text{run})}\\
= & \displaystyle{\sum_{0 < k < b} {n \choose k}\mathbb{P}(\text{run}) - \sum_{0 < k \le a} {n \choose k}\mathbb{P}(\text{run})}\\
= & \mathbb{P}(\{X < b\}) - \mathbb{P}(\{X \le a\})
\end{array} 
\]
% http://stattrek.com/online-calculator/binomial.aspx
% p = 1/6, n = 30, X = 8, X = 2
% P(2 < X < 8) = P(X < 8) - P(X <= 2) = 0.88631319778 - 0.10279037287 = 0.7835228249099999
% P(X < 8) = 0.88631319778 = P(X <= 7)
% P(X <= 2) = 0.10279037287 = P(X < 3)
% P(3<= X<= 7) = P(X <= 7) - P(X < 3) = 0.88631319778 - 0.10279037287 = 0.7835228249099999
The cumulative probabilites $\mathbb{P}(\{X < x\})$ or $\mathbb{P}(\{X \le x\})$, can be obtained from a binomial distribution calculator.
For example, with $n = 30$ and $p = \frac{1}{6}$ (standard dice), StatTrek (\url{http://stattrek.com/online-calculator/binomial.aspx}) gives for the range $2 < X < 8$
:
\begin{verbatim}
P(2 < X < 8) = P(X < 8) - P(X <= 2)
             = 0.88631319778 - 0.10279037287 = 0.7835228249099999
\end{verbatim}
In Wolfram Alpha the sum can be computed directly, with exact boundaries:
\begin{verbatim}
sum C(n,k)p^k(1 - p)^(n - k) //N from k = 3 to 7 for n = 30, p = 1/6
\end{verbatim}


\newpage
\section{Answers}
\textbf{Exercise 1} (page~\pageref{q:ex-1}).
% Can you simulate a d8 using d6 throws?
Since the sample space of d8 has size $8 = 2^{3}$,
take $3$ throws of d6 with $(a, b, c)$:
\[
\begin{array}{rrr|c|l}
   (a \bmod{2}) & (b \bmod{2}) & (c \bmod{2}) &
   \text{base }2 & v = 1 + 4(a \bmod{2}) + 2(b \bmod{2}) + (c \bmod{2})\\
\hline
   0 & 0 & 0 & \mathtt{000} & 1\\
   0 & 0 & 1 & \mathtt{001} & 2\\
   0 & 1 & 0 & \mathtt{010} & 3\\
   0 & 1 & 1 & \mathtt{011} & 4\\
   1 & 0 & 0 & \mathtt{100} & 5\\
   1 & 0 & 1 & \mathtt{101} & 6\\
   1 & 1 & 0 & \mathtt{110} & 7\\
   1 & 1 & 1 & \mathtt{111} & 8\\
\end{array}
\]

\bigskip
\noindent
\textbf{Exercise 2} (page~\pageref{q:ex-2}).
% Can you simulate a d9 using d6 throws?
Since the sample space of d9 has size $9 = 3^{2}$,
take $2$ throws of d6 with $(a, b)$, and use modulo $3$:
\[
\begin{array}{rr|c|l}
   (a \bmod{3}) & (b \bmod{3}) & \text{base }3 & v = 1 + 3(a \bmod{3}) + (b \bmod{3})\\
\hline
   0 & 0 & \mathtt{00} & 1\\
   0 & 1 & \mathtt{01} & 2\\
   0 & 2 & \mathtt{02} & 3\\
   1 & 0 & \mathtt{10} & 4\\
   1 & 1 & \mathtt{11} & 5\\
   1 & 2 & \mathtt{12} & 6\\
   2 & 0 & \mathtt{20} & 7\\
   2 & 1 & \mathtt{21} & 8\\
   2 & 2 & \mathtt{22} & 9\\
\end{array}
\]

\end{document}

Probability

Q3.

\begin{itemize}
\item Sample size $|S| = {n \choose k}$.
\item Reduced sample size (those not special) $|E^{c}| = {(n - s) \choose k}$.
\item $\mathbb{P}(E) = 1 - |E^{c}|/|S| = 1 - \frac{{(n - s) \choose k}}{{n \choose k}}$.
\end{itemize}
Computing by $1$-special, $2$-specials, \emph{etc.} is too tedious?

Verify directly count and complementary count.

Q4.
A picture of the distribution (Bell shape curve)
\url{http://stattrek.com/online-calculator/binomial.aspx}
\[
\mathbb{P}(k\text{ success}) = {n \choose k}p^{k}(1 - p)^{n-k}
\]
\begin{verbatim}
let p = 0.4 in C(n,k) p^{k} (1 - p)^{n - k}

DiscretePlot[
 Table[PDF[BinomialDistribution[40, p], k], {p, {0.1, 0.5, 0.7}}] // 
  Evaluate, {k, 36}, PlotRange -> All, PlotMarkers -> Automatic]
\end{verbatim}
This gives the Probability Density Function (PDF), and Cumulative Distribution Function (CDF).
\end{verbatim}

Q5.
\noindent
Hence from the matrix of probabilities $p_{i,j}$,
\begin{itemize}
\item sum along row to get $\mathbb{P}(\{ X = i \})$. Part $(b)$ is $\mathbb{P}(\{X = 2\})$.
\item sum along column to get $\mathbb{P}(\{ Y = j \})$. Part $(c)$ is $\mathbb{P}(\{Y = 2\})$.
\item The events $\{ X = 2 \}$ and $\{ Y = 2 \}$ are independent, by product check.
\item The variables $X, Y$ are independent iff all products check.
\end{itemize}


HERE;

% Why group them together?

% The lectures introduce matrix, and treat vector as 1xn (row vector) or nx1 (column vector) matrices.
% I do the reverse: talk about vectors first, then matrices.

% Vector = list of numbers  (no geometry for this course)
% arithmetic on vector: add, scale -- that's it: no multiply, no divide.
% linearity (B3 slide 10) is based on these two.
% filters are linear functions (check!) B3 slide 10
% system of equations is linear (check!)  B3 slide 11

% Transpose of a vector: row to column, column to row.
% row-by-column multiplication: row . column
% The sum of natural numbers:   [1,..,n] [1,..,1]ᵗ = n(n+1)/2
% The sum of natural squares:   [1,..,n] [1,..,n]ᵗ = n(n+1)(2n+1)/6
% Can invent some symbols here. N², even N 
% [1,2,3] . [1,2,3]

% Matrix = array of numbers
% arithmetic on matrix: add, scale,  hence matrix is linear, too.
% matrix multiplication: row-by-column, dimension matching.
% [[1,2],[3,4]] [[1,2],[3,4]]    (dot or no dot)

% Matrix and vector.
% Matrix equation: Ax = b.
% Matrix inverse:   x = A⁻¹ b.
% Matrix determinant for 2x2 inverse:   A⁻¹ = formula using det(A).
% What this means: a square matrix with det A = 0 has no inverse. (B3 slide 21)

% Population dynamics: iteration by matrix
% M = A B A⁻¹, so Mⁿ = A Bⁿ A⁻¹



GAss04 Q2†
This question provides information for merge sort, M(r) the maximum comparisons, and m(r) the minimum comparisons. m(r) has an explicit formula. Can you find the explicit formula for M(r)?

solve M(r) = 2^r − 1 + 2 * M(r−1)
M(r) = 2^(r-1)(c1 + 2r - 2) + 1 = 2^(r-1)2(r - 1) + 1 = 2^r(r-1) + 1
M(0) = 1/2(c1 - 2) + 1 = 0 ⇒ c1 = 0
solve m(r) = 2^(r-1) + 2 * m(r−1)
m(r) = 2^(r-1)(c1 + r) = 2^(r-1) r = r 2^(r-1).
m(0) = 1/2(c1 + 0) = 0 ⇒ c1 = 0


B. R. Heap in 1963.[1] 
Heap, B. R. (1963). "Permutations by Interchanges" (PDF). The Computer Journal. 6 (3): 293–4. doi:10.1093/comjnl/6.3.293.
Heap's algorithm for generating permutations
https://en.wikipedia.org/wiki/Heap%27s_algorithm

// check if n is even
function even(n) {
   return ((n % 2) == 0);
}
// test:
even(3);
even(4);

// swap array a[i] and a[j]
function swap(a, i, j) {
   var t = a[i];
   a[i] = a[j];
   a[j] = t;
   return a;
}
// test:
swap([1,2,3,4], 1, 2);
swap([1,2,3,4], 0, 3);

// generate all permutations of an array a.
function gen_perm(a) {
    var n = a.length; // get length
    var b = []; // encoding of stack state
    var j;
    for (j = 0; j < n; j++) b[j] = 0;
    var c = 0; // the count
    print("a: " + a); c++;
    j = 0; // reset j, the stack pointer
    while (j < n) {
       if (b[j] < j) {
          if (even(j)) {
             swap(a, 0, j); // swap a[0],a[j]
          }
          else {
             swap(a, b[j], j); // swap a[b[j]], a[j]
          }
          print("a: " + a); c++;
          b[j] = b[j] + 1;
          j = 0;
       }
       else {
          b[j] = 0;
          j = j + 1;
       }
    }
    print("Done. "+n+"! = "+c);
}
// run:
gen_perm([1,2,3]);
a: 1,2,3
a: 2,1,3
a: 3,1,2
a: 1,3,2
a: 2,3,1
a: 3,2,1
Done. 3! = 6

gen_perm([1,2,3,4]);
Done. 4! = 24

Algorithms: Generating Combinations 
https://dev.to/rrampage/algorithms-generating-combinations-100daysofcode-4o0a

// generate combinations of n choose k.
function comb(n, k) {
   var a = [];
   var j; // initialize first combination
   for (j = 0; j < k; j++) a[j] = j;
   var c = 0; // the count;

   j = k - 1; // Index to keep track of maximum unsaturated element in array
   // a[0] can only be n-k+1 exactly once - our termination condition!
   while (a[0] < n - k + 1) {
      // If outer elements are saturated, keep j-- till you find unsaturated element
      while (j > 0 && a[j] == n - k + j) {
          j--;
      }
      print("a: " + a); c++;
      a[j]++;
      // Reset each outer element to prev element + 1
      while (j < k - 1) {
          a[j + 1] = a[j] + 1;
          j++;
      }
   }
   print("Done. C("+n+","+k+") = "+c);
}
// run as:
comb(3,2);
a: 0,1
a: 0,2
a: 1,2
Done. C(3,2) = 3

comb(5,2);
a: 0,1
a: 0,2
a: 0,3
a: 0,4
a: 1,2
a: 1,3
a: 1,4
a: 2,3
a: 2,4
a: 3,4
Done. C(5,2) = 10




% pdflatex info05.tex