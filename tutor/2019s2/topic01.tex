\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amssymb} % for therefore
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams
% \usepackage{graphicx}
\usepackage{caption}   % for caption options
\usepackage{pgfplots}  % to plot graphs
\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\usepgfplotslibrary{fillbetween} % for filling
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
\usepackage[hang,flushmargin]{footmisc} % remove footnote indentation.

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}
% \usepackage[a4paper, total={6in, 8in}]{geometry} % for margin adjustment
\usepackage{titling} % for \droptitle

% encircle text
\newcommand\encircle[1]{%
  \tikz[baseline=(X.base)] 
    \node (X) [draw, shape=circle, inner sep=0] {\strut #1};}

\setlength{\droptitle}{-7em}   % This is your set screw, more -ve higher
\title{Math Topics: Animation, Vectors and Calculus}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Animation}
\label{sec:animation}
Animation begins with drawing: say a flying dragon.
You draw using a pencil, or using a computer software with mouse controlling a cursor, even a smartphone app with your finger. The pencil tip, cursor tip, or finger tip, marks a \textbf{point}. The movement of the point traces out a \textbf{path}. Connecting the paths gives you the shape -- a dragon. Repeat the drawing, either by hand or by computer, by varying the shape: translate, rotate, enlarge, shrink, even shear or reflect, called transformtions. Finally, put the drawings into frames, and you have an animation!

\section{Line}
\label{sec:line}
You probably don't want to do by hand, but then you need math to do that by computer.
To simplify the math, consider paths that are straight: each is a \textbf{line}.
Let the dragon shrink to a point $P$, moving within a sheet of paper, or on the 2D screen.
The point $P$ has coordinates $(x,y)$ with respect to the $x$-axis and $y$-axis.
Both will change with time $t$, for example,
\begin{equation}
\label{eqn:line-parameter}
% original: x = 2 + 3t
%           y = 1 + 2t
% xmin = -0.5, xmax = 22, ymin = 0, ymax = 15,
\begin{cases}
x = 2 + t \\
y = 3 + 2t
\end{cases}
\end{equation}
From~(\ref{eqn:line-parameter}),
the dragon position $P = (x,y)$ at various time $t$ can be computed:\\

\begin{tabular}{r|rrrr}
t & 0 & 1 & 2 & 3\\
\hline
x & 2 & 3 & 4 & 5\\
y & 3 & 5 & 7 & 9\\
\end{tabular}

\bigskip
\noindent
showing that our dragon $P$ moves in a straight line:

\begin{tikzpicture}[scale=1.0]
\tikzstyle{dot}=[circle,fill=black,minimum size=5pt,inner sep=0pt]
\begin{axis}[
 axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
  % xmin = -0, xmax = 1.2,
  ymin = -2, ymax = 10,
    % xtick={-2,-1,0,1,2}, ytick={-2,-1,0,1,2,3},
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = 2x - 1
\addplot[domain=0:6]{2 * x - 1};
% points
\node (p) [dot] at (axis cs: 2,3) {}; \node [anchor=west] at (p) {$t = 0$};
\node (p) [dot] at (axis cs: 3,5) {}; \node [anchor=west] at (p) {$t = 1$};
\node (p) [dot] at (axis cs: 4,7) {}; \node [anchor=west] at (p) {$t = 2$};
\node (p) [dot] at (axis cs: 5,9) {}; \node [anchor=west] at (p) {$t = 3$};
\end{axis}
\end{tikzpicture}

\noindent
Solving $t$ in terms of $x$, \emph{i.e.}, $t = x - 2$,
then $y = 3 + 2(x-2) = 2x - 1$.
That is: $2x - y = 1$, perhaps your familiar form of a line.

\paragraph{Exercise}
Draw the line $L: 2x - y = 1$.
\paragraph{Solution}
Put $L$ into intercept form:
$\displaystyle{\frac{x}{1/2} + \frac{y}{-1} = 1}$.\\
This shows $x$-intercept $= 1/2$ when $y = 0$,
and $y$-intercept $= -1$ when $x = 0$.
Hence:

\begin{tikzpicture}[scale=1.0]
\begin{axis}[
 axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -1, xmax = 3, ymin = -2, ymax = 5,
    xtick={-2,-1,1,2,3,4,5}, ytick={-2,-1,1,2,3,4,5},
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = 2x - 1
\addplot[domain=-1:3]{2 * x - 1};
\node[anchor=west] at (axis cs:1,1) {$L$};
\end{axis}
\end{tikzpicture}

\section{Vector}
\label{sec:vector}
Computers cannot see the geometry.
They prefer numbers, and recast~(\ref{eqn:line-parameter}) as:
\begin{equation}
\label{eqn:line-vector-example}
\left[\begin{array}{r}x\\y\end{array}\right] =
\left[\begin{array}{r}2\\3\end{array}\right] +
t \left[\begin{array}{r}1\\2\end{array}\right]
\end{equation}
which can be interpreted using vectors:

\begin{tikzpicture}[scale=1.0]
\begin{axis}[
 axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
    % xmin = 0, xmax = 3.5, ymin = -1, ymax = 6,
    xtick={-2,-1,1,2,3,4,5,6}, ytick={-1,1,2,3,4,5,6,7},
    grid=both,
    % extra x ticks={0}, % <--- 0 at x
    % extra y ticks={0}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = 2x - 1
\addplot[domain=0:4]{2 * x - 1};
% vectors
    \coordinate (O) at (axis cs: 0,0);
    \coordinate (A) at (axis cs: 2,3);
    \coordinate (B) at (axis cs: 3,5);
    \coordinate (L) at (axis cs: 1,1);
    \coordinate (P) at (axis cs: 4,7);
    \node[anchor=east] at (O) {$O$};
    \draw[->,very thick] (O)--(A) node[anchor=west]{$A$};
    \draw[->,very thick] (A)--(B) node[anchor=west]{$B$};
    \node[anchor=west] at (L) {$L$};
    \node[anchor=north] at (P) {$P$};
\end{axis}
\end{tikzpicture}

\noindent
Any point $P$ on the line $L$ can be reached by vector addition:
\begin{equation}
\label{eqn:line-vector}
\overrightarrow{OP} = \overrightarrow{OA} + t\; \overrightarrow{AB}
\end{equation}
This means $\overrightarrow{OP} = \left[\begin{array}{r}x\\y\end{array}\right]$,
$\overrightarrow{OA} = \left[\begin{array}{r}2\\3\end{array}\right]$
and slope $\overrightarrow{AB} = \left[\begin{array}{r}1\\2\end{array}\right]$.
Note that the slope $\overrightarrow{AB}$ can be found by identifying the point $B$,
so that $\overrightarrow{OB} = \left[\begin{array}{r}3\\5\end{array}\right]$,
and $\overrightarrow{AB} = \overrightarrow{OB} - \overrightarrow{OA}
= \left[\begin{array}{r}3\\5\end{array}\right] - \left[\begin{array}{r}2\\3\end{array}\right]
= \left[\begin{array}{r}1\\2\end{array}\right]$.

\paragraph{Exercise}
Draw the vector $\overrightarrow{OB}$, and use head-to-tail vector addition
to verify that $\overrightarrow{OB} = \overrightarrow{OA} + \overrightarrow{AB}$.

\bigskip
\noindent
With numbers, computer can manipulate vectors in $3$D.
For example, let the horizontal ground be the $x-y$ plane.
A vertical pole $AB$ has base $B$ on the ground,
and a light ray $L$ strikes the tip $A$ and hits the ground at $C$.
How to represent the $3$D line $L$ in computer?
% Or, a dragon perches at the top A, and spits fire at C on the ground.

% picture of pole with shadow
\begin{tikzpicture}[scale=1.0]
    % for xy horizontal, and z vertical, coordinate is (y,z,x).
    \coordinate (O) at (0, 0, 0);
    \draw[thick,->] (O) --++ (0,0,10) node[anchor=east]{$x$};
    \draw[thick,->] (O) --++ (4.5,0,0) node[anchor=west]{$y$};     
    \draw[thick,->] (O) --++ (0,4.5,0) node[anchor=south]{$z$};     
    % verify by unit vectors
    \coordinate (i) at (0,0,1);
    \coordinate (j) at (1,0,0);
    \coordinate (k) at (0,1,0);
    \draw[->,red,thick] (O)--(i) node[anchor=west]{$i$};
    \draw[->,red,thick] (O)--(j) node[anchor=west]{$j$};
    \draw[->,red,thick] (O)--(k) node[anchor=west]{$k$};

    \coordinate (A) at (3,5,3); % for (3,3,5)
    \coordinate (B) at (3,0,3); % for (3,3,0)
    \coordinate (C) at (0,0,8); % for (8,0,0)
    \draw[->,very thick] (O)--(A) node[anchor=west]{$A$};
    \draw[->,very thick] (O)--(B) node[anchor=west]{$B$};
    \draw[->,very thick] (O)--(C) node[anchor=north]{$C$};
    % \draw[->] (A) -- (C);
    \draw [thick] ($(C)!-1cm!0:(A)$) -- ($(C)!10cm!0:(A)$) node[anchor=east]{$L$};
    \node[anchor=east] at (O) {$O$};
    \draw[blue,very thick] (A) -- (B); % the pole
    \draw[dashed] (0,0,3) -- (B) -- (3,0,0); 
    \draw[dotted] (C) -- (B) -- (O);

    % plane containing 3 points: A B C.
    % \filldraw[fill=blue!10, opacity=0.6] (A) -- (B) --  (C) -- (A);

    % determine D: OA + AD = OD, and AD = BC, so D = (8,0,5)
    \coordinate (D) at (0,5,8); % for (8,0,5)
    \node[anchor=east] at (D) {$D$};
    % plane containing 3 points: A B C, with D on the plane.
    \filldraw[fill=blue!10, opacity=0.6] (A) -- (B) -- (C) --  (D) -- (A);

\end{tikzpicture}

\noindent
Note the ray $L$ contains two points $A$ and $C$.
The slope of $L$, also called its \textbf{direction},
is given by $\overrightarrow{AC}$.
Using the same idea in (\ref{eqn:line-vector}),
any point $P$ on the ray $L$ is given by the vector equation:
\[
\overrightarrow{OP} = \overrightarrow{OA} + t\;\overrightarrow{AC}
\qquad\text{with }t \in \mathbb{R}.
\]
% If A = (a,a,h), C = (k,0,0), then AC = (k-a,a,h), and P = (a,a,h) + t(k-a,a,h).
% Also, B = (a,a,0), BC = OC - OB = (k,0,0) - (a,a,0) = (k-a,-a).
% Put a = 3, k = 7, then |BC| = sqrt(4^2 + 3^2) = 5.  A = (3,3,7), C = (7,0,0)

\paragraph{Exercise}
If the light ray hits the pole tip $A = (3,3,7)$ and the ground at $C = (7,0,0)$,
find the parametric equation of the light ray, 
and the length of the shadow of the vertical pole $AB$.

\noindent
We can draw $2$D vectors, see $3$D vectors, but we can only imagine vectors in higher dimensions.
However, vectors are just columns of numbers, with just two properties:
\begin{enumerate}
\item Vector addition: $\overrightarrow{\mathtt{u}} + \overrightarrow{\mathtt{v}}$, component by component,
\item Scalar multiplication: $\lambda\overrightarrow{\mathtt{v}}$, multiply each component by scalar $\lambda$.
\end{enumerate}
These operations can readily be handled by computers. Geometrically,

\begin{tikzpicture}[scale=0.5]
    \draw [gray,thin] (0,0) grid (5,5);
    \draw [thick,red,<->] (6,0) node [above] {$x$}
              -- (0,0) -- (0,6) node [right] {$y$};
    \coordinate (O) at (0,0);
    \coordinate (A) at (4,1);
    \coordinate (B) at (1,3);
    \coordinate (C) at (5,4);
    \draw [->,blue,ultra thick] (O) -- (A)
      node[anchor=west] {$\overrightarrow{\mathtt{u}}$};
    \draw [->,blue,ultra thick] (O) -- (B)
      node[anchor=south] {$\overrightarrow{\mathtt{v}}$};
    \draw [->,green!60!black,ultra thick] (O) -- (C)
      node[anchor=west] {$\overrightarrow{\mathtt{u}} + \overrightarrow{\mathtt{v}}$};
    \filldraw[fill=red, fill opacity=0.3, draw=black] (O) -- (A) -- (C) -- (B) -- (O);
\end{tikzpicture}
\begin{tikzpicture}[scale=0.5]
    \draw [gray,thin] (0,0) grid (5,5);
    \draw [thick,red,<->] (6,0) node [above] {$x$}
              -- (0,0) -- (0,6) node [right] {$y$};
    \coordinate (O) at (0,0);
    \coordinate (A) at (4,1);
    \coordinate (B) at (1,3);
    \coordinate (C) at (5,4);
    \draw [->,blue,ultra thick] (O) -- (A);
    \node[anchor=north] at (3.3,1) {$\overrightarrow{\mathtt{u}}$};
    \draw [->,blue,ultra thick] (A) -- (C);
    \node[anchor=south] at (5,2) {$\overrightarrow{\mathtt{v}}$};
    \draw [->,green!60!black,ultra thick] (O) -- (C);
    \node[anchor=east] at (3.5,3) {$\overrightarrow{\mathtt{u}} + \overrightarrow{\mathtt{v}}$};
    \filldraw[fill=red, fill opacity=0.3, draw=black] (O) -- (A) -- (C) -- (O);
\end{tikzpicture}
\begin{tikzpicture}[scale=0.5]
    \draw [gray,thin] (0,0) grid (5,5);
    \draw [thick,red,<->] (6,0) node [above] {$x$}
              -- (0,0) -- (0,6) node [right] {$y$};
    \coordinate (O) at (0,0);
    \coordinate (A) at (2,1.6);
    \coordinate (C) at (5,4);
    \draw [->,blue,ultra thick] (O) -- (A)
      node[anchor=west] {$\overrightarrow{\mathtt{v}}$};
    \draw [->,green!60!black,ultra thick] (O) -- (C)
      node[anchor=west] {$\lambda\overrightarrow{\mathtt{v}}$};
\end{tikzpicture}

\bigskip
\noindent
Further investigation involves more lines, their intersection,
spanning planes and linear combination and linear independence.
These are studied in linear algebra.

% Curves:
% Folium of Descartes
% Cartesian: x^3 + y^3 = 3xy
% Parametric: let y = xt, then show: x = 3t/(1+t^2), y = 3t^2/(1+t^2)

% Parabola: x^2 - 1 = (x + 1)(x - 1)  for dragon to dive and soar.

% line -> lines -> system of equations
% A line has a constant slope. A curve has changing slopes.
% curve -> slope curve   differentiation
% slope curve -> curve   integeration
% relate back to stop-motion animation?

\section{Curve}
\label{sec:curve}
Dragons usually don't fly straight -- they dive into water, soar back to sky.
\begin{equation}
\label{eqn:curve-parameter}
\begin{cases}
x = t - 2 \\
y = (t - 1)(t - 3)
\end{cases}
\end{equation}
Some typical positions are:

\begin{tabular}{r|rrrrr}
t & 0 & 1 & 2 & 3 & 4\\
\hline
x & -2 & -1 &  0 & 1 & 2\\
y &  3 &  0 & -1 & 0 & 3\\
\end{tabular}

% For y = x^2 - 1.  x = t - 2, y = t^2 - 4t + 4 - 1 = t^2 - 4t + 3 = (t - 1)(t - 3)
\noindent
Alternatively, we can solve $t = x + 2$,\\
thus $y = (x + 2 - 1)(x + 2 - 3) = (x + 1)(x - 1) = x^{2} - 1$.

\bigskip
\begin{tikzpicture}[scale=1.0]
\tikzstyle{dot}=[circle,fill=black,minimum size=5pt,inner sep=0pt]
\begin{axis}[
 axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -3, xmax = 3.2, ymin = -2, ymax = 4,
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0,1,3,-1}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = (x^2 - 1)
\addplot[domain=-3:3, thick, smooth]{(x * x - 1)};
\node[anchor=west] at (axis cs: 2.2,3.8) {$x^{2} - 1$};
% points
\node (p) [dot] at (axis cs: -2,3) {}; \node [anchor=east] at (p) {$t = 0$};
\node (p) [dot] at (axis cs: -1,0) {}; \node [anchor=south east] at (p) {$t = 1$};
\node (p) [dot] at (axis cs: 0,-1) {}; \node [anchor=north west] at (p) {$t = 2$};
\node (p) [dot] at (axis cs: 1,0) {}; \node [anchor=south west] at (p) {$t = 3$};
\node (p) [dot] at (axis cs: 2,3) {}; \node [anchor=west] at (p) {$t = 4$};
\end{axis}
\end{tikzpicture}

\section{Calculus}
\label{sec:calculus}
Again, computers cannot see curves. While two points determine a line, many points make up a curve. To teach a computer about curves, we chop up a curve into pieces --- each piece approximates a line, and the pieces together approximate the curve. This is the essential idea behind calculus.

\subsection{Slope Function}
\label{sec:slope-function}
A line has a constant slope, but a curve has a varying slope.
In general, the slope of a function $f(x)$ is called its \textbf{derivative}:
\[
\displaystyle\frac{df(x)}{dx} = \lim_{h\rightarrow{0}}\frac{f(x + h) - f(x)}{h}
\]

\noindent
For the path of the dragon flight, its slope changes as follows:

\noindent
\begin{tikzpicture}% [scale=0.8] move inside
\begin{axis}[
    % xmin = -3, xmax = 3.2, ymin = -1, ymax = 8,
    xtick = \empty,    ytick = \empty,
    xlabel = {$x$},
    x label style = {at={(1,0)},anchor=west},
    ylabel = {$y$},
    y label style = {at={(0,1)},rotate=-90,anchor=south},
    axis lines=center,
    scale=0.8,
    enlargelimits=0.2,
]
% f(x) = x^2 - 1
\addplot[color=black,smooth,thick,-,domain=-3:3] {(x)^2 - 1};
\addplot[color=blue,mark=*,label={right:$P$}] (2,3);
\addplot[mark=none, blue] coordinates {(1,-1) (3,7)};
% 2x, (y-3)/(x-2) = 4, y = 4x - 8 + 3 = 4x - 5
% \addplot[mark=none, green!60!black] coordinates {(1,-0.6) (3,6.6)};
% 1.8x, (y-3)/(x-2) = 3.6, y = 3.6x - 7.2 + 3 = 3.6x - 4.2
\draw[red] (axis cs:2,3) -- node[anchor=north] {$h$} (axis cs:2.5,3)
                         -- node[anchor=west] {$\Delta{f}$} (axis cs:2.5,5.25);
\node[anchor=west] at (axis cs: -2.5,6) {$f(x)$};
% f'(x) = 2x
\addplot[dashed,domain=-1:1.5] {2 * x};
% \addplot[mark=none, dashed] coordinates {(-3,-6) (3,6)};
\node[anchor=west] at (axis cs: -0.5,2) {$f'(x)$};
\node[anchor=north west] at (axis cs: 0,0) {$0$};
\end{axis}
\end{tikzpicture}
\begin{tikzpicture}% [scale=0.8] move inside
\draw[draw=none] (0,0) grid (0.1,0.1); % need dummy grid
\node[anchor=south] at (0,2) {$\underrightarrow{\frac{df}{dx}}$};
\end{tikzpicture}
\begin{tikzpicture}
\begin{axis}[
    % xmin = -3, xmax = 3.2, ymin = -1, ymax = 8,
    xtick = \empty,    ytick = \empty,
    xlabel = {$x$},
    x label style = {at={(1,0)},anchor=west},
    ylabel = {$y$},
    y label style = {at={(0,1)},rotate=-90,anchor=south},
    axis lines=center,
    scale=0.8,
    enlargelimits=0.2,
]
% f'(x) = 2x
\addplot[domain=-1:4,thick] {2 * x};
% \addplot[mark=none, dashed] coordinates {(-3,-6) (3,6)};
\node[anchor=west] at (axis cs: 1.5,6) {$f'(x)$};
\node[anchor=north west] at (axis cs: 0,0) {$0$};
\end{axis}
\end{tikzpicture}

\noindent
Note that, around $x = 0$,
the slope changes from negative on the left to positive on the right.
The derivative $f'(x) = 0$ at $x = 0$, at the minimum of $f$.


\paragraph{Exercise}
Show that $\displaystyle\frac{d(x^{2} - 1)}{dx} = 2x$ by definition.

\bigskip
\noindent
Note that the derivative is itself a function.
Thus differentiation \emph{transforms} a function to its slope function:

\noindent
\begin{tikzpicture}[scale=0.8]
\begin{axis}[
 % axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -2, xmax = 3.2, ymin = -3, ymax = 3,
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = x(x + 1)(x - 2) = x(x^2 - x - 2) = x^3 - x^2 - 2x
\addplot[domain=-2:3, thick, smooth]{x * (x + 1) * (x - 2)};
\node[anchor=west] at (axis cs: 2.5,2.8) {$f(x)$};
\end{axis}
\end{tikzpicture}
\begin{tikzpicture}
\draw[draw=none] (0,0) grid (0.1,0.1); % need dummy grid
\node[anchor=south] at (0,2) {$\underrightarrow{\frac{df}{dx}}$};
\end{tikzpicture}
\begin{tikzpicture}[scale=0.8]
\begin{axis}[
 % axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -2, xmax = 3.2, ymin = -3, ymax = 3,
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = 3x^2 - 2x - 2 = x(3x - 2) - 2
\addplot[domain=-2:3, thick, smooth]{x * (3 * x - 2) - 2};
\node[anchor=west] at (axis cs: 1.8,2.8) {$f'(x)$};
\end{axis}
\end{tikzpicture}

\noindent
Check the roots of $f'(x)$, corresponding to the points where the slopes of $f(x)$ is zero, \emph{i.e.}, at maximum and minimum.


\subsection{Area Function}
\label{sec:area-function}
The area under a curve can be computed as the limit of the Riemann sum:
\[
\displaystyle\int_{a}^{b} f(x)\; dx = 
\lim_{n\rightarrow\infty}\sum_{i=0}^{n}f(\xi)\left(\frac{b-a}{n}\right)
\qquad\text{where }x_{0} = a, x_{n} = b,\text{ and }x_{i} \le \xi \le x_{i+1}.
\]

% A Riemann sum plot
\begin{tikzpicture}[scale=1.2]
\def\a{1.7}
\def\b{5.7}
\def\c{3.7}
\def\L{0.5} % width of interval

\pgfmathsetmacro{\Va}{2*sin(\a r+1)+4} \pgfmathresult
\pgfmathsetmacro{\Vb}{2*sin(\b r+1)+4} \pgfmathresult
\pgfmathsetmacro{\Vc}{2*sin(\c r+1)+4} \pgfmathresult

\draw[->,thick] (-0.5,0) -- (7,0) coordinate (x axis) node[below] {$x$};
\draw[->,thick] (0,-0.5) -- (0,7) coordinate (y axis) node[left] {$y$};
\foreach \f in {1.7,2.2,...,6.2} {\pgfmathparse{2*sin(\f r+1)+4} \pgfmathresult
\draw[fill=blue!20] (\f-\L/2,\pgfmathresult |- x axis) -- (\f-\L/2,\pgfmathresult) -- (\f+\L/2,\pgfmathresult) -- (\f+\L/2,\pgfmathresult |- x axis) -- cycle;}
\node at (\a-\L/2,-5pt) {\footnotesize{$a=x_0$}};
\node at (\b+\L/2+\L,-5pt) {\footnotesize{$b=x_n$}};
\draw[blue] (\c-\L/2,0) -- (\c-\L/2,\Vc) -- (\c+\L/2,\Vc) -- (\c+\L/2,0);
\draw[dashed] (\c,0) node[below] {\footnotesize{$\xi_i$}} -- (\c,\Vc) -- (0,\Vc) node[left] {$f(\xi_i)$};
\node at (\a+5*\L/2,-5pt) {\footnotesize{$x_{i-1}$}};
\node at (\a+7*\L/2,-5pt) {\footnotesize{$x_i$}};
\node at (\a+5*\L,-5pt) {\footnotesize{$x_{i+1}$}};
\draw[blue,thick,smooth,samples=100,domain=1.45:6.2] plot(\x,{2*sin(\x r+1)+4});
\filldraw[black] (\c,\Vc) circle (.03cm);
\end{tikzpicture}

\paragraph{Exercise}
Show that $\displaystyle\int_{a}^{b}x\; dx = \frac{1}{2}(b + a)(b - a)$ by definition.


\section{Fundamental Theorem of Calculus}
\label{sec:fundamental-theorem}

% line -> another line  linear transformation
% for line through origin (otherwise affine transformation)

% Fundamental Theorem of Calculus
% A(x) = int 0 to x  f(t)dt ==> dA/dt = f       dA = f dt.
% int a to b F'(x) dx = F(b) - F(a)           dF = F'(x) dx.

\begin{tikzpicture}
\tikzset{edge/.style = {->,> = latex',semithick}}
\node (A) at (0,0)
  {\begin{tabular}{c}function $F$\\[1em]\hline\\area function $F(x) = \int_{0}^{x}f(t)\;dt$\end{tabular}};
\node (B) at (6,0)
  {\begin{tabular}{c}slope function $F'(x) = \frac{dF}{dx}$\\[1em]\hline\\function $f$\end{tabular}};
\draw[edge] (A) to[bend left] node[pos=0.5, above] {Differentiation} (B);
\draw[edge] (B) to[bend left] node[pos=0.5, below] {Integration} (A);
\end{tikzpicture}

\bigskip
\noindent
On the one hand, these arrows are definitions: differentiation gives derivative,
and integration gives area under curve.
On the other hand, these arrows show that integration and differentiation
are closely related.
Looking at the right way, these arrows show the Fundamental Theorem of Calculus.

% \newpage
For the integration arrow, going back from $F$ to $f$ by differentiation is the \emph{first form} of the Fundamental Theorem of Calculus.
For the differentiation arrow, going back from $F'$ to $F$ by integration is the \emph{second form} of the Fundamental Theorem of Calculus. 

\subsection{Fundamental Theorem of Calculus (first form)}
\label{sec:fundamental-theorem-a}

The differentiation of an integral is its integrand:

\bigskip
\begin{tikzpicture}
\tikzset{edge/.style = {->,> = latex',semithick}}
\node (A) at (0,0)
  {\begin{tabular}{c}area function $F(x) = \int_{0}^{x}f(t)\;dt$\end{tabular}};
\node (B) at (6,0)
  {\begin{tabular}{c}function $f$\end{tabular}};
\draw[edge,thick,blue] (A) to[bend left] node[pos=0.5, above] {Differentiation (theorem)} (B);
\draw[edge] (B) to[bend left] node[pos=0.5, below] {Integration (definition)} (A);
\end{tikzpicture}

% Derivative of Area
\begin{tikzpicture}[scale=1.0]
\begin{axis}[
 % axis equal, % ensure equal scales on both axes
 % major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -0.5, xmax = 4, ymin = -1, ymax = 5.5,
 ticks=none, % no ticks
    % grid=both,
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
\addplot[domain=0:4,red,thick,smooth,samples=100] {2*cos(deg(10 - x)) + 3};
\addplot[draw=none,name path=A] {2*cos(deg(10 - x)) + 3};
\addplot[draw=none,name path=B] {0};  % y = 0: x-axis
\addplot[yellow] fill between[of=A and B,soft clip={domain=0:3}]; % filling
% points
\node[anchor=north] at (axis cs:0,0) {$O$};
\node[anchor=north] at (axis cs:3.7,5) {$f$};
\node[anchor=north east] at (axis cs:3,0) {$x$};
\draw[blue] (axis cs:3,0) -- (axis cs:3,4.5); % 2*cos(deg(10 - 3)) + 3
\node[anchor=north west] at (axis cs:3.2,0.1) {$x + h$};
\draw[blue] (axis cs:3.2,0) -- (axis cs:3.2,4.7); % 2*cos(deg(10 - 3)) + 3
\draw[dashed] (axis cs:3.1,0) node[below] {\footnotesize{$\xi$}}
    -- (axis cs:3.1,4.6)
    -- (axis cs:0,4.6) node[left] {$f(\xi)$};
\filldraw[fill=red, fill opacity=0.3, draw=black]
     (axis cs:3,0) rectangle (axis cs:3.2,4.6);
\node[anchor=north] at (axis cs:2,2) {$F(x)$};
\end{axis}
\end{tikzpicture}

\noindent
Let $F(x) = \displaystyle\int_{0}^{x} f(x)\; dx$,
the yellow area under $f$ from $0$ to $x$, which varies as $x$ changes.
The derivative of this integral function is its integrand:
\[
\begin{array}{rll}
F'(x) = \displaystyle\frac{d F(x)}{dx} = &
     \displaystyle\lim_{h \rightarrow 0}\frac{F(x + h) - F(x)}{h} &
     \text{definition of derivative}\\[1em]
     = & \displaystyle{\lim_{h \rightarrow 0}
            \frac{\int_{0}^{x + h} f(x)\; dx - \int_{0}^{x} f(x)\; dx}{h}} &
     \text{definition of }F(x)\\[1em]
     = & \displaystyle{\lim_{h \rightarrow 0}
            \frac{\int_{x}^{x + h} f(x)\; dx}{h}} &
     \text{difference of areas}\\[1em]
     = & \displaystyle{\lim_{h \rightarrow 0}
            \frac{f(\xi) h}{h}} &
     \text{area of thin red rectangle, width }h\\[1em]
     = & \displaystyle\lim_{h \rightarrow 0} f(\xi) & \text{before }h \rightarrow 0\\[1em]
     = & f(x) & \text{since }x \le \xi \le x+h
\end{array}
\]

\subsection{Fundamental Theorem of Calculus (second form)}
\label{sec:fundamental-theorem-b}

The integral of a derivative is itself, almost\footnote{For indefinite integral, there is an integration constant $\mathtt{C}$. For definite integral, it is the funciton value difference.}:

\bigskip
\begin{tikzpicture}
\tikzset{edge/.style = {->,> = latex',semithick}}
\node (A) at (0,0)
  {\begin{tabular}{c}function $F$\end{tabular}};
\node (B) at (6,0)
  {\begin{tabular}{c}slope function $F'(x) = \frac{dF}{dx}$\end{tabular}};
\draw[edge] (A) to[bend left] node[pos=0.5, above,thick] {Differentiation (definition)} (B);
\draw[edge,thick,blue] (B) to[bend left] node[pos=0.5, below] {Integration (theorem)} (A);
\end{tikzpicture}

% Summation of changes
\begin{tikzpicture}[scale=1.0]
\begin{axis}[
 % axis equal, % ensure equal scales on both axes
 % major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -1, xmax = 4, ymin = -1, ymax = 5.5,
    ticks=none, % no ticks
    % grid=both,
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
\addplot[domain=0:4,red,thick,smooth,samples=100] {2*cos(deg(10 - x)) + 3};
% points
\node[anchor=north] at (axis cs:0,0) {$O$};
\node[anchor=north] at (axis cs:3.7,5) {$F$};
\draw[dashed] (axis cs:1.3,0) node[below] {$a$}
    -- (axis cs:1.3,1.5)
    -- (axis cs:0,1.5) node[left] {$F(a)$};
\draw[dashed] (axis cs:2.9,0) node[below] {$b$}
    -- (axis cs:2.9,4.4)
    -- (axis cs:0,4.4) node[left] {$F(b)$};
\draw[green!60!black] (axis cs:1.3,1.5) -- (axis cs:1.5,1.5);
\draw[blue, thick]  (axis cs:1.5,1.5) -- (axis cs:1.5,1.8);
\draw[green!60!black] (axis cs:1.5,1.8) -- (axis cs:1.7,1.8);
\draw[blue, thick]  (axis cs:1.7,1.8) -- (axis cs:1.7,2.1);
\draw[green!60!black] (axis cs:1.7,2.1) -- (axis cs:1.9,2.1);
\draw[blue, thick]  (axis cs:1.9,2.1) -- (axis cs:1.9,2.5);
\draw[green!60!black] (axis cs:1.9,2.5) -- (axis cs:2.1,2.5);
\draw[blue, thick]  (axis cs:2.1,2.5) -- (axis cs:2.1,2.9);
\draw[green!60!black] (axis cs:2.1,2.9) -- (axis cs:2.3,2.9);
\draw[blue, thick]  (axis cs:2.3,2.9) -- (axis cs:2.3,3.3);
\draw[green!60!black] (axis cs:2.3,3.3) -- (axis cs:2.5,3.3);
\draw[blue, thick]  (axis cs:2.5,3.3) -- (axis cs:2.5,3.7);
\draw[green!60!black] (axis cs:2.5,3.7) -- (axis cs:2.7,3.7);
\draw[blue, thick]  (axis cs:2.7,3.7) -- (axis cs:2.7,4.1);
\draw[green!60!black] (axis cs:2.7,4.1) -- (axis cs:2.9,4.1);
\draw[blue, thick]  (axis cs:2.9,4.1) -- (axis cs:2.9,4.4);
\node[anchor=north] at (axis cs:2.1,2.5) {$h$};
\node[anchor=west] at (axis cs:2.1,2.7) {$\Delta{F}$};
\end{axis}
\end{tikzpicture}

\noindent
The definite integral of a derivative is the difference of its function values:
\[
\begin{array}{rll}
 \displaystyle\int_{a}^{b} F'(x)\; dx = & 
  \displaystyle\int_{a}^{b} \lim_{h \rightarrow 0}\frac{\Delta{F}}{h}\; dx &
  \text{definition of derivative}\\[1em]
 = & \displaystyle\lim_{h \rightarrow 0}\sum_{a}^{b}\frac{\Delta{F}}{h}\; h &
  \text{Riemann Sum of integral}\\[1em]
 = & \displaystyle\lim_{h \rightarrow 0}\sum_{a}^{b}\Delta{F} &
  \text{before } h \rightarrow 0\\[1.5em]
 = & F(b) - F(a) &
  \text{limit sum of vertical }\Delta{F}
\end{array}
\]

% Next:
% Row Reduction and Echelon form
% Matrix as Transform
% Differentiation chart
% Integration chart
% Rolle's theorem, Mean Value Theorem,
% Implicit differentiation
% Integration by substitution

\end{document}

% pdflatex topic01.tex