\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amssymb} % for therefore
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams
% \usepackage{graphicx}
\usepackage{caption}   % for caption options
\usepackage{pgfplots}  % to plot graphs
\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\usetikzlibrary{fit,matrix} % to fit matrix
\usepgfplotslibrary{fillbetween} % for fill between in plots
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
\renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}
\usepackage[a4paper, total={6in, 8in}]{geometry} % for margin adjustment
\usepackage{titling} % for \droptitle

% For task choices
\usepackage{tasks}
% define new task: choice   for \begin{choice}  \end{choice}
\NewTasks[style=enumerate,counter-format=tsk[a]),label-width=1em,item-indent = 1em,column-sep =2em,after-item-skip =3ex]{choices}[\choice](2)

% For blackboard mode 0 and 1 (double-stroke) \mathbbold{0} \mathbbold{1}
\DeclareSymbolFont{bbold}{U}{bbold}{m}{n}
\DeclareSymbolFontAlphabet{\mathbbold}{bbold}
\DeclareMathOperator{\cis}{cis}
\DeclareMathOperator{\sech}{sech}
\DeclareMathOperator{\csch}{csch}
\DeclareMathOperator{\Nul}{Nul}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Span}{Span}

% encircle text
\newcommand\encircle[1]{%
  \tikz[baseline=(X.base)] 
    \node (X) [draw, red, shape=circle, inner sep=0] {\strut #1};}

\setlength{\droptitle}{-7em}   % This is your set screw, more -ve higher
\title{MATH1013 Exam Preparation}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle



\section{Resources}
The MATH1013 Wattle provides a lot of review material:
\begin{itemize}
\item Past Final Exams (most with solutions)
\item Assignment Solutions (Week 5 and Week 8)
\item Mid-semester Exams (with solutions, last one in Week 8)
\item Linear Algebra Review (questions and solutions in Document Camera Week 12)
\item Calculus Review (examples and solutions) and Techniques (Week 12)
\end{itemize}

With so many choices, what to do? I suppose you should concentrate on the last two
--- they should be most relevant to what the lecturers have been teaching.
However, don't just look at the examples and solutions ---
you need to think: what are the steps? How should I do this in exam?

In exam, you have to work out your answer to a question. There are steps of reasoning, steps of computation, and steps to transform a difficult problem to an easy one.
Looking at the given solution won't let you see these steps --- you have to work out the answer yourself, and discover those steps.


\section{Tips}
When you read the exam questions, pick up which ones are easy, and which ones are hard (look at the marks). Any exam has both types of questions, so that there is a spread of grades to identify student levels. Aim for the 50 mark, then anything else is a bonus.
To achieve this,
\begin{itemize}
\item Don't lose marks on the easy ones. If you can check your answer (solution to equations, matrix inverse, limit) check it.
\item Don't spend too much time on the hard ones. Better spend time on consistency check of solved problems.
\item When you have to deal with a hard one, try to understand the question properly: draw a diagram, use sample values to replace variables (say put x = 0 or put x = 1). Think hard, a trick may be hidden somewhere to simplify the problem.
\item Note the parts of a question. Attempt first questions with independent parts (unrelated limits, unrelated determinants) -- if you make a mistake, it is localised.
\item When attempting questions with dependent parts,
\begin{itemize}
\item Unless you are confident of the answer of part (a), don't go to part (b).
\item When you go to solve part (b), see if part (a) helps.
\item If you cannot do part (b), which is just a result, see if the result of (b) is enough to solve part (c).
\end{itemize}
\end{itemize}
More specific tips
\begin{itemize}
\item When ask for a maximum or minimum, most likely you have to justify it. You can look at f'(x) for increase/decrease, but f''(x) is quicker: +ve is concave up, so it is a minimum (think of $y = x^{2}$). However, if f''(x) is hard to get (say you'll need product rule or quotient rule), then go back to f'(x) for explanation.
\item When doing matrix row reduction, stick to the format $R_{i} + \alpha R_{j}$, that is, use only row addition. Copy the row $R_{i}$ to a scrap sheet,
and write the row $\alpha R_{j}$ beneath it, with $\alpha$ possibly negative.
Then just add on the scrap sheet, and copy (carefully) back.
Since you are doing one thing (either multiply by $\pm$ve or add) each time,
this reduces errors.
\item Don't use arrows in writing up answers. Arrows are confusing for markers, although lecturers and tutors often use arrows to direct your attention. If you have an urge to use an arrows to connect two line in your answer, mark the first line with an asterisk (*), then write: From (*), or By (*), and continue.
\end{itemize}

\section{Examples}
Let me illustrate these points by some examples.

\subsection{Algebra Revision Q1}
For the matrix
$A = \left[\begin{array}{rrrrr}
       1 & -1 &  3 & 5 & 7\\
       3 & -4 &  2 & 1 & 1\\
      -8 & 11 & -3 & 2 & 4\end{array}\right]$

\begin{enumerate}[label=\alph*)]
\item Find a basis for $\Nul(A)$.
\item Find a basis for the column space of $A$ and write down $\rank(A)$.
\item Verify the rank theorem for $A$.
\item Prove or disprove: the transformation defined by $A$ is one-to-one.
\item Prove or disprove: the transformation defined by $A$ is onto.
\end{enumerate}
\emph{Comment}
This is a typical multi-part question, and we've seen this before (Worksheet 9, Q2(a)).
The parts are relatively independent (part (c) slightly depends on (a) and (b)),
but we know verifying rank theorem is easy (just add up dimensions). Let's do this one.

The key is to reduce the matrix to reduced row echelon form (RREF) for $\Nul(A)$ and $\Col(A)$. First we need to reduce these red entries to $0$:

$\left[\begin{array}{rrrrr}
       1 & -1 &  3 & 5 & 7\\
       \encircle{3} & -4 &  2 & 1 & 1\\
       \encircle{-8} & 11 & -3 & 2 & 4\end{array}\right]$

The first $\encircle{3}$ is eliminated by $R_{2} - 3R_{1}$,
which I insist to be $R_{2} + (-3)R_{1}$. On scrap paper, I do this:
\[
\begin{array}{rrrrrl}
     -3 &  3 &  -9 & -15 & -21 & \leftarrow (-3)R_{1}\\
      3 & -4 &   2 &   1 &   1 & \leftarrow R_{2}\\
      0 & -1 &  -7 & -14 & -20 & \text{add}\\
      0 &  1 &   7 &  14 &  20 & \text{negate}
\end{array}
\]
Similarly, eliminate $\encircle{-8}$ by $R_{3} + 8R_{1}$:
\[
\begin{array}{rrrrrl}
      8 & -8 &  24 &  40 &  56 & \leftarrow 8R_{1}\\
     -8 & 11 &  -3 &   2 &   4 & \leftarrow R_{3}\\
      0 &  3 &  21 &  42 &  60 & \text{add}\\
      0 &  1 &   7 &  14 &  20 & \text{divide by }3
\end{array}
\]
Obviously the next one is $R_{3} - R_{2}$ for the new rows, giving
\[
A \sim
\left[\begin{array}{rrrrr}
       1 & -1 &  3 &  5 &  7\\
       0 &  1 &  7 & 14 & 20\\
       0 &  0 &  0 &  0 &  0\end{array}\right]
\]
At this point, we have only a row echelon form (REF).
This already gives $\rank(A) = \dim(\Col(A)) = 2$, the number of columns with pivots.
By Rank Theorem, $\nullity(A) = \dim(\Nul(A)) = 5 - 2 = 3$, the number of free parameters.
We don't need this formation yet, but they'll be answers for parts (b) and (c).

We could work out the parameteric solution from REF, from which $\Nul(A)$ can be deduced.
It is much better to eliminate $\encircle{-1}$, obtaining an RREF that $\Nul(A)$ can be easily seen. This is just $R_{1} + R_{2}$ to be the new $R_{1}$, so
\begin{equation}
\label{eqn:rref-1}
A \sim
\left[\begin{array}{rrrrr}
       1 &  0 & 10 & 19 & 27\\
       0 &  1 &  7 & 14 & 20\\
       0 &  0 &  0 &  0 &  0\end{array}\right]
\end{equation}
At this point, we have an RREF, and you should be able to answer all parts (a) to (e).
If not, you should revise (Algebra L17, Column Space and Null Space), although the textbook explanation is better.

\paragraph{Theory of Null Space}
Since $\Nul(A)$ is the solution set of $A\mathtt{x} = \mathbbold{0}$,
our RREF in~\eqref{eqn:rref-1} shows:
\[
\begin{array}{rrrrrrrrrlcl}
   1x_{1} & + &   &  & 10x_{3} & + & 19x_{4} & + & 27x_{5} & = 0 & \text{means}
 & x_{1} = -10 x_{3} - 19x_{4} - 27x_{5}\\    
           &  & 1x_{2}  & + & 7x_{3} & + & 14x_{4} & + & 20x_{5} & = 0 & \text{means}
 & x_{2} = -7 x_{3} - 14x_{4} - 20x_{5}\\    
\end{array}
\]
The parameteric solution is therefore,
\[
\left[\begin{array}{r}x_{1}\\x_{2}\\x_{3}\\x_{4}\\x_{5}\end{array}\right]
= x_{3} \left[\begin{array}{r}-10\\-7\\1\\0\\0\end{array}\right]
+ x_{4} \left[\begin{array}{r}-19\\-14\\0\\1\\0\end{array}\right]
+ x_{5} \left[\begin{array}{r}-27\\-20\\0\\0\\1\end{array}\right]
= x_{3} \mathtt{v_{1}} + x_{4} \mathtt{v_{2}} + x_{5} \mathtt{v_{3}}
\]
showing a linear combination of basis vectors $\mathtt{v_{1}}, \mathtt{v_{2}}$ and $\mathtt{v_{3}}$.
Hence $\Nul(A) = \Span\{\mathtt{v_{1}}, \mathtt{v_{2}, \mathtt{v_{3}}}\}$,
or basis of $\Nul(A) = \{\mathtt{v_{1}}, \mathtt{v_{2}}, \mathtt{v_{3}}\}$.
\textbf{End Theory}

If you are familiar with the theory above -- and the examiner/marker expects you to be familiar with the theory above, you can deduce the basis $\{\mathtt{v_{1}}, \mathtt{v_{2}}, \mathtt{v_{3}}\}$ directly from the RREF in~\eqref{eqn:rref-1}, by flipping signs and appending $0$-$1$ entries appropriately.

\paragraph{Theory of Column Space}
Since $\Col{A}$ is the space spanned by the column vectors of $A$,
$\Col{A} = \Span\left\{
\left[\begin{array}{r}1\\3\\-8\end{array}\right],
\left[\begin{array}{r}-1\\-4\\11\end{array}\right],
\left[\begin{array}{r}3\\2\\-3\end{array}\right],
\left[\begin{array}{r}5\\1\\2\end{array}\right],
\left[\begin{array}{r}7\\1\\4\end{array}\right]
\right\}$.
% by chopping up matrix $A$ into its column vectors.
To find a basis, we have to eliminate all linearly dependent vectors from this set:
\begin{itemize}
\item The first column vector
$\left[\begin{array}{r}1\\3\\-8\end{array}\right]$ is nonzero,
thus it is linearly independent by itself.
\item The second column vector
$\left[\begin{array}{r}-1\\-4\\11\end{array}\right]$ is not a multiple of the first one,
thus they are linearly independent.
\item To find out if the third column vector
$\left[\begin{array}{r}3\\2\\-3\end{array}\right]$ is linearly independent of the first two, we need to solve for $a$ and $b$ such that:
$\left[\begin{array}{r}3\\2\\-3\end{array}\right] =
a \left[\begin{array}{r}1\\3\\-8\end{array}\right] +
b \left[\begin{array}{r}-1\\-4\\11\end{array}\right]$.
The solution can be found by row reduction of the augmented matrix:
\[
\left[\begin{array}{rr|r}
      1 & -1 & 3\\
      3 & -4 & 2\\
     -8 & 11 & -3\end{array}\right]
\]
We have done this already, and~\eqref{eqn:rref-1} shows the result:
\[
\left[\begin{array}{rr|r}
      1 & -1 & 3\\
      3 & -4 & 2\\
     -8 & 11 & -3\end{array}\right]
\sim
\left[\begin{array}{rr|r}
      1 &  0 & 10\\
      0 &  1 &  7\\
      0 &  0 &  0\end{array}\right]
\]
Therefore the third column vector is a linear combination of the first two:\\
$\left[\begin{array}{r}3\\2\\-3\end{array}\right]
= 10 \left[\begin{array}{r}1\\3\\-8\end{array}\right]
+ 7 \left[\begin{array}{r}-1\\-4\\11\end{array}\right]$.
\item Similarly from~\eqref{eqn:rref-1},
the fourth and fifth column vectors are linear combinations of the first two:
$\left[\begin{array}{r}5\\1\\2\end{array}\right]
= 19 \left[\begin{array}{r}1\\3\\-8\end{array}\right]
+ 14 \left[\begin{array}{r}-1\\-4\\11\end{array}\right],
\left[\begin{array}{r}7\\1\\4\end{array}\right]
= 27 \left[\begin{array}{r}1\\3\\-8\end{array}\right]
+ 20 \left[\begin{array}{r}-1\\-4\\11\end{array}\right]$
\end{itemize}
The RREF in~\eqref{eqn:rref-1} shows that, in the matrix $A$,
only the first two columns are linearly independent,
forming a basis for $\Col(A) $.
The remaining columns $(10,7), (19,14), (27,20)$ actually show:
\[
\left[\begin{array}{r}3\\2\\-3\end{array}\right]
= 10 \left[\begin{array}{r}1\\3\\-8\end{array}\right]
+ 7 \left[\begin{array}{r}-1\\-4\\11\end{array}\right],
\left[\begin{array}{r}5\\1\\2\end{array}\right]
= 19 \left[\begin{array}{r}1\\3\\-8\end{array}\right]
+ 14 \left[\begin{array}{r}-1\\-4\\11\end{array}\right],
\left[\begin{array}{r}7\\1\\4\end{array}\right]
= 27 \left[\begin{array}{r}1\\3\\-8\end{array}\right]
+ 20 \left[\begin{array}{r}-1\\-4\\11\end{array}\right]
\]
\textbf{End Theory}

Again, if you are familiar with the theory above, you can just grab from matrix $A$ the corresponding linearly independent vectors in RREF~\eqref{eqn:rref-1} to form a basis of $\Col(A)$.

\bigskip
\noindent
\emph{Solution} Using row reduction to reduced row echelon form:
\[
A = 
\left[
\tikz[baseline=(M.west)]{%
    \node[matrix of math nodes,matrix anchor=west,
          %left delimiter=(,right delimiter=),
          ampersand replacement=\&] (M) {%
      1 \& -1 \& 3 \& 5 \& 7\\
      3 \& -4 \& 2 \& 1 \& 1 \\
     -8 \& 11 \& -3 \& 2 \& 4 \\
    };
    \node[draw,red,fit=(M-1-1)(M-3-1),inner sep=-1pt] {};
    \node[draw,red,fit=(M-1-2)(M-3-2),inner sep=-1pt] {};
  }
\right]
\sim
\left[
\tikz[baseline=(M.west)]{%
    \node[matrix of math nodes,matrix anchor=west,
          %left delimiter=(,right delimiter=),
          ampersand replacement=\&] (R) {%
      \encircle{1} \&  0 \& 10 \& 19 \& 27\\
      0 \&  \encircle{1} \&  7 \& 14 \& 20\\
      0 \&  0 \&  0 \&  0 \&  0\\
    };
    \node[draw,blue,fit=(R-1-3)(R-2-3),inner sep=-1pt] {};
    \node[draw,blue,fit=(R-1-4)(R-2-4),inner sep=-1pt] {};
    \node[draw,blue,fit=(R-1-5)(R-2-5),inner sep=-1pt] {};
  }
\right]
\]
From the blue columns, basis for $\Nul(A) = \left\{
\left[\begin{array}{r}-10\\-7\\1\\0\\0\end{array}\right],
\left[\begin{array}{r}-19\\-14\\0\\1\\0\end{array}\right],
\left[\begin{array}{r}-27\\-20\\0\\0\\1\end{array}\right]\right\}$,
with $\nullity(A) = 3$.
From the pivots and red columns, basis for $\Col(A) = \left\{
\left[\begin{array}{r}1\\3\\-8\end{array}\right],
\left[\begin{array}{r}-1\\-4\\11\end{array}\right]\right\}$,
with $\rank(A) = 2$.
Since $A$ represents a linear transformation $T : \mathbb{R}^5 \rightarrow \mathbb{R}^{3}$, the Rank Theorem asserts that $5 = \nullity(A) + \rank(A) = 3 + 2$, which is verified in this case.
Since not every row has a pivot, $T$ is not onto (or other reasons).
Since not every column as a pivot, $T$ is not one-to-one (or other reasons).

\paragraph{Exercise}
Work out Q1 properly, showing what you should write in your answer, omitting any theory.

\section{Remarks}
In exams, the questions are same for everyone. Your job is to get as many marks as possible in the limited time allocated. So pick and choose, and think. Use your time wisely!

\end{document}

% pdflatex prepare.tex