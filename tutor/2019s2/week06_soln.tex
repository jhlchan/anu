\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amssymb} % for therefore
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams
% \usepackage{graphicx}
\usepackage{caption}   % for caption options
\usepackage{pgfplots}  % to plot graphs
\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\usepgfplotslibrary{fillbetween} % for fill between in plots
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
\renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}
\usepackage[a4paper, total={6in, 8in}]{geometry} % for margin adjustment
\usepackage{titling} % for \droptitle

% For task choices
\usepackage{tasks}
% define new task: choice   for \begin{choice}  \end{choice}
\NewTasks[style=enumerate,counter-format=tsk[a]),label-width=1em,item-indent = 1em,column-sep =2em,after-item-skip =3ex]{choices}[\choice](2)

% For blackboard mode 0 and 1 (double-stroke) \mathbbold{0} \mathbbold{1}
\DeclareSymbolFont{bbold}{U}{bbold}{m}{n}
\DeclareSymbolFontAlphabet{\mathbbold}{bbold}

% encircle text
\newcommand\encircle[1]{%
  \tikz[baseline=(X.base)] 
    \node (X) [draw, shape=circle, inner sep=0] {\strut #1};}

\setlength{\droptitle}{-7em}   % This is your set screw, more -ve higher
\title{Week 6 Calculus Solutions}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\paragraph{2}
\textbf{Calculus}
\begin{enumerate}[label=\alph*)]
\item Use part $1$ of the Fundamental Theorem of Calculus to find the derivative of
\[
g(x) = \displaystyle\int_{2x}^{3x}\; u^{2} du.
\]
Note: you will need to think about breaking the integral into two parts.
\item A function $f$ satisfies the integral equation
\[
6 + \displaystyle\int_{a}^{x}\;\frac{f(t)}{t^{2}} dt = 2\sqrt{x}\qquad\forall{x} > 0,
\]
where $a$ is a constant.
\begin{enumerate}[label=\roman*)]
\item By choosing a suitable value for $x$, find the value of $a$.
\item By differentiating both sides with respect to $x$, find $f(x)$.
\end{enumerate}
\item Evaluate the following integrals.
For parts i) and ii), use an appropriate substitution.
\begin{enumerate}[label=\roman*)]
\item $\displaystyle\int\;\frac{dt}{(1 - 6t)^{4}}$
\item $\displaystyle\int_{1}^{2}\;(3x - 2)^{20} dx$
\item $\displaystyle\int_{-2}^{2}\;\sin(x^{3}) dx$
\end{enumerate}
\end{enumerate}
\emph{Solution}
\begin{enumerate}[label=\alph*)]
\item Note $g(x) = f(3x) - f(2x)$,
where $f(t) = \displaystyle\int_{0}^{t}\; u^{2} du$, so $f'(t) = t^{2}$.\\
Thus $g'(x) = \displaystyle{f'(3x)\frac{d(3x)}{dx} - f'(2x)\frac{d(2x)}{dx}}
= 3(3x)^{2} - 2(2x)^{2} = 19x^{2}$.
\item 
\begin{enumerate}[label=\roman*)]
\item Put $x = a$, then $\displaystyle\int_{a}^{a}\;\frac{f(t)}{t^{2}} dt = 0$.
Thus $6 = 2\sqrt{a}$, giving $a = 9$.
\item Differentiating both sides with respect to $x$,
\[
\displaystyle\frac{f(x)}{x^{2}} = 2\frac{d \sqrt{x}}{dx} = 2\frac{1}{2\sqrt{x}} = \frac{1}{\sqrt{x}}
\]
Thus $f(x) = x^{\frac{3}{2}} = x \sqrt{x}$ for all $x > 0$. 
\end{enumerate}
\item
\begin{enumerate}[label=\roman*)]
\item Let $u = 1 - 6t$, then $du = -6dt$.
\[
\displaystyle\int\;\frac{dt}{(1 - 6t)^{4}}
= \int\;\frac{u^{-4} du}{-6}
= \frac{u^{-3}}{(-6)(-3)} + C
= \frac{1}{18(1 - 6t)^{3}} + C
\]
% Wolfram Alpha:
% int (1/(1 - 6t)^4) dt   gives 1/(18(1-6t)^3) + C
\item Let $y = 3x - 2$, then $dy = 3dx$, and $x = 1$ has $y = 1$, $x = 2$ has $y = 4$.
\[
\displaystyle\int_{1}^{2}\;(3x - 2)^{20} dx
= \int_{1}^{4}\;y^{20}\frac{dy}{3}
= \left.\frac{y^{21}}{3(21)}\right\rvert_{1}^{4}
% = \frac{1}{63}(4^{21} - 1)
= \frac{4^{21} - 1}{4^{3} - 1}
= \frac{64^{7} - 1}{64 - 1} = 69810262081
\]
% Wolfram Alpha:
% int from 1 to 2 (3x - 2)^20 dx  gives 69810262081, indefinite is -1/63(2 - 3x)^21
% int (3x - 2)^20 dx from 1 to 2
% (4^21 - 1)/63  = 4398046511103/63 = 69810262081
\item Let $f(x) = \sin(x^{3})$, then 
$f(-x) = \sin((-x)^{3}) = \sin(- x^{3}) = -\sin(x^{3}) = -f(x)$.
Thus $f(x)$ is \textbf{odd}, hence
\[
\displaystyle\int_{-2}^{2}\;\sin(x^{3}) dx = 0
\]
% Wolfram Alpha:
% int sin(x^3) dx from -2 to 2   gives 0, indefinite integral has gamma function!
\end{enumerate}
\end{enumerate}
\emph{Note}\\
For $(a)$, $\displaystyle\int\;u^{2} du = \frac{u^{3}}{3} + C$,
so $g(x) = \displaystyle\frac{1}{3}((3x)^{3} - (2x)^{3}) = \frac{19 x^{3}}{3}$,
and $g'(x) =19x^{2}$.\\
For $(b)$, indeed,
$\displaystyle\int_{9}^{x}\;\frac{t\sqrt{t}}{t^{2}} dt = 
\int_{9}^{x}\;\frac{dt}{\sqrt{t}} = \left. 2\sqrt{t} \right\rvert_{9}^{x} =
2\sqrt{x} - 6$.
Some relevant graphs are show:

\bigskip
\begin{tikzpicture}[scale=1.0]
\begin{axis}[
 axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = 0, xmax = 3.1, ymin = -0.5, ymax = 4,
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0}, % <--- 0 at y
    ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = x (sqrt x)
\addplot[domain=0:3, thick, blue]{x * (sqrt x)};
% label
\node at (axis cs: 2.8,3) {$y = x \sqrt{x}$};
\end{axis}
\end{tikzpicture}
\begin{tikzpicture}[scale=1.0]
\begin{axis}[
 axis equal, % ensure equal scales on both axes
 major grid style={line width=0.2pt,draw=gray!50},
 xlabel={$x$},
 ylabel={$y$},
 xmin = -2, xmax = 2.2, ymin = -1, ymax = 1,
    grid=both,
    extra x ticks={0}, % <--- 0 at x
    extra y ticks={0}, % <--- 0 at y
    % ticklabel style={fill=white},
    axis lines=middle,
    axis line style={latex-latex},
    xlabel style={at={(ticklabel* cs:1)},anchor=north west},
    ylabel style={at={(ticklabel* cs:1)},anchor=south west}
]
% f(x) = sin (x^3)
\addplot[domain=-2:2, thick, blue, smooth,name path=A]{sin (deg (x * x * x))};
\addplot[draw=none,name path=B] {0};     % fictional curve = x-axis
\addplot[blue!30!white] fill between[of=A and B,soft clip={domain=-2:2}]; % filling
% label
\node at (axis cs: 1.5,1.2) {$y = sin (x^{3})$};
\end{axis}
\end{tikzpicture}

\end{document}

% pdflatex week06_soln.tex