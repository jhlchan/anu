\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
\usetikzlibrary{trees} % for tree diagram
\usetikzlibrary{automata, positioning} % for automata
\usepackage{pgfplots} % for function plotting
\pgfplotsset{compat=1.7} % for color matching

\usetikzlibrary{matrix} % for matrix nodes
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
% \usepackage{ifthen} % has \ifthenelse, but PGF Math Error: unknown o or of
\usepackage{todonotes} % has \ifthenelse !
\usepackage{textcomp} % for textdollaroldstyle
\usepackage{bm}       % for bold math symbols
\DeclareSymbolFont{bbold}{U}{bbold}{m}{n}
\DeclareSymbolFontAlphabet{\mathbbold}{bbold}

% special marks
\usepackage{amssymb} % http://ctan.org/pkg/amssymb, for \because and \therefore
\usepackage{pifont}% http://ctan.org/pkg/pifont, for \ding
\newcommand{\cmark}{\ding{51}}% check tick
\newcommand{\xmark}{\ding{55}}% cross X
% For table color
\usepackage{color, colortbl}  % for definecolor and cellcolor
\definecolor{Gray}{gray}{0.8}  % 1.0 is no gray, 0.0 is full gray.

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

\title{Markov Process}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

% the text cross-mark
\newcommand{\txmark}{\text{\xmark}} % opposite of \checkmark, after begin{document}

\noindent
Markov Process is the topic for unit C3, making use of ideas from matrix, tree, and probabilites.

\section{Symmetry}
If the following Markov process has a steady state vector $S$,
what is it?

\begin{center}
\begin{tikzpicture}[scale=0.8, % using a scale
  shorten >=1pt,
  node distance = 2, auto]
  \tikzset{every loop/.style={min distance=2,looseness=12}}
  \draw ( 90: 1.8) node[state,accepting] (a) {$A$};
  \draw (-30: 1.8) node[state,accepting] (b) {$B$};
  \draw (210: 1.8) node[state,accepting] (c) {$C$};
  \path[->,thick] (a) edge[bend left] node {$p$} (b)
                      edge [loop above] node {} ()
                  (b) edge[bend left] node {$p$} (c)
                      edge [loop right] node {} ()
                  (c) edge[bend left] node {$p$} (a)
                      edge [loop left] node {} ();
\end{tikzpicture}
\end{center}

\noindent
The steady state vector $S = \left[\begin{array}{c} a\\b\\c\end{array}\right]$,
where $a, b, c$ are the probabilities of staying at states $A, B$ and $C$, respectively.
The three states are symmetrical: if you cycle through
$A \rightarrow B \rightarrow C \rightarrow A$ once or twice, the process is the same.
Although the steady state vector becomes $\left[\begin{array}{c}c\\a\\b\end{array}\right]$ first, then $\left[\begin{array}{c}b\\c\\a\end{array}\right]$, it must stay the same as before.
What does this mean? It means $a = b = c$. Now, since $\mathbf{v}$ is a probability vector (C3 slide 17), $1 = a + b + c = 3a$, giving $a = b = c = \frac{1}{3}$.
Therefore, with only insight into the diagram,
we can conclude that the steady state vector must be 
$S = \left[\begin{array}{c} 1/3\\1/3\\1/3\end{array}\right]$.
That's the power of symmetry.
% Exercise:
% What is the transition matrix T?
% Verify that v is indeed the steady state vector.
% T = [[(1-p), p, 0],[0,(1-p),p], [p,0,(1-p)]]
% (transpose [[(1-p), p, 0],[0,(1-p),p], [p,0,(1-p)]]) . [1/3,1/3,1/3]
% (transpose [[(1-p), p, 0],[0,(1-p),p], [p,0,(1-p)]]) . 1/3 [1,1,1]
% gives [(1-p)/3 + p/3, (1-p)/3 + p/3, (1-p)/3 + p/3] = [1/3,1/3,1/3]
% (transpose [[(1-p), p, 0],[0,(1-p),p], [p,0,(1-p)]]) . [1,1,1] = [1,1,1]


\section{Markov process with $2$ States}
Let $\mathtt{A, B}$ be the two states.
The simplest case of such a Mark process is:

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
  shorten >=1pt,
  node distance = 2, auto]
  \node[state,accepting] (a) {$\mathtt{A}$};
  \node[state,accepting] (b) [right = of a] {$\mathtt{B}$};
  \path[->,thick] (a) edge[bend left] node {$p$} (b)
                      edge [loop left] node {} ()
                  (b) edge[bend left] node {$q$} (a)
                      edge [loop right] node {} ();
\end{tikzpicture}

\noindent
There is not much information: just the transition probability $p$ from $\mathtt{A}$ to $\mathtt{B}$, and $q$ from $\mathtt{B}$ to $\mathtt{A}$. There are some missing information: two loop arrows are shown without labels. Let us forget them for a moment, but ask the most important question: if this system has a steady state vector $S$, what is it?

Let steady state vector $S = \left[\begin{array}{c} a\\b\end{array}\right]$.
A steady state is balanced, hence:
\[
\begin{array}{rll}
\mathbb{P}(\mathtt{A}\text{ in}) =
\mathbb{P}(\mathtt{A}\text{ out})\text{, so} & bq = ap
 & \text{Left: at }\mathtt{B},\text{ into }\mathtt{A}.\;
   \text{Right: at }\mathtt{A},\text{ out of }\mathtt{A}\\
\mathbb{P}(\mathtt{B}\text{ in}) =
\mathbb{P}(\mathtt{B}\text{ out})\text{, so} & ap = bq
 & \text{similarly, but gives the same equation.}\\ 
\end{array}
\]
With $a + b = 1$ for a probability vector (C3 slide 17),
this gives $(1 - a)q = ap$, or $a = q/(p + q), b = p/(p + q)$.
Therefore (see Worksheet 7 Q4):
\begin{equation}
\label{eqn:steady-state-two}
S = \left[\begin{array}{c} q/(p + q)\\p/(p + q)\end{array}\right]
\end{equation}
To verify $S$ is indeed the steady state vector, first form the transition matrix $T$.
This is a stochastic matrix (C3 slide 17) with rows of probability vectors.
Note that the missing values in the state transition diagram are easy to deduce for probability vectors, thus
$T = \left[\begin{array}{cc} (1-p) & p\\q & (1-q)\end{array}\right]$.\\
\label{q:ex-1}%
\textbf{Exercise 1}. Show that this transition matrix $T$ has the steady state vector $S$ given by Equation~\eqref{eqn:steady-state-two}.


% References
% 
% First Links in the Markov Chain (By Brian Hayes)
% https://www.americanscientist.org/article/first-links-in-the-markov-chain
% Probability and poetry were unlikely partners in the creation of a computational tool
% American Scientist: Issue March-April 2013 Volume 101, Number 2 Page 92
% DOI: 10.1511/2013.101.92

\subsection{Special Case: $1$}
For the $2$-state Markov process, if $p = q$,

\noindent
\begin{tikzpicture}[
  shorten >=1pt,
  node distance = 2, auto]
  \node[state,accepting] (a) {$\mathtt{A}$};
  \node[state,accepting] (b) [right = of a] {$\mathtt{B}$};
  \path[->,thick] (a) edge[bend left] node {$p$} (b)
                      edge [loop left] node {} ()
                  (b) edge[bend left] node {$p$} (a)
                      edge [loop right] node {} ();
\end{tikzpicture}

\noindent
Then $bp = ap$. If $p \ne 0$, $a = b = 1/2$,
and $S = \left[\begin{array}{c} 1/2 \\ 1/2\end{array}\right]$ for any $p \le 1$.
This is evident from the symmetry of state-transition diagram.
Convergence to $S$ is faster when probability $p$ approaches $1$.
If $p = 1$, the loops vanishes, and the system oscillates between states $\mathtt{A}$ and $\mathtt{B}$, never settling to the steady state $S$.

\subsection{Special Case: $2$}
On the other hand, if $q = 0$, then $a = 0, b = 1$ for $p \ne 0$,
and $S = \left[\begin{array}{c} 0 \\ 1\end{array}\right]$.
This is because the state-transition diagram shows state $\mathtt{B}$ is a sink:

\noindent
\begin{tikzpicture}[
  shorten >=1pt,
  node distance = 2, auto]
  \node[state,accepting] (a) {$\mathtt{A}$};
  \node[state,accepting] (b) [right = of a] {$\mathtt{B}$};
  \path[->,thick] (a) edge[bend left] node {$p$} (b)
                      edge [loop left] node {} ()
                  (b) edge [loop right] node {$1$} ();
\end{tikzpicture}

\noindent
For a sink, $\mathbb{P}(\mathtt{B}\text{ in}) = \mathbb{P}(\mathtt{B}\text{ out})$,
so $ap + b = b$.
Thus $ap = 0$, or $a = 0$ since $p \ne 0$.
Therefore $b = 1$,
giving the steady state vector $S = \left[\begin{array}{c}  0 \\ 1\end{array}\right]$.\\
% In general, a sink state gives a special row in the transition matrix, and the steady state vector is the transpose of it.

\bigskip
\noindent
\label{q:ex-2}%
\textbf{Exercise 2}. What happens when $p = 0$ ? Figure out its transition matrix.
% Two sinks. Two steady state vectors.

% Markov Chain simulator
% http://setosa.io/ev/markov-chains/
% You can also access a fullscreen version at setosa.io/markov
% http://markov.yoriz.co.uk


\section{Process Tree}
From an initial state, the trace of the Markov process is represented by a tree,
with the initial state as the root.
For the $2$-state example, a trace tree from initial state $\mathtt{A}$ for $3$ steps looks like:

\noindent
\tikzstyle{level 1}=[level distance=20mm, sibling distance=30mm]
\tikzstyle{level 2}=[level distance=20mm, sibling distance=15mm]
\tikzstyle{level 3}=[level distance=20mm, sibling distance=10mm]
\begin{tikzpicture}[scale=0.8, % using a scale
  grow=right,->,>=angle 60]
  \node {$\mathtt{A}$}
    child {node {$\mathtt{AB}$}
      child {node {$\mathtt{ABB}$}
         child {node {$\mathtt{ABBB}$}
           child[->] {node[right]{$p(1-q)(1-q)$}}  
         }
         child {node {$\mathtt{ABBA}$}
           child[->] {node[right]{$p(1-q)q$}}  
         }
      }
      child {node{$\mathtt{ABA}$}
         child {node {$\mathtt{ABAB}$}
           child[->] {node[right]{$pqp$}}  
         }
         child {node {$\mathtt{ABAA}$}
           child[->] {node[right]{$pq(1-p)$}}  
         }
      }
    }
    child {node {$\mathtt{AA}$}
      child {node{$\mathtt{AAB}$}
         child {node {$\mathtt{AABB}$}
           child[->] {node[right]{$(1-p)p(1-q)$}}  
         }
         child {node {$\mathtt{AABA}$}
           child[->] {node[right]{$(1-p)pq$}}  
         }
      }
      child {node{$\mathtt{AAA}$}
         child {node {$\mathtt{AAAB}$}
           child[->] {node[right]{$(1-p)(1-p)p$}}  
         }
         child {node {$\mathtt{AAAA}$}
           child[->] {node[right]{$(1-p)(1-p)(1-p)$}}  
        }
      }
    };
    \node () at (0.8,  1.3) {$(1-p)$}; % AA
    \node () at (0.8, -1.2) {$p$};     % AB
    \node () at (2.8,  1.5 + 0.7) {$(1-p)$}; % AAA
    \node () at (2.8,  1.5 - 0.5) {$p$};     % AAB
    \node () at (4.9,  2.2 + 0.6) {$(1-p)$}; % AAAA
    \node () at (5.0,  2.2 - 0.5) {$p$};     % AAAB
    \node () at (5.0, -0.7 + 0.5) {$(1-p)$}; % ABAA
    \node () at (5.0, -0.7 - 0.5) {$p$};     % ABAB
    \node () at (2.8, -1.5 + 0.5) {$q$};     % ABA
    \node () at (2.8, -1.5 - 0.7) {$(1-q)$}; % ABB
    \node () at (5.0, -2.2 + 0.5) {$q$};     % ABBA
    \node () at (4.9, -2.2 - 0.6) {$(1-q)$}; % ABBB
    \node () at (5.0,  0.7 + 0.5) {$q$};     % AABA
    \node () at (5.0,  0.7 - 0.5) {$(1-q)$}; % AABB
\end{tikzpicture}

\noindent
This can be used for computation, but quite tedious:
\[
\begin{array}{rl}
    & \mathbb{P}(\text{in }\mathtt{B}\text{ after }3\text{ steps})\\
  = & \mathbb{P}(\mathtt{AAAB}) + \mathbb{P}(\mathtt{AABB}) + \mathbb{P}(\mathtt{ABAB}) + \mathbb{P}(\mathtt{ABBB})\\
%  = & (1-p)(1-p)p + (1-p)p(1-q) + pqp + p(1-q)(1-q)\\
\end{array}
\]
It is much better to obtain all information on step iteration by matrix method.
For $1$ step, by probablity theory,
\[
\begin{array}{l}
\mathbb{P}(\text{now in }\mathtt{A}) = 
\mathbb{P}(\text{was in }\mathtt{A})\mathbb{P}(\text{from }\mathtt{A}\text{ to }\mathtt{A}) + 
\mathbb{P}(\text{was in }\mathtt{B})\mathbb{P}(\text{from }\mathtt{B}\text{ to }\mathtt{A})\\
\mathbb{P}(\text{now in }\mathtt{B}) = 
\mathbb{P}(\text{was in }\mathtt{A})\mathbb{P}(\text{from }\mathtt{A}\text{ to }\mathtt{B}) + 
\mathbb{P}(\text{was in }\mathtt{B})\mathbb{P}(\text{from }\mathtt{B}\text{ to }\mathtt{B})\\
\end{array}
\]
In matrix form,
\[
\left[
\begin{array}{r}
\mathbb{P}(\text{now in }\mathtt{A})\\
\mathbb{P}(\text{now in }\mathtt{B})
\end{array}
\right]
 = 
\left[
\begin{array}{rr}
\mathbb{P}(\text{from }\mathtt{A}\text{ to }\mathtt{A}) &
\mathbb{P}(\text{from }\mathtt{B}\text{ to }\mathtt{A}) \\
\mathbb{P}(\text{from }\mathtt{A}\text{ to }\mathtt{B}) &
\mathbb{P}(\text{from }\mathtt{B}\text{ to }\mathtt{B})
\end{array}
\right]
\left[
\begin{array}{r}
\mathbb{P}(\text{was in }\mathtt{A})\\
\mathbb{P}(\text{was in }\mathtt{B})
\end{array}
\right]
\]
Let state vector $v_{t} = \left[
\begin{array}{r}
\mathbb{P}(\text{in }\mathtt{A}\text{ at }t)\\
\mathbb{P}(\text{in }\mathtt{B}\text{ at }t)
\end{array}
\right]$ at time $t$.
The column vectors are $v_{n}$ on the right, and $v_{n-1}$ on the left, where $n \in \mathbb{N}$.
Careful comparison of the middle iteration matrix $M$ and
the process transition matrix (C3 slide 18 (ii))
$ T =
\left[
\begin{array}{rr}
\mathbb{P}(\text{from }\mathtt{A}\text{ to }\mathtt{A}) &
\mathbb{P}(\text{from }\mathtt{A}\text{ to }\mathtt{B}) \\
\mathbb{P}(\text{from }\mathtt{B}\text{ to }\mathtt{A}) &
\mathbb{P}(\text{from }\mathtt{B}\text{ to }\mathtt{B})
\end{array}
\right]
$
shows that $M = T'$, the transpose (row and column exchagne) of $T$.
Hence the step iteration in matrix form is:
\begin{equation}
\label{eqn:step-iteration}
v_{n} = T' v_{n-1}
\qquad\text{so by induction: }v_{n} = (T')^{n} v_{0}
\end{equation}
where $v_{0}$ is the initial state vector.
Since the transition matrix $T$ is composed of row probability vectors
(because it is a stochastic matrix, C3 slide 17), the iteration matrix $T'$
is composed of column probability vectors.
In fact, if the states are numbered, and the initial state is $j$,
then multiplication by iteration matrix $(T')^{n}$ extracts its $j$-th column as the $n$-th step state vector:
\begin{equation}
\label{eqn:extract-col}
\left[
\begin{array}{r}
c_{12}\\
c_{22}\\
c_{32}
\end{array}
\right]
 = 
\left[
\begin{array}{rrr}
c_{11} & c_{12} & c_{13}\\
c_{21} & c_{22} & c_{23}\\
c_{31} & c_{32} & c_{33}\\
\end{array}
\right]
\left[
\begin{array}{r}
0\\
1\\
0
\end{array}
\right]   \qquad\qquad\text{ for } j = 2.
\end{equation}


\section{Steady State Vector}
For a Markov process of only $2$ states,
we have a formula for the steady state vector $S$, see Equation~\eqref{eqn:steady-state-two}.
For $3$ states, there is no good formula (lecturer gives one in Worksheet 7 Q6).
In general, we need a way to find the steady state vector $S$.
It turns out there are two methods.

\subsection{By Iteration (C3 slide 13)}
Recall Equation~\eqref{eqn:extract-col}, where the $n$-th iterated matrix is expanded:
$(T')^{n} = \left[
\begin{array}{rrr}
c_{11} & c_{12} & c_{13}\\
c_{21} & c_{22} & c_{23}\\
c_{31} & c_{32} & c_{33}\\
\end{array}
\right]$, and the multiplication by a known initial state $j$ vector extracts its $j$-th column:
$
\left[
\begin{array}{r}
c_{12}\\
c_{22}\\
c_{32}
\end{array}
\right]
 = (T')^{n}
\left[
\begin{array}{r}
0\\
1\\
0
\end{array}
\right]
$.
For large $n$, 
It may (or may not) happen that, for large $n$,
such $n$-th state converges to a steady state with
$S = \left[
\begin{array}{r}
s_{1}\\
s_{2}\\
s_{3}
\end{array}
\right]$, independent of the column $j$.
The best scenario is that this convergence happens to all initial states:
\begin{equation}
\label{eqn:iteration-converge}
\left[
\begin{array}{rrr}
s_{1} & s_{1} & s_{1}\\
s_{2} & s_{2} & s_{2}\\
s_{3} & s_{3} & s_{3}
\end{array}
\right]
= (T')^{n}
\left[
\begin{array}{rrr}
1 & 0 & 0\\
0 & 1 & 0\\
0 & 0 & 1\\
\end{array}
\right] = (T')^{n}\text{ by identity multiplication}
\end{equation}
Hence using an online matrix calculator to compute $(T')^{64}$, say,
you may find the steady state vector $S$ appears before your eyes,
as columns of the iteration.

\bigskip
\noindent
\label{q:ex-3}%
\textbf{Exercise 3}. Can we use $T^{n}$ instead to discover the steady state vector $S$?
% Yes, see C3 slide 21, but we shall have rows of steady state vector.

\subsection{By Short-cut (C3 slide 36)}
When iteration matrix $T'$ converges, $(T')^{n+1} = (T')^{n}$:
Multiplying both sides of Equation~\eqref{eqn:iteration-converge} once more by the iteration matrix, we have:
\[
\begin{array}{r@{\;=\;}ll}
           T' \left[
\begin{array}{rrr}
s_{1} & s_{1} & s_{1}\\
s_{2} & s_{2} & s_{2}\\
s_{3} & s_{3} & s_{3}
\end{array}
\right]
= (T')^{n+1} & (T')^{n} = & 
\left[
\begin{array}{rrr}
s_{1} & s_{1} & s_{1}\\
s_{2} & s_{2} & s_{2}\\
s_{3} & s_{3} & s_{3}
\end{array}
\right],\quad
\left[
\begin{array}{r}
s_{1}\\
s_{2}\\
s_{3}
\end{array}
\right] = S\\
T' S & S  & \text{by simplification}\\
(T' - I) S & \mathbbold{0} & \text{by subtraction}\\
(T' - I \dots 1) S & \mathbbold{0} \dots 1 & \text{replacing bottom row by all }1's\\
\end{array}
\]
I make up the notation for the last line --- so don't use it in exams!
The preceding equation with $\mathbbold{0}$ on the right-side has a solution in terms of a free parameter (C3 slide 34).
The short-cut trick is applied to remove the free parameter, as the final steady state vector is a probability vector: sum of all posssibles is $1$.
The last system can be solved by matrix inverse or Gaussian elimination, either by hand or by online calculators (see Worksheet 7 Q3).


\section{Ehrenfest Urns Models}

\subsection{Two urns}
In an experiment with two urns $A, B$ and $4$ balls, a ball is taken from one urn and put into the other. Assign states $1, 2, 3, 4, 5$ corresponding to $0, 1, 2, 3, 4$ ball in urn $A$, describe the behaviour of the Markov process. 

Construct a table for the state changes. The total number of balls for $A$ and $B$ is $4$.
Let $\mathbb{P}(A_{-})$ be the probability of one less ball in urn $A$ after a step.
The possible state changes are: $2 \rightarrow 1, 3 \rightarrow 2,$ \emph{etc.}, with probablilites given by: $(\text{number of balls in }A)/4$.
Similarly, let $\mathbb{P}(A_{+})$ be the probability of one more ball in urn $A$ after a step.
The possible state changes are: $1 \rightarrow 2, 2 \rightarrow 3,$ \emph{etc.}, with probablilites given by: $(\text{number of balls in }B)/4$.
\[
\begin{array}{c|cc|cc|cc}
\text{state } i & A & B & \mathbb{P}(A_{-}) & i \rightarrow j
                         & \mathbb{P}(A_{+}) & i \rightarrow j
                         \\
\hline
1 & 0 & 4 & 0   & -               & 4/4 & 1 \rightarrow 2 \\
2 & 1 & 3 & 1/4 & 2 \rightarrow 1 & 3/4 & 2 \rightarrow 3 \\
3 & 2 & 2 & 2/4 & 3 \rightarrow 2 & 2/4 & 3 \rightarrow 4\\
4 & 3 & 1 & 3/4 & 4 \rightarrow 3 & 1/4 & 4 \rightarrow 5\\
5 & 4 & 0 & 4/4 & 5 \rightarrow 4 & 0   & - \\
\end{array}
\]
Identifying the transition entry $T_{ij} = \text{probablility for }i \rightarrow j$:\\[1em]
$
\begin{array}{r|ccccc}
\text{state }i\backslash{j} & 1 & 2 & 3 & 4 & 5\\
\hline
        1 & 0   &   1 &   0 &   0 & 0  \\
        2 & 1/4 &   0 & 3/4 &   0 & 0  \\
        3 & 0   & 1/2 &   0 & 1/2 & 0  \\
        4 & 0   &   0 & 3/4 &   0 & 1/4\\
        5 & 0   &   0 &   0 &   1 & 0  \\
\end{array}
$,
hence $T = \displaystyle{\frac{1}{4}}
\left[
\begin{array}{rrrrr}
0 &   4 &   0 &   0 & 0 \\
1 &   0 &   3 &   0 & 0 \\
0 &   2 &   0 &   2 & 0 \\
0 &   0 &   3 &   0 & 1 \\
0 &   0 &   0 &   4 & 0 \\
\end{array}
\right]$.

\bigskip
\noindent
The state transition diagram of the Markov process is:

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
  shorten >=1pt,
  node distance = 1, auto]
  \node[state,accepting] (a) {$1$};
  \node[state,accepting] (b) [right = of a] {$2$};
  \node[state,accepting] (c) [right = of b] {$3$};
  \node[state,accepting] (d) [right = of c] {$4$};
  \node[state,accepting] (e) [right = of d] {$5$};
  \path[->,thick] (a) edge[bend left] node {$1$} (b)
                  (b) edge[bend left] node {$3/4$} (c)
                      edge[bend left] node {$1/4$} (a)
                  (c) edge[bend left] node {$1/2$} (b)    
                      edge[bend left] node {$1/2$} (d)
                  (d) edge[bend left] node {$3/4$} (c)
                      edge[bend left] node {$1/4$} (e)
                  (e) edge[bend left] node {$1$} (d);
  \node[below = of a] {$odd$};
  \node[below = of b] {$even$};
  \node[below = of c] {$odd$};
  \node[below = of d] {$even$};
  \node[below = of e] {$odd$};
\end{tikzpicture}

\paragraph{The zeros of $T^{n}$}
Note that number of balls in $A$ must change by $1$, increase or decrease.
That's why if you look at $T^{1} = T$, the main diagonal entries $(T^{1})_{jj}$ are all zero. Moreover, $T_{24} = 0$, as even $2$ cannot reach another even $4$ by $\pm{1}$.

Note that $T_{12} = 1$.
What do you think is $(T^{2})_{12}$ ?
This is the probability of changing from an odd state $1$ to an even state $2$ in $2$ steps. However, after the step, the initial odd state will be an even state,
and the next step will make the state odd again, thus $1 \rightarrow 2$ in $2$ steps is impossble, forcing $(T^{2})_{12} = 0$.

Using an online matrix calculator, we can trace how $(T^{2})_{12}$ becomes $0$:
\[
T^{2} = \frac{1}{4}
\left[
\begin{array}{rrrrr}
0 &   4 &   0 &   0 & 0 \\
1 &   0 &   3 &   0 & 0 \\
0 &   2 &   0 &   2 & 0 \\
0 &   0 &   3 &   0 & 1 \\
0 &   0 &   0 &   4 & 0 \\
\end{array}
\right]
\frac{1}{4}
\left[
\begin{array}{rrrrr}
0 &   4 &   0 &   0 & 0 \\
1 &   0 &   3 &   0 & 0 \\
0 &   2 &   0 &   2 & 0 \\
0 &   0 &   3 &   0 & 1 \\
0 &   0 &   0 &   4 & 0 \\
\end{array}
\right]
= \frac{1}{8}
\left[
\begin{array}{rrrrr}
2   &   0 &   6 &   0 & 0  \\
0   &   5 &   0 &   3 & 0  \\
1   &   0 &   6 &   0 & 1  \\
0   &   3 &   0 &   5 & 0  \\
0   &   0 &   6 &   0 & 2  \\
\end{array}
\right]
\]
% Wolfram Alpha:
% [[0,1,0,0,0],[1/4,0,3/4,0,0],[0,1/2,0,1/2,0],[0,0,3/4,0,1/4],[0,0,0,1,0]]^2
% (1/4 * [[0,4,0,0,0],[1,0,3,0,0],[0,2,0,2,0],[0,0,3,0,1],[0,0,0,4,0]])^2
% result: 1/8 [[2,0,6,0,0],[0,5,0,3,0],[1,0,6,0,1],[0,3,0,5,0],[0,0,6,0,2]]
Note that in $T^{2}$, the main diagonal is nonzero, but the one-off diagonals are all zero. This verifies, for example, state $1 \rightarrow 2$ or state $2 \rightarrow 1$,
or indeed between any odd and even states, are all impossible for two steps.

Therefore, during every step, the state parity (odd/even) must change.
For the transition matrix $T_{ij}$, the index expression $(i + j)$ represents entries along various diagonals. Let us say a diagonal is even if $(i + j)$ is even, and odd if $(i + j)$ is odd. You can see that, due to parity change of the state for each step,
\begin{itemize}
\item when $n$ is odd, $T^{n}$ has zero along even diagonals,
\item when $n$ is even, $T^{n}$ has zero along odd diagonals.
\end{itemize}

\[
\begin{array}{r|ccccc@{\qquad}r|ccccc}
n \text{ is odd}\\
(T^{n})_{ij} & 1 & 2 & 3 & 4 & 5 &
(i+j)  & 1 & 2 & 3 & 4 & 5\\
\hline
1 & 0   &   \blacksquare &   0 &   \blacksquare & 0 &
1 & \cellcolor{green}even & odd & \cellcolor{yellow}even & odd & \cellcolor{green}even\\
2 & \blacksquare &   0 & \blacksquare &   0 & \blacksquare &
2 & odd & \cellcolor{green}even & odd & \cellcolor{yellow}even & odd \\
3 & 0   & \blacksquare &   0 & \blacksquare & 0 &
3 & \cellcolor{yellow}even & odd & \cellcolor{green}even & odd & \cellcolor{yellow}even\\
4 & \blacksquare   &   0 & \blacksquare &   0 & \blacksquare &
4 & odd & \cellcolor{yellow}even & odd & \cellcolor{green}even & odd \\
5 & 0   &   \blacksquare &   0 &   \blacksquare & 0 &
5 & \cellcolor{green}even & odd & \cellcolor{yellow}even & odd & \cellcolor{green}even\\
\end{array}
\]

\[
\begin{array}{r|ccccc@{\qquad}r|ccccc}
n \text{ is even}\\
(T^{n})_{ij} & 1 & 2 & 3 & 4 & 5 &
(i+j)  & 1 & 2 & 3 & 4 & 5\\
\hline
1 & \blacksquare &   0 &   \blacksquare & 0 & \blacksquare &
1 & even & \cellcolor{green}odd & even & \cellcolor{yellow}odd & even\\
2 & 0 & \blacksquare &   0 & \blacksquare & 0 &
2 & \cellcolor{green}odd & even & \cellcolor{green}odd & even & \cellcolor{yellow}odd \\
3 & \blacksquare &   0 & \blacksquare & 0 & \blacksquare &
3 & even & \cellcolor{green}odd & even & \cellcolor{green}odd & even\\
4 & 0 & \blacksquare &   0 & \blacksquare & 0 &
4 & \cellcolor{yellow}odd & even & \cellcolor{green}odd & even & \cellcolor{green}odd \\
5 & \blacksquare &   0 &   \blacksquare & 0 & \blacksquare   &
5 & even & \cellcolor{yellow}odd & even & \cellcolor{green}odd & even\\
\end{array}
\]

\noindent
Another way to express this situation is:
\begin{itemize}
\item for even $n$, $(T^{n})_{ij} = 0$ for $(i+j)$ odd.
\item for odd $n$, $(T^{n})_{ij} = 0$ for $(i+j)$ even.
\end{itemize}
Thus $T^{n}$ always has zeros; in fact, diagonal zeros.
This makes the transition matrix $T$ non-regular.

\paragraph{Steady state and reachability}
Using the short-cut method and an online matrix calculator,
the steady state vector $S$ for this transition matrix $T$ is:
\[
S = \left[\begin{array}{c} 0.06250\\ 0.25000\\ 0.37500\\ 0.25000\\ 0.06250 \end{array}\right]
    = \left[\begin{array}{c} 1/16\\ 1/4\\ 3/8\\ 1/4\\ 1/16 \end{array}\right]
\]
Given an initial state vector $\mathbf{x_{0}}$,
the $n$-th state vector $\mathbf{x_{n}} = (T')^{n}\mathbf{x_{0}}$.
Will $\mathbf{x_{n}}$ converges to the steady state vector $S$ for large $n$?

When the initial state is at one of the $5$ states: $j = 1, 2, 3, 4, 5$,
$\mathbf{x_{0}}$ is all $0$s except for a single $1$ at $j$-th position.
Recall Equation~\eqref{eqn:extract-col}, 
$\mathbf{x_{n}} = (T^{n})\mathbf{x_{0}}$ will be the $j$-th column of $T^{n}$.
Since $T^{n}$ always has diagonal zeros, $\mathbf{x_{n}}$ contains $0$.
But the steady state vector $S$ has nonzero entries.
Hence $\mathbf{x_{n}}$ cannot converge to $S$.

\subsection{Three urns}
In this experiment, we have $3$ urns $A, B, C$, but only $2$ balls.
The states of urn $A$ are denoted by $1, 2, 3$, corresponding to $0, 1, 2$ balls it contains. Since it is now possible to take a ball from $B$ and put to $C$, leaving $A$ unchanged, we add a column $\mathbb{P}(A_{=})$ in the construction of the state transition table:

\[
\begin{array}{ccc|cc|cc|cc}
\text{state }i & A & BC & \mathbb{P}(A_{-}) & i \rightarrow j
                        & \mathbb{P}(A_{=}) & i \rightarrow j
                        & \mathbb{P}(A_{+}) & i \rightarrow j\\
\hline
1 & 0 & 2 & 0 & - & 1/2 & 1 \rightarrow 1 & 1/2 & 1 \rightarrow 2\\
2 & 1 & 1 & 1/2 & 2\rightarrow 1 & 1/4 & 2\rightarrow 2 & 1/4 & 2\rightarrow 3\\
3 & 2 & 0 & 2/2 & 3 \rightarrow 2 & 0 & - & 0 & - \\
\end{array}
\]
\noindent
Identifying the transition entry $T_{ij} = \text{probablility for }i \rightarrow j$:\\[1em]
$
\begin{array}{r|ccc}
\text{state }i\backslash{j} & 1 & 2 & 3\\
\hline
        1 & 1/2 &   1/2 &   0 \\
        2 & 1/2 &   1/4 & 1/4 \\
        3 & 0   &     1 &   0 \\
\end{array}
$,\qquad
hence $T = \displaystyle{\frac{1}{4}}
\left[
\begin{array}{rrr}
2 &   2 &   0 \\
2 &   1 &   1 \\
0 &   4 &   0 \\
\end{array}
\right]$.

\bigskip
\noindent
The state transition diagram for the transition matrix $T$ is:

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
  shorten >=1pt,
  node distance = 2, auto]
  \node[state,accepting] (a) {$1$};
  \node[state,accepting] (b) [right = of a] {$2$};
  \node[state,accepting] (c) [right = of b] {$3$};
  \path[->,thick] (a) edge[bend left] node {$1/2$} (b)
                      edge [loop above] node {$1/2$} ()
                  (b) edge[bend left] node {$1/2$} (a)
                      edge[bend left] node {$1/4$} (c)
                      edge [loop above] node {$1/4$} ()
                  (c) edge[bend left] node {$1$} (b);
\end{tikzpicture}

\noindent
For a $3\times{3}$ transition matrix $T$,
the steady state vector $S$ is given by a formula:
\begin{equation}
T = 
\left[
\begin{array}{ccc}
t_{11} & t_{12} & t_{13}\\
t_{21} & t_{22} & t_{23}\\
t_{31} & t_{32} & t_{33}\\
\end{array}
\right],
\qquad
S = \frac{1}{\sigma_{1} + \sigma_{2} + \sigma_{3}} 
\left[
\begin{array}{c}
\sigma_{1}\\
\sigma_{2}\\
\sigma_{3}\\
\end{array}
\right]
\end{equation}
where, for $\{i, j, k\} = \{1, 2, 3\}$,
$\sigma_{i} = t_{kj}t_{ji} + t_{ji}t_{ki} + t_{ki}t_{jk}$.

\bigskip
Recall that a set is unorderd, so there are $P(3,3) = 3! = 6$ way to arrange the indices $i,j,k$.
They only produce $3$ distinct values, as the table shows for this example:
\[
\begin{array}{ccc|rll}
i & j & k & \sigma_{i}\\
\hline
1 & 2 & 3 & \sigma_{1} & = t_{32}t_{21} + t_{21}t_{31} + t_{31}t_{23}
                       & = 1/2 \\
1 & 3 & 2 & \sigma_{1} & = t_{23}t_{31} + t_{31}t_{21} + t_{21}t_{32}
                       & = 1/2 \\
2 & 3 & 1 & \sigma_{2} & = t_{13}t_{32} + t_{32}t_{12} + t_{12}t_{31}
                       & = 1/2\\
2 & 1 & 3 & \sigma_{2} & = t_{31}t_{12} + t_{12}t_{32} + t_{32}t_{13}
                       & = 1/2\\
3 & 1 & 2 & \sigma_{3} & = t_{21}t_{13} + t_{13}t_{23} + t_{23}t_{12}
                       & = 1/8\\
3 & 2 & 1 & \sigma_{3} & = t_{12}t_{23} + t_{23}t_{13} + t_{13}t_{21}
                       & = 1/8\\
\end{array}
\]
Thus $\sigma_{1} + \sigma_{2} + \sigma_{3} = 1/2 + 1/2 + 1/8 = 9/8$, and
\[
 S = \frac{8}{9} \left[\begin{array}{c} 1/2\\1/2\\1/8 \end{array} \right]
   = \left[\begin{array}{c} 4/9\\4/9\\1/9 \end{array} \right]
\]
You can check that $T'S = S$.

% References
% 
% First Links in the Markov Chain (By Brian Hayes)
% https://www.americanscientist.org/article/first-links-in-the-markov-chain
% Probability and poetry were unlikely partners in the creation of a computational tool
% American Scientist: Issue March-April 2013 Volume 101, Number 2 Page 92
% DOI: 10.1511/2013.101.92

\section{Markov Process --- Live!}
You can see Markov process in action:
\begin{enumerate}[label={[\arabic*]}] 
\item Markov Chains: Explained Visually (by Victor Powell, text by Lewis Lehe)\\
\url{http://setosa.io/ev/markov-chains/}\\
Read and scroll to find a link to a fullscreen version.
% You can also access a fullscreen version at setosa.io/markov
\item Markov Chain Simulator (by Yori Zwols, 2014)\\
\url{http://markov.yoriz.co.uk}\\
Try to choose and run different examples.
\end{enumerate}

% \newpage
\section{Answers}
\textbf{Exercise 1} (page~\pageref{q:ex-1}).
We can verify
\[
\begin{array}{rl}
    T' S = &
   \left[\begin{array}{cc} (1-p) & q\\ p & (1-q) \end{array}\right]
             \left[\begin{array}{c} q/(p + q) \\ p/(p + q)\end{array}\right]\\
  = & \left[\begin{array}{c} ((1-p)q + qp)/(p + q)\\
                             (pq + (1-q)p)/(p + q)\end{array}\right]\\  
         = & \left[\begin{array}{c} q/(p + q) \\ p/(p + q)\end{array}\right]
          = S\\  
\end{array}
\]
or we can compute by the short-cut method using $(T' - I)$, by Wolfram Alpha:
\begin{verbatim}
solve a, b in [[-p, q], [1, 1]] . [a, b] = [0, 1]
\end{verbatim}
% WolframAlpha: solve a, b in  ((-p, q), (1, 1)) . (a, b) = (0, 1)
%    b = 1 - a and q = 0 and p = 0
%    a = q/(p + q) and b = p/(p + q) and p + q != 0
% [[-p, q], [1, 1]] . [a, b] = [0, 1]
% Wolfram Alpha solves b and q.

\bigskip
\noindent
\textbf{Exercise 2} (page~\pageref{q:ex-2}).
% What happens when $p = 0$ ? Figure out its transition matrix.
The state transition diagram becomes:

\bigskip
\noindent
\begin{tikzpicture}[
  shorten >=1pt,
  node distance = 2, auto]
  \node[state,accepting] (a) {$\mathtt{A}$};
  \node[state,accepting] (b) [right = of a] {$\mathtt{B}$};
  \path[->,thick] (a) edge[loop left] node {$1$} ()
                  (b) edge[loop right] node {$1$} ();
\end{tikzpicture}

\bigskip
\noindent
Each state is a sink, and the transition matrix 
$T = \left[\begin{array}{rr}1 & 0\\0 & 1\end{array}\right] = I$, the indentity matrix.
Any initial state vector is unchanged, thus a steady state vector.

\bigskip
\noindent
\textbf{Exercise 3}. (page~\pageref{q:ex-3})
% Can we use $T^{n}$ instead to discover the steady state vector $S$?
Yes, see C3 slide 21, but $T^{n}$ have rows of steady state vector $S$ instead of columns for $(T')^{n}$. This is applied in Worksheet 7 Q2(a).



\end{document}

Extra:

% pdflatex info06.tex