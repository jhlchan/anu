\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams
\usepackage{caption} % for captionsetup
\usepackage[hang,flushmargin]{footmisc} % remove footnote indent

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc, fit} % fit for box
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
\usepackage{titling} % for \droptitle

\usepackage{pgfplots} % for function plotting

% special marks
\usepackage{amssymb}% http://ctan.org/pkg/amssymb
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}% check tick
\newcommand{\xmark}{\ding{55}}% cross X

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
\renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}

% show division
\newcommand\showdiv[1]{\ensuremath{\overline{\smash{)}#1}}}
\newcommand\Mydiv[2]{%
$\strut#1$\kern.25em\smash{\raise.3ex\hbox{$\big)$}}$\mkern-8mu
        \overline{\enspace\strut#2}$}

% \setlength{\droptitle}{-7em}   % This is your set screw, more -ve higher
\title{Worksheet 4: comments}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

% Math symbols
% https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols
% Text colors
% https://en.wikibooks.org/wiki/LaTeX/Colors

This worksheet looks at several topics I have already covered in my info02.pdf:
summation, sequences, mathetmatical induction proofs, and sorting. 

For sequences and summation, there are online tools to verify the results,
\emph{e.g.},
Wolfram Alpha: a symbolic calculator
\url{https://www.wolframalpha.com/}.
For sorting, we need to trace pseudocode of algorithms.
With Javascript, we can use
JavaScript Shell 1.4:
\url{https://www.squarefree.com/shell/shell.html}
to run it online.

Note that Wolfram Alpha accepts only single-line input,
while JavaScript Shell allows multi-line input.
Copy/paste text to the appropriate online tool.

\paragraph{Q1}
Sum, product and sequence.
\begin{enumerate}[label=(\alph*)]
\item The sum
$\displaystyle{\sum_{k = 1}^{5}\;(k - 1)^{2} = \sum_{j = 0}^{4}\;j^{2}}$
\quad by a change of index, a useful technique.
The product $\displaystyle{\prod_{k = 1}^{5}\;\left(\frac{k + 1}{k}\right)}$
is simple because it is telescoping.

You can verify the computation by Wolfram Alpha (one line each time):
\begin{verbatim}
sum of (k - 1)^2 from 1 to 5
sum of j^2 from 0 to 4
product of (k + 1)/k from 1 to 5
\end{verbatim}
Wolfram Alpha is a \textbf{symbolic} calculator, so you can type in:
\begin{verbatim}
sum of j^2 from 1 to n
product of (k + 1)/k from 1 to n
\end{verbatim}
giving the general formula, and a table of a few values (scroll down).

\item This is a recurrence formula, with $a_{n+1}$ given by $a_{n}$ and $a_{n-1}$,
\emph{i.e.}, two preceding values.
Given $a_{0}$ and $a_{1}$, this gives $a_{2}$, then $a_{3}$, \emph{etc.}, successively.

Wolfram Alpha can give the first few values:
\begin{verbatim}
a(0) = 2, a(1) = 3, a(n+1) = a(n)(a(n-1) - 1)
\end{verbatim}
% a(n+1) = a(n)(a(n-1) - 1), a(0) = 2, a(1) = 3  (* not the same! *)
There seems to be no way to display more values.

In Javascript, we can first initialise the sequence:
\begin{verbatim}
var a = []; // sequence a_n
a[0] = 2;
a[1] = 3;
\end{verbatim}

Then compute the next values up to index $k$:
% separate out to avoid extra copy/paste with displaystyle above.
% \newpage
\begin{verbatim}
// compute up to a[k]
function upto(k) {
   var j = 1;
   while (j < k) {
      a[j+1] = a[j] * (a[j-1] - 1); // recurrence
      j = j + 1;
      print("a["+j+"] = "+a[j]); // print value
   }
}
// run as
upto(10);
\end{verbatim}


\end{enumerate}

\paragraph{Q2}
An \emph{arithmetic} sequence has starting value $a$ and common difference $d$:
\begin{itemize}
\item Given: $a_{0} = a, a_{n} = a_{n-1} + d$ (implicit form)
\item To prove: $a_{n} = a + n d$ (explicit form)
\end{itemize}
The proof skeleton, or template, is given, for you to fill in.
This example takes $n \in \mathbb{N}^{*}$, with $n = 0$ for the basis step.
I have explained in info02.pdf that the conclusion ``By mathematical induction \dots'' is part of the pattern.
The formula to show has the form $\mathtt{LHS} = \mathtt{RHS}$,
thus both the basis step and  inductive step should start from $\mathtt{LHS}$
and transform to $\mathtt{RHS}$.
A direct proof can go like this:
\[
\begin{array}{r@{\;=\;}ll}
             a_{0} & a & \text{initial}\\
     a_{1} - a_{0} & d & \text{by given}\\
     a_{2} - a_{1} & d & \\
     \dots         & \dots & \\
     a_{n} - a_{n-1} & d & \\
\hline
             a_{n} & a + nd & \text{by total}\\
\end{array}
\]
but this is not a proof by mathematical induction.

\paragraph{Q3} The \emph{arithmetic} series (Q2 is sequence).
\begin{enumerate}[label=(\alph*)]
\item Prove by induction that $\mathtt{sum} = \mathtt{result}$.
Apply the template for inductive proof (Q2), and go from left to right.
You'll need to know how to break out the last term of a sum.
Remember the conclusion!
\item Use summation to convert a sequence to a series (B2 slide 10).
You are asked to show, for the sequence in Q2:
\begin{equation}
\label{eqn:arithmetic-series}
\displaystyle{\sum_{j=0}^{n-1} a_{j} = n\left(\frac{a_{0} + a_{n-1}}{2}\right)}
\end{equation}
\end{enumerate}
You need to think of ways to manipulate summation.
Check that the inductive hypothesis is used in the inductive step,
and include the words: \emph{by mathematical induction} in conclusion.


Using unit squares (squares of $1\times{1}$), the left-side sum of Equation~\eqref{eqn:arithmetic-series} is
represented by the area in blue outline (see the figure).
This is equal to the area of the red outline (in middle),
which can be computed by average in green outline (at right):

\noindent
\begin{tikzpicture}[scale=0.5] % using a scale
\node at (0,5.5) {}; // dummy
% the grid
\draw[step=1, color=black!90!white] (-1, -2) grid (5, 5);
\draw[blue, ultra thick]
     (0,-2) -- (0,0) -- (0,1) -- (1,1) -- (1,2) --
     (2,2) -- (2,3) -- (3,3) -- (3,4) -- (4,4) -- (4,5) -- (5,5) -- (5,-2) -- (0,-2);
% labels
\node at (0,-0.5) [left]{$a$};
\node at (1,1.5) [left]{$d$};
\node at (2,2.5) [left]{$d$};
% \node at (3,2.5) [left]{$d$};
\node at (4,4.5) [left]{$d$};
\node at (5,2.5) [right]{$a + (n-1)d$};
\node at (3,-2) [below]{$n$};
% another grid
\draw[step=1, color=black!90!white] (10, -2) grid (16, 5);
\draw[blue, ultra thick]
     (11,-2) -- (11,0) -- (11,1) -- (12,1) -- (12,2) --
     (13,2) -- (13,3) -- (14,3) -- (14,4) -- (15,4) -- (15,5) -- (16,5) -- (16,-2) -- (11,-2);
\draw[red, ultra thick]
     (11,-2) -- (11,1) -- (16,5) -- (16,-2) -- (11,-2);
% this is not better than the one before
% \draw[red, thick] (10,-2) -- (10,0) -- (16,5) -- (16,-2) -- (11,-2);
\node at (14,-2) [below]{$n$};
% last grid
\draw[step=1, color=black!90!white] (17, -2) grid (23, 5);
\draw[red, ultra thick]
     (18,-2) -- (18,1) -- (23,5) -- (23,-2) -- (18,-2);
\draw[green!60!black, ultra thick]
     (18,-2) -- (18,3) -- (23,3) -- (23,-2) -- (18,-2);
\node at (21,-2) [below]{$n$};
\end{tikzpicture}


\paragraph{Q4} Compound Interest and Debt.
\begin{enumerate}[label=(\alph*)]
\item Another induction proof of a formula $\mathtt{LHS} = \mathtt{RHS}$, just apply the template.
Input into Wolfram Alpha:
\begin{verbatim}
a(n) = r * a(n-1) - f
\end{verbatim}
gives the recurrence solution:
$\displaystyle{a_{n} = c_{1} r^{n-1} - \frac{f (r^{n} - 1)}{r - 1}}$.
Is this the same solution as given in Q4?

The induction proof is another exercise doing from left to right.
The basis step puts $n = 1$ for $n \in \mathbb{N}$, and uses $r^{0} = 1$.
The inductive step involves summation manipulation:
\[
\begin{array}{r@{\;=\;}ll}
a_{n+1} & r a_{n} - f & \text{by implicit definition}\\
   & r \displaystyle{\left(a r^{n} - f \sum_{k=0}^{n-1} r^{k}\right)} - f &
             \text{by inductive assumption}\\[1em]
   & a r^{n+1} - f \displaystyle{\left(\sum_{k=0}^{n-1} r^{k+1} + 1\right)} & \text{rearrange}\\[1em]
   & a r^{n+1} - f \displaystyle{\left(\sum_{k=1}^{n} r^{k} + 1\right)} & \text{change index}\\[1.2em]
   & a r^{n+1} - f \displaystyle{\sum_{k=0}^{n} r^{k}} & \text{merge summation term, }r^{0} = 1\\[1.2em]
   & a r^{n+1} - f \displaystyle{\sum_{k=0}^{n+1-1} r^{k}} & \text{rearrange as RHS for }(n+1)\\
\end{array}
\]
You need to provide the conclusion to complete the proof.
\item For numerical calculation, convert $10$ years to months.
Note that the debt after $10$ years is still greater than the borrowed amount,
a result of compound interest!
\end{enumerate}

\paragraph{Q5} Algorithm tracing.\\
The trace table is the same in the Least element algorithm (B2 slide 23):

\noindent
\includegraphics[scale=0.43]{least-element-algorithm.png} 

\noindent
For part (a), stage $s = 1$, index $i \leftarrow 2$ and marker $m \leftarrow 1$.
The index $i$ increments as it loops, but the marker $m$ moves only when
the indexed character is ``less'': $x_{\pi(i)} < x_{\pi(m)}$.
At the end of the loop,
the marker points to the least element from $s$ to end of the word,
thus the swap of indexes to ensure that $x_{\pi(s)}$ has the least element.
The rows $i$ and $\pi(i)$ form the indexed function, or permutation (B2 slide 21), for part (b).

Parts (c) and (d) traces stage $s = 2$.
Part (e) skips the trace, just the permutation for subsequent stages.
Part (f) counts the number of comparisons in Selection Sort,
and do the same for the Merge Sort in part (g).
Use the method in B2 slide 25, B2 slide 31, or read my notes in info02.pdf.



\paragraph{Q6} Merge sort in general.\\
Once you can follow the example of $\mathtt{PROVISIONAL}$ with comparison counts,
you can perform the merge in stages (B2 slide 31).
Read my notes in info02.pdf for complete examples.
The count of Merge Sort comparisons should be smaller than the count of Selection Sort comparison, for the same word.

\section*{Selection Sort and Merge Sort}
There are psudocode for these sorting algorithms (see slides or my notes),
but it is important to see the central idea behind the algorithm.

The idea for \textbf{selection sort} is this: \emph{grow the sorted}.
For example,
selection sort of $\mathtt{PROVISIONAL}$ proceeds as (sorted part in brackets):
\[
\begin{array}{rll}
             & \mathtt{()PROVISIONAL} & \text{nothing sorted}\\
\rightarrow  & \mathtt{(A)PROVISIONL} & \text{take the least of unsorted}\\
\rightarrow  & \mathtt{(AI)PROVSIONL} & \text{add the least of unsorted\footnotemark}\\
\rightarrow  & \mathtt{(AII)PROVSONL} & \text{\emph{etc.}}\\
\dots\\
\rightarrow  & \mathtt{(AIILNOOPRS)V} & \text{last remains}\\
\rightarrow  & \mathtt{(AIILNOOPRSV)} & \text{done, all sorted}\\
\end{array}
\]
\footnotetext{For the two equal $\mathtt{I}$'s, the (arbitrary) rule in B2 slide 24 takes the first one: if less, move the marker $m$. This means, if equal, don't move marker $m$.}

The idea for \textbf{merge sort} is this: \emph{keep the sorted}.
This means the merge sort has to start with something sorted: the trivial one-element lists!
For example,
merge sort of $\mathtt{PROVISIONAL}$ is given in Q6. I shall rewrite as follows in stages:
\[
\begin{array}{rll}
0 & \mathtt{(P)(R)(O)(V)(I)(S)(I)(O)(N)(A)(L)} & \text{sorted, by default}\\
\hline
1
& \text{merge }\mathtt{(P)(R)\rightarrow P()(R)\overset{copy}{\longrightarrow}PR} &
 1\text{ compare}\\
& \text{merge }\mathtt{(O)(V)\rightarrow O()(v)\overset{copy}{\longrightarrow}OV} &
 1\text{ compare}\\
& \text{\emph{etc.} for merge singleton pairs} & 3\text{ more compare's}\\
& \mathtt{(E)}  & \text{remains}\\
\hline
2
& \mathtt{(PR)(OV)\rightarrow O(PR)(V)\rightarrow OP(R)(V)\rightarrow OPR()(V)\overset{copy}{\longrightarrow}OPRV} &
 3\text{ compare's}\\
& \mathtt{(IS)(IO)\rightarrow I(IS)(O)\rightarrow II(S)(O)\rightarrow IIO(S)()\overset{copy}{\longrightarrow}IIOS} &
 3\text{ compare's}\footnotemark\\
& \mathtt{(AN)(L)\rightarrow A(N)(L)\rightarrow AL(N)()\overset{copy}{\longrightarrow}ALN} &
 2\text{ compare's}\\
\hline
3
& \mathtt{(OPRV)(IIOS)\rightarrow I(OPRV)(IOS)\rightarrow II(OPRV)(OS)}\rightarrow &\\
& \mathtt{IIO(OPRV)(S)\rightarrow IIOO(PRV)(S)\rightarrow IIOOP(RV)(S)}\rightarrow & \text{eq take from 2nd}\\
& \mathtt{IIOOPR(V)(S)\rightarrow IIOOPRS(V)()\overset{copy}{\longrightarrow}IIOOPRSV}  & 7\text{ compare's}\\
& \mathtt{(ANL)}  & \text{remains}\\
\hline
4
& \mathtt{(IIOOPRSV)(ALN)\rightarrow A(IIOOPRSV)(LN)}\rightarrow  &\\
& \mathtt{AI(IOOPRSV)(LN)\rightarrow AII(OOPRSV)(LN)}\rightarrow  &\\
& \mathtt{AIIL(OOPRSV)(N)\rightarrow AIILN(OOPRSV)()}\overset{copy}{\longrightarrow}\mathtt{AIILNOOPRSV} & 5\text{ compare's}\\
\hline
\end{array}
\]
\footnotetext{For the two equal $\mathtt{I}$'s, the (arbitrary) rule in B2 slide 27 takes the second one: if less, take from $a$-list, else take from $b$-list. This means if equal, take from $b$-list.}

\noindent
This is stated in Q6: 
for $\mathtt{PROVISIONAL}$ there are only $5$ comparisons during the first iteration, $8$ in the 2nd, $7$ in the 3rd and $5$ in the last.

For the merge sort of a word with $n$ alphabets,
there is no formula for the number of comparisons: you have to count carefully by going through each iteration (B2 slide 31).
However, the number of \emph{transfers} (B2 slide 32), has a formula $T_{r}$, where $n = 2^{r}$ (for those who knows, $r = \log_{2}n$). Each comparison is a transfer, and the last copy of $x$ remaing alphabets is counted as $x$ transfers.

For the selection sort of a word with $n$ alphabets,
the sorted portion grows (remember?) with lengths $0, 1, \dots (n-1)$,
and each time the first get swapped by checking the least of the remaing $n-1, n-2, \dots, 0$ alphabets.
Thus the number of comparison is always $\frac{1}{2}n(n-1)$ (see Q3(a), or B2 slide 25).

\section*{Javascript codes for sorting}
With these ideas for Selection Sort and Merge Sort, 
their pseudocodescan be implemented in Javascript.

First, you need to know how to convert a string to an array (of characters), and undo that:
\begin{verbatim}
"happyeverafter".split(""); // string to char array
"happyeverafter".split("").join(""); // char array back to string
\end{verbatim}
Once you have an array, you can examine its elements by shifting:
\begin{verbatim}
var a = "happyeverafter".split(""); // a char array
a.shift();  // get 'h'
a.shift();  // get 'a'
a;          // the array now
a.length;   // how long?
a[0];       // the head
a[1];       // the next
\end{verbatim}
You can build an array from an empty one by pushing:
\begin{verbatim}
var c = [];  // empty
c.push("b");
c.push("y");
c.push("e"); // returns c.length
c;           // the array of "bye"
c[0];        // the head
\end{verbatim}
The position of a character in a string is its index, which counts from $0$:
\begin{verbatim}
"happy".indexOf("y");  // where is 'y'?
c.join("").indexOf("y"); // 'y' in "bye"
\end{verbatim}
Javascript knows $2 < 5$, but it doesn't know that $\mathtt{B} < \mathtt{K}$.
We have to teach Javascript to compare two characters:
\begin{verbatim}
// Define all the alphabets
var alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// compare characters x, y
function less(x, y) {
    return alphabets.indexOf(x) < alphabets.indexOf(y);
}
// testing
less("C", "E");
less("E", "C");
less("E", "E");
\end{verbatim}
\bigskip
\noindent
For \textbf{selection sort}, we can modify the Least Element Algorithm (B2 slide 23):
\begin{verbatim}
// select the least in a list and swap to front
function select(x) { // x = character list
    var k = 0; // mark itself
    var j = 1; // no need to compare itself
    // locate the least to swap
    while (j < x.length) {
        if (less(x[j],x[k])) k = j; // move marker
        j = j + 1;
    }
    // swap x[k] with x[0]
    var c = x[0];
    x[0] = x[k];
    x[k] = c;
    return x; // front has least
}
// testing
select("ALBUM".split(""));
select("BLAUM".split(""));
\end{verbatim}
% \newpage
% \noindent
Then we can do the Selection Sort Algorithm (B2 slide 24):
\begin{verbatim}
// selection sort of a word
function select_sort(word) {
    var x = word.split(""); // to character list
    var y = []; // the sorted word
    while (x.length > 0) {
        x = select(x); // swap least to front
        y.push(x.shift()); // grow the sorted!
        print(y.join("")+"("+x.join("")+")");
    }
    return y.join(""); 
}
// run as:
select_sort("ALBUM");
\end{verbatim}
\bigskip
\noindent
For \textbf{merge sort}, we first modify the Merge Algorithm (B2 slide 27):
% or tick tock, or tik tok
\begin{verbatim}
// merge two strings
function merge(dee, dum) {
    var a = dee.split(""); // to character list
    var b = dum.split(""); // to character list
    var c = [];
    print("merge: ("+a.join("")+")("+b.join("")+")");
    while ((a.length !== 0) || (b.length !== 0)) {
        if (a.length == 0) { // empty a
            c.push(b.shift());
        }
        else if (b.length == 0) { // empty b
            c.push(a.shift());
        }
        else {
            if (less(a[0], b[0])) { // less, take from a
                c.push(a.shift());
            }
            else { // otherwise, take from b
                c.push(b.shift());
            }
        }
        print(c.join("")+"("+a.join("")+")("+b.join("")+")");
    }
    return c.join("");
}
// testing:
merge("ACEF", "BE");
\end{verbatim}
\newpage
\noindent
Then we follow the example in Q6 for Merge Sort (\emph{not} B2 slide 29):
\begin{verbatim}
// merge sort of a word
function merge_sort(word) {
    var pool = word.split(""); // the pool of sorted lists
    pool.push(""); // mark stage end
    while (pool.length > 2) {
        if (pool[0] == "") { // end of stage
            print("next");
            pool.push(pool.shift()); // put marker to back
        }
        else if (pool[1] == "") { // not a pair
            print("skip");
            pool.push(pool.shift()); // put singleton to back
            pool.push(pool.shift()); // put marker to back
        }
        else { // merge a pair: keep the sorted!
            pool.push(merge(pool.shift(), pool.shift()));
        }
        print("pool: " + pool);
    }
    return pool.join(""); // eliminate marker
}
// run as:
merge_sort("ALBUM");
\end{verbatim}
\bigskip
\noindent
\textbf{Exercise}\\
Add a global comparison count, and increment it for every $\mathtt{less}$ call:
\begin{verbatim}
var comparison = 0; // global count
// compare characters x, y
function less(x, y) {
    comparison = comparison + 1; // increment count  
    return alphabets.indexOf(x) < alphabets.indexOf(y);
}
\end{verbatim}
You can modify \verb|select_sort(word)| and \verb|merge_sort(word)|
by first resetting the count to $0$, then print the count before the final $\mathtt{return}$.


\end{document}

Extra code (sorting with counts):

function select_sort(word) {
    comparison = 0;
    var x = word.split(""); // to character list
    var y = []; // the sorted word
    while (x.length > 0) {
        x = select(x); // swap least to front
        y.push(x.shift()); // grow the sorted!
        print(y.join("")+"("+x.join("")+")");
    }
    print("comparisons: "+comparison);
    return y.join("");
}
select_sort("TROUNCED"); // count = 28

function merge_sort(word) {
    comparison = 0;
    var pool = word.split(""); // the pool of sorted lists
    pool.push(""); // mark stage end
    while (pool.length > 2) {
        if (pool[0] == "") { // end of stage
            print("next");
            pool.push(pool.shift()); // put marker to back
        }
        else if (pool[1] == "") { // not a pair
            print("skip");
            pool.push(pool.shift()); // put singleton to back
            pool.push(pool.shift()); // put marker to back
        }
        else { // merge a pair: keep the sorted!
            pool.push(merge(pool.shift(), pool.shift()));
        }
        print("pool: " + pool);
    }
    print("comparisons: "+comparison);
    return pool.join(""); // eliminate marker
}
merge_sort("TROUNCED"); // count = 14

merge_sort("PROVISIONAL"); // count = 25
select_sort("PROVISIONAL"); // count = 55

merge_sort("APPROPRIATION"); // count = 37
select_sort("APPROPRIATION"); // count = 78

% Insertion sort (GradAssB and GradAssC)

function insert_sort(word) {
    var a = word.split(""); // to character list
    var n = a.length;
    a.unshift(""); // make index start from 1
    var i = 2;
    var x;
    var j;
    var k = 0;
    print("i="+i+": "+a);
    while (i <= n) {
       x = a[i]; // item to be sorted
       j = i - 1; // mark item to be compared
       while (j > 0) {
          if (debug) { print("before j="+j+": x="+x+", a[j]="+a[j]); };
          k++;
          if (less(x,a[j])) {
             a[j+1] = a[j]; // slide to right
             j = j - 1;
          }
          else {
             a[j+1] = x; // insert here
             j = -1;
          }
          if (debug) { print("after j="+j+": x="+x+", a="+a); };
       }
       if (j == 0) a[1] = x; // not inserted, do now
       i = i + 1;
       print("i="+i+": "+a);
    }
    return k; // the comparisons  
}

insert_sort("FDCEBC");
i=2: ,F,D,C,E,B,C
before j=1: x=D, a[j]=F
after j=0: x=D, a=,F,F,C,E,B,C
i=3: ,D,F,C,E,B,C
before j=2: x=C, a[j]=F
after j=1: x=C, a=,D,F,F,E,B,C
before j=1: x=C, a[j]=D
after j=0: x=C, a=,D,D,F,E,B,C
i=4: ,C,D,F,E,B,C
before j=3: x=E, a[j]=F
after j=2: x=E, a=,C,D,F,F,B,C
before j=2: x=E, a[j]=D
after j=-1: x=E, a=,C,D,E,F,B,C
i=5: ,C,D,E,F,B,C
before j=4: x=B, a[j]=F
after j=3: x=B, a=,C,D,E,F,F,C
before j=3: x=B, a[j]=E
after j=2: x=B, a=,C,D,E,E,F,C
before j=2: x=B, a[j]=D
after j=1: x=B, a=,C,D,D,E,F,C
before j=1: x=B, a[j]=C
after j=0: x=B, a=,C,C,D,E,F,C
i=6: ,B,C,D,E,F,C
before j=5: x=C, a[j]=F
after j=4: x=C, a=,B,C,D,E,F,F
before j=4: x=C, a[j]=E
after j=3: x=C, a=,B,C,D,E,E,F
before j=3: x=C, a[j]=D
after j=2: x=C, a=,B,C,D,D,E,F
before j=2: x=C, a[j]=C
after j=-1: x=C, a=,B,C,C,D,E,F
i=7: ,B,C,C,D,E,F
13
debug = false;
false
insert_sort("FDCEBC");
i=2: ,F,D,C,E,B,C
i=3: ,D,F,C,E,B,C
i=4: ,C,D,F,E,B,C
i=5: ,C,D,E,F,B,C
i=6: ,B,C,D,E,F,C
i=7: ,B,C,C,D,E,F
13

insert_sort("ABC");  ABC, ABC, ABC 2 (given)
insert_sort("ACB");  ACB, ACB, ABC 3
insert_sort("BAC");  BAC, ABC, ABC 2
insert_sort("BCA");  BCA, BCA, ABC 3
insert_sort("CAB");  CAB, ACB, ABC 3
insert_sort("CBA");  CBA, BAC, ABC 3 (given)


% pdflatex sheet04.tex