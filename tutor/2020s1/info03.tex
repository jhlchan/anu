\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
% \usepackage{ifthen} % has \ifthenelse, but PGF Math Error: unknown o or of
\usepackage{todonotes} % has \ifthenelse !

% special marks
\usepackage{amssymb} % http://ctan.org/pkg/amssymb, for \because and \therefore

% Graphics for Triangle
\newlength\radius
\pgfmathsetlength{\radius}{0.25cm}

% Introduce a counter for label
\newcounter{nodecount} % give counter a name
\setcounter{nodecount}{0} % count from 0
\newcommand\Square[1]{+(-#1,-#1) rectangle +(#1,#1)} % the square background

% #1 = placement, e.g. xshfit=2.1cm, optional, as []
% #2 = number of rows, e.g. 4, to determine numbering scheme
\newcommand\DrawTriangle[2][]{%   [n] = total n args, [] = #1 is optional
    \foreach \x in {1,...,#2} {%
     % let #2 = 5, then x = 1,2,3,4,5
        \foreach \y in {1,...,\x} {%
        % here y = 1..1 first, then y = 1..2, ..., last y = 1..5
        \ifthenelse{\x=#2}{%
              % draw the ball
              \draw[fill=black,#1]
                  ({sqrt(3)*\x*\radius},{sqrt(3)*\y*\radius}) circle (0.8*\radius);
        }%
        {%
              % draw the ball
              \draw[fill=white,#1]
                  ({sqrt(3)*\x*\radius},{sqrt(3)*\y*\radius}) circle (0.8*\radius);
        }%          
        }; % end for /y
    }% end for /x
    % update count and put count label
    \stepcounter{nodecount};
    \node[#1] (step-\thenodecount) {}; % \the<name> to get value of counter <name>
    \edef\n{\number\numexpr (\thenodecount) \relax} % n = node count
    \edef\t{\number\numexpr (\n * (\n + 1) / 2) \relax} % the triangle number
    % \node[right=5pt of step-\n] () {$T_{\n} = \t$}; % the label
    % \node[right=5pt of step-\n, below=10pt] () {$T_{\n} = \t$}; % the label
    % \node[below=0pt of step-\n] () {$T_{\n} = \t$}; % the label
    \node[below=0pt of step-\n, xshift=25pt] () {$T_{\n} = \t$}; % the label
}

% #1 = placement, e.g. xshfit=2.1cm, optional, as []
% #2 = number of rows, e.g. 4, to determine numbering scheme
\newcommand\DrawSquare[2][]{%   [n] = total n args, [] = #1 is optional
    \foreach \x in {1,...,#2} {%
     % let #2 = 5, then x = 1,2,3,4,5
        \foreach \y in {1,...,#2} {%
        % here y = 1..5
        \ifthenelse{\x=#2 \OR \y=#2}{%
              % draw the ball
              \draw[fill=black,#1]
                  ({sqrt(3)*\x*\radius},{sqrt(3)*\y*\radius}) circle (0.8*\radius);
        }%
        {%
              % draw the ball
              \draw[fill=white,#1]
                  ({sqrt(3)*\x*\radius},{sqrt(3)*\y*\radius}) circle (0.8*\radius);
        }%          
        }; % end for /y
    }% end for /x
    % update count and put count label
    \stepcounter{nodecount};
    \node[#1] (step-\thenodecount) {}; % \the<name> to get value of counter <name>
    \edef\n{\number\numexpr (\thenodecount) \relax} % n = node count
    \edef\t{\number\numexpr (\n * \n) \relax} % the square number
    \node[below=0pt of step-\n, xshift=25pt] () {$S_{\n} = \t$}; % the label
}

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

\title{Induction, Summation, Difference and Proofs}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Pattern}
Ordinarily, induction means the generalisation of a pattern to suggest a result.
In math, a proof by \emph{mathematical induction} is a clever trick to prove formally the suggested result.
The proof pattern can be distilled from B2 slides 6, 9, 14:

\begin{table}[h]
\begin{tabular}{ll}
(To prove):  & (a formula involing $n$)\\
\textbf{Basis Step}: & (prove the case or $n = \text{start}$, can be $0$ or $1$, or others)\\
\textbf{Inductive Step}: & \textbf{Assume the formula is true for some fixed $n$.}\\
            & \textbf{Then} (prove the case $n+1$) \\
(Conclude): & \textbf{By mathematical induction,}\\
            & \textbf{the formula is true for all $n$}.\\
\end{tabular}
\end{table}

\noindent
You must include all the \underline{words in bold} for a complete proof --- don't be lazy!
In the lecture slides (B2 slides 5,6,9,14), the conclusion appears as the declaration at the start. When working in the Inductive Step, at some point you should state `\textbf{by inductive assumption}' (as in lecture slides), otherwise it is not really a proof by mathematical induction.

\section{Example}
For the number of transfers in merge sort (B2 slide 32)
you are asked to verify this by induction (\emph{i.e.}, mathematical induction):

\bigskip
\noindent
\textbf{Claim}: $\forall{r} \in \mathbb{N}\;\; T_{r} = r 2^{r}$ where
\[
\begin{cases}
      T_{1} = 2\\
      T_{r} = 2^{r} + 2 T_{r-1}\; \forall r \in \mathbb{N}\setminus\{ 1 \}
    \end{cases}
\]
\paragraph{Proof}
By induction on $r$.\\
Basis Step: when $r = 1$,
\[
\begin{array}{r@{\; = \;}ll}
T_{1} & 2              & \text{by definition}\\
      & 1\times{2^{1}} & \text{by arithmetic}\\  
\end{array}
\]
$\therefore$ The formula is true for $r = 1$.\\
Inductive Step: assume the formula is true for some fixed $r \in \mathbb{N}$. Then
\[
\begin{array}{r@{\; = \;}ll}
T_{r+1} & 2^{r+1} + 2 T_{r}     & \text{by definition}\\
        & 2^{r+1} + 2 (r 2^{r}) & \text{by inductive assumption}\\
        & 2^{r+1} + r 2^{r+1}   & \text{by arithmetic}\\  
        & (r+1) 2^{r+1}         & \text{by arithmetic}\\  
\end{array}
\]
Thus the formula is true for $r + 1$.\\
By mathematical induction, the formula is true for all $r \in \mathbb{N}$.
$\Box$

\section{Reasoning}
What have we proved? Let $P(r)$ be the following predicate (a sentence with a variable that becomes a statement when the variable has a value, A1 slide 45):
\[
P(r) = (\forall{r} \in \mathbb{N}\;\; T_{r} = r 2^{r})
\]
\begin{itemize}
\item Basis step proves $P(1)$ is true.
\item Inductive step proves this implication: $P(r) \Rightarrow P(r+1)$  is true (recall \emph{assume} $\dots$ \emph{then} $\dots$). Neither $P(r)$ nor $P(r+1)$ is proved, but this gives the bridge to go from $r$ to its next: \textbf{if} $P(r)$ is true, \textbf{then} $P(r+1)$ is true.
\item Conclusion (which you should write every time, like a citation) means:
\begin{itemize}[label=$\diamond$]
\item since $P(1)$ is true, $P(2)$ is true by implication,
\item since $P(2)$ is true, $P(3)$ is true by implication,
\item $\dots$
\item (I can go on forever, but you get the idea.)
\end{itemize}
Without the conclusion, you are missing an important piece of reasoning!
\end{itemize}
All these fit into the pattern outlined in B2 slide 5.
There are many ways to write the conclusion (or use a declaration as in the slides).
The shortest (but I don't recommend) is just saying, ``This completes the (mathematical) induction.''


\section{Basis}
You need to read carefully the question to determine the starting value for the basis (not \emph{basic}) step.
In the previous example, the definition starts with $T_{1}$ (hence the ugly $ \mathbb{N}\setminus\{ 1 \}$ for $T_{r}$, which is correct).

The formula is actually true for $r = 0$: for a merge sort with $0$ stages, the sequence has length $n = 2^{0} = 1$, which is a singleton, hence no need to transfer!
Thus the example could have been given as:

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 8);
\node at (7.8,7) {$
\textbf{Claim}: \forall{r} \in \mathbb{N}^{*} = \mathbb{N} \cup \{ 0 \}\;\; T_{r} = r 2^{r} \qquad\text{ where }
\begin{cases}
      T_{0} = 0\\
      T_{r} = 2 T_{r-1} + 2^{r}\;\; \forall r \in \mathbb{N}
    \end{cases}
$};
\node at (1,6) {$\textbf{Proof}: $};
\end{tikzpicture}

\noindent
Rework the whole inductive proof for this claim, using a new basis step.
% Don't forget the important words!
% This is actually Assignment 4, Q2†.

\section{Feel the Pattern}
Before attempting to prove a claim by mathematical induction, you need to get a feel of the inductive pattern. This can help you to crack the inductive step.

Remember Gauss' story of adding $1$ to $100$? The pairing up of heads and tails is the feel for the following pattern of triangular numbers:

\noindent
\begin{tikzpicture}[scale=1.0] % using a scale
% Balls are numbered:
%           1           1
%           2  5        2  6
%           3  6  9     3  7  11
%          (4)(7)       4  8  12  16
%             (8)      (5)(9)(13)
%                        (10)(14)
%                            (15)
%       scheme for 3   scheme for 4
%
% #1 = placement, e.g. xshfit=2.1cm
% #2 = number of rows, e.g. 4, to determine numbering scheme
\setcounter{nodecount}{0} % count steps from 0
    \DrawTriangle[xshift=0.0cm]{1}
    \DrawTriangle[xshift=2.1cm]{2}
    \DrawTriangle[xshift=4.2cm]{3}
    \DrawTriangle[xshift=6.3cm]{4}
    \DrawTriangle[xshift=8.4cm]{5}
\end{tikzpicture}

\noindent
This pattern of triangular numbers $T_{n}$ certainly suggests the following
(imagine adding an inverted copy of the triangle):
\[
1 + 2 + 3 + \dots + n = \displaystyle{\frac{n(n+1)}{2}}
\]
But we have to establish this by a formal proof.

% A guide for induction proof
\paragraph{Theorem} The sum of consecutive natural numbers is triangular:
\begin{equation}
\label{eqn:sum-of-numbers}
\displaystyle{\sum_{j=1}^{n} j = \frac{n(n+1)}{2}}
\qquad \forall{n} \in \mathbb{N}
\end{equation}
\paragraph{Proof}
By induction on $n$.\\
Basis Step: when $n = 1$,
\[
\begin{array}{r@{\; = \;}ll}
  \displaystyle{\sum_{j=1}^{1} j} 
      & 1              & \text{by summation}\\
      & (1)(1 + 1)/2   & \text{by arithmetic}\\ 
\end{array}
\]
$\therefore$ The formula is true for $n = 1$.\\
Inductive Step: assume the formula is true for some fixed $n \in \mathbb{N}$.
\[
\begin{array}{r@{\; = \;}ll}
   \displaystyle{\sum_{j=1}^{n+1} j}
 & \displaystyle{\sum_{j=1}^{n} j} + (n+1)             & \text{by summation}\\[1.5em]
 & \displaystyle{\frac{n(n+1)}{2} + (n+1)}  & \text{by inductive assumption}\\[1em]
 & \displaystyle{(n+1)\left(\frac{n}{2} + 1\right)}   & \text{by arithmetic}\\[1em]
 & \displaystyle{\frac{(n+1)(n+1 + 1)}{2}}            & \text{by arithmetic}\\  
\end{array}
\]
Thus the formula is true for $n + 1$.\\
By mathematical induction, the formula is true for all $n \in \mathbb{N}$.
$\Box$

\noindent
The $n$-th triangular number is defined as:
$T_{n} = \displaystyle{\frac{n(n+1)}{2}}$.
Thus $\displaystyle{\sum_{j=1}^{n} j = T_{n}}$.

It is relatively easy to see from Equation~\eqref{eqn:sum-of-numbers} this sum of first $n$ even numbers:
\begin{equation}
\label{eqn:sum-of-even}
2 + 4 + 6 + 8 + \dots + (2n) = 2(1 + 2 + 3 + 4 + \dots + n) = n(n+1)
\end{equation}
With hindsight,
we can get the same result by a trick (called telescoping sum):
\[
\begin{array}{r@{\hskip 5pt}l}
  & 2 + 4 + 6 + \dots + 2n\\
= & 2\times{1} + 2\times{2} + 2\times{3} + \dots + 2\times{n}\\
= & (2 - 0)\times{1} + (3 - 1)\times{2} + (4 - 2)\times{3} + \dots + \big((n + 1) - (n-1)\big)\times{n}\\
= & - 0\times{1} + (n+1)\times{n}
  \qquad\qquad\text{after cancellation of intermediate terms}\\
= & n(n+1)\\
\end{array}
\]
Try to express this trick as a manipuation of summation (\emph{hint:} split summation):

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 10);
\node at (3,9) {$
\textbf{Theorem}: \displaystyle{\sum_{j=1}^{n} 2j = n(n+1)}
$};
\node at (5.7,7.2) {$\textbf{Proof}:\qquad
\displaystyle{\sum_{j=1}^{n} 2j = \sum_{j=1}^{n} ((j+1) - (j-1))j}
\qquad\text{by trick}
$};
\node at (3.7,5.5) {$\qquad = $};
\end{tikzpicture}

\bigskip
\noindent
\textbf{Think:}
What does this picture suggest? Can you prove your conjecture?
% Can you express similarly the sum of the first $n$ odd numbers? See videos at the end.
% or guide the inductive proof: sum of consecutive odd numbers is square.

\bigskip
\noindent
\begin{tikzpicture}[scale=1.0] % using a scale
\setcounter{nodecount}{0} % count steps from 0
    \DrawSquare[xshift=0.0cm]{1}
    \DrawSquare[xshift=2.1cm]{2}
    \DrawSquare[xshift=4.2cm]{3}
    \DrawSquare[xshift=6.3cm]{4}
    \DrawSquare[xshift=8.4cm]{5}
\end{tikzpicture}

% B2 slide 15:
% Given: c_0 = c, c_(n+1) = rc_n + d   n IN N*
% Prove: for all n IN N*, c_n = c r^n + (1-r^n)/(1-r) d.
% This is derived by pattern in B2 slide 15.

\section{Find the Pattern}
% QSB2, Q3:
% x IN Q, prove: for all n IN N*, (1+x) sum_j=0 to n (-x)^j = 1 - (-x)^(n+1)
% Do a few checks first, get the pattern, then apply mathematical induction.
In Question Sets for B2 (Prep05.pdf), Q3 is this (slightly modified):
\[
\text{Let }x \in \mathbb{Q}.
\text{ Prove that } \forall n \in \mathbb{N}^{*}
\qquad \displaystyle{(1 + x)\sum_{j=0}^{n} (-x)^{j} = 1 - (-x)^{n+1}}.
\qquad\qquad\quad
\]
To find the pattern, check the successive values in $\mathbb{N}^{*}$:
\[
\begin{array}{rll}
n  &  \text{left-hand side}  & \text{right-hand side}\\
\hline
0  &  (1 + x)  & 1 - (-x) \\
1  &  (1 + x)(1 - x) & 1 - (-x)^{2} = 1 - x^{2}\\
2  &  (1 + x)(1 - x + x^{2}) & 1 - (-x)^{3} = 1 + x^{3}\\
3  &  (1 + x)(1 - x + x^{2} - x^{3}) & 1 - (-x)^{4} = 1 - x^{4}\\
\end{array}
\]
If we multiply the polynomials on the left-hand side, for $n = 4$, we have:
\[
\begin{array}{r@{\hskip 5pt}l}
    & (1 + x)(1 - x + x^{2} - x^{3} + x^{4})\\
  = & \;\;\quad\qquad  1 - x + x^{2} - x^{3} + x^{4}\\
    & \;\qquad\qquad +\; x - x^{2} + x^{3} - x^{4} + x^{5}\\
  = & 1 + x^{5}  \qquad\qquad\text{after cancellation of intermediate terms}\\  
\end{array}
\]
\textbf{Exercise:}
You can try a proof by mathematical induction, as Q3 states,
or supply a proof by summation manipulation, based on the above observation.


\section{Fighting Virus}
% QSB2, Q5: is better
% Can give answer to (a) as explanation of the pair of equations.
% (b) is simple substitution.
% (c) can be simplified by α β, or h k.
% (d) compare growth of 2^n and 3^n.
% derive a similar formula for antibodies?
If you catch a virus (hopefully not the COVID-19!), your immune system develops antibodies to kill it. However, it takes time for the antibody development, and meanwhile the virus reproduces. Therefore, it is a battle of numbers: if there are more antibodies than viruses, you body wins.

Let time be represented in steps of $n \in \mathbb{N}^{*}$, and $a_{n} = $ number of antibodies, and $v_{n} = $ number of viruses. Thus $a_{0}$ and $v_{0}$ are the initial numbers, presumably positive. The following is a model for the battle (Question Set for B2, Q5):
\[
\begin{array}{l}
   a_{n+1} = a_{n} + 2 v_{n}
     \qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad(3) \\
   \qquad \text{At each time step, antibodies increases by twice the amount of viruses.}\\
   v_{n+1} = 4 v_{n} - a_{n}  
     \qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad(4) \\
   \qquad \text{At each time step, virus quadruples, but some are killed by antibodies.}\\
\end{array}
\]
Using JavaScript Shell 1.4: \url{https://www.squarefree.com/shell/shell.html},
you can copy and paste the following:
\begin{verbatim}
// The battle with virus,
// with initial antibodies a0, viruses v0, for n time steps.
function battle(a0, v0, n) {
    var a = [a0];
    var v = [v0];
    var j = 0;
    while (j <= n) {
        print("a["+j+"] = "+a[j]+", v["+j+"] = "+v[j]);
        a[j+1] = a[j] + 2 * v[j];
        v[j+1] = 4 * v[j] - a[j];
        j = j + 1;
    }
}
// run as:
battle(2,3,10); // for a0 = 2, v0 = 3, 10 time steps.
battle(3,2,10); // for a0 = 3, v0 = 2, 10 time steps.
\end{verbatim}
You can show the following (first one is Q5(b)):

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 6);
\node at (7,5.5) {$
\textbf{Show}: v_{n+2} = 5 v_{n+1} - 6 v_{n} \qquad\forall n \in \mathbb{N}^{*}
\qquad\qquad\qquad\qquad\qquad\qquad(5) $};
\node at (4.5,4.5) {$
\textbf{Proof}: v_{n+2} = 4 v_{n+1} - a_{n+1} \qquad\text{by definition}
$};
\node at (2.4,3.6) {$\qquad = $};
\end{tikzpicture}
% v_{n+2} = 4 v_{n+1} - a_{n+1}   by definition
%         = 4 v_{n+1} - (a_{n} + 2 v_{n})  by definition
%         = 4 v_{n+1} - (4 v_{n} - v_{n+1}) - 2 v_{n}  by definition
%         = 5 v_{n+1} - 6 v_{n}

\noindent
\begin{tikzpicture}[scale=0.8] % using a scale
% the grid
\draw[step=1, color=white!90!black] (0, 0) grid (15, 6);
\node at (7,5.5) {$
\textbf{Show}: a_{n+2} = 5 a_{n+1} - 6 a_{n} \qquad\forall n \in \mathbb{N}^{*}
\qquad\qquad\qquad\qquad\qquad\qquad(6) $};
\node at (4.5,4.5) {
$\textbf{Proof}: a_{n+2} = a_{n+1} + 2 v_{n+1} \qquad\text{by definition}
$};
\node at (2.4,3.6) {$\qquad = $};
\end{tikzpicture}
% a_{n+2} = a_{n+1} + 2 v_{n+1}    by definition
%         = a_{n+1} + 2 (4 v_{n} - a_{n})   by definition
%         = a_{n+1} + 8 (a_{n+1} - a_{n})/2 - 2 a_{n}  by definition
%         = 5 a_{n+1} - 6 a_{n}

\noindent
For Q5(c), you'll prove that:
$v_{n} = (2 v_{0} - a_{0}) 3^{n} + (a_{0} - v_{0}) 2^{n}
\quad\forall n \in \mathbb{N}^{*}$.\\
To simplify the working, you should let $\alpha = 2 v_{0} - a_{0}, \beta = a_{0} - v_{0}$,
then prove: $v_{n} = \alpha 3^{n} + \beta 2^{n} \quad\forall n \in \mathbb{N}^{*}$.

Equations (5), (6) are called difference equations (more information from Wikipedia \url{https://en.wikipedia.org/wiki/Linear_difference_equation}), a topic outside the scope of this course.
I suppose you want to know, right?
The way to solve such equations is to make an educated guess. The numerical simulation (by Javascript) shows that the growth is exponential (B2 slide 11), which looks like a geometric sequence (B2 slide 8). Maybe we can try: $v_{n} = z^{n}$ for some unknown $z$, and see what happens. Substituting this into (3), we have:

\begin{table}[h]
\begin{tabular}{rl}
       & $z^{n+2} = 5 z^{n+1} - 6 z^{n}$\\
which simplifies to & $\;\quad z^{2} - 5 z + 6 = 0$\\
which factorises to & $\; (z - 2)(z - 3) = 0$\\
with the roots      & $\qquad z = 2 \text{ or } z = 3$\\
\end{tabular}
\end{table}
\noindent
Which root to choose? We don't know, so we try both, and we use a linear combination:
$v_{n} = \alpha 3^{n} + \beta 2^{n}$, and solve for $\alpha, \beta$.
For two unknowns, we need two equations, like so:
\[
\begin{array}{rl}
n = 0: & v_{0} = \alpha 3^{0} + \beta 2^{0} = \alpha + \beta\\
n = 1: & v_{1} = \alpha 3^{1} + \beta 2^{1} = 3\alpha + 2\beta\\
\end{array}
\]
Brushing up our skills to solve simultaneous equations, we multiply the first one by $2$ and subtract from the second, giving $\alpha = v_{1} - 2v_{0}$, so $\beta = v_{0} - \alpha = 3v_{0} - v_{1}$.
Of course, $v_{1} = 4v_{0} - a_{0}$ by Equation (2), thus
$\alpha = 2v_{0} - a_{0}$, and $\beta = a_{0} - v_{0}$.

This is just a suggested solution. A formal proof by mathematical induction establishes its validity (Q5(b)). Incidentally, Equations (3) (4) can be written in matrix form:
\[
\left(\begin{array}{r} a_{n+1}\\ v_{n+1}\end{array}\right) =
\left(\begin{array}{rr} 1 & 2\\ -1 & 4\end{array}\right)
\left(\begin{array}{r} a_{n}\\ v_{n}\end{array}\right)
\]
Matrix is the topic in B3, which is not covered by the mid-term exam.

\bigskip
\noindent
\textbf{Think:}
Can you formulate and prove an explicit formula for the antibodies $a_{n}$?
% Let  a_{n} = A 3^n + B 2^n
%      a0 = A + B
%      a1 = 3A + 2B
%      A = a1 - 2a0, B = a0 - A = 3a0 - a1
% But a1 = a0 + 2v0,
%  so  A = (a0 + 2v0) - 2a0 = 2v0 - a0,
%      B = 3a0 - (a0 + 2v0) = 2a0 - 2v0 = 2(a0 - v0)
%    a_{n} = (2v0 - a0) 3^n + 2(a0 - v0) 2^n
%          = (2v0 - a0) 3^n + (a0 - v0) 2^(n+1)

\section{Complete Induction}
In B2 slides 6, 9, 14,
the Inductive Step says ``Assume the formula is true for \emph{up to and including} some fixed $n$''. This is a more general idea, and is used in the following:

\bigskip
\noindent
\textbf{Theorem:} Every number $n \in \mathbb{N} \setminus\{ 1 \}$ has a prime factor.\\
\textbf{Note:} The unit $1$ is neither a prime nor a composite, so it is excluded.\\
\textbf{Proof:} By complete induction on $n$, which starts at $n = 2$.\\
Basis step: Since $2$ is prime and has factor $2$, the assertion is true for $n = 2$.\\
Inductive step: Assume the assertion is \underline{true for up to and including} some fixed $n$.
Then $m = n+1$ is either a prime or a composite. We proceed by cases:
\begin{itemize}
\item If $m$ is prime, then $m$ has itself as a factor, so the assertion is true for $m$.
\item If $m$ is composite, then $m = a\times{b}$ with $a < m$ (also $b < m$). In other words, $a \le n$, so $a$ has a prime factor $p$ by inductive assumption. Since $p$ divides $a$ and $a$ divides $m$, $m$ has a prime factor $p$. Thus the assertion is true for $m$.
\end{itemize}
By mathematical induction, the assertion is true for all $n > 1, n \in \mathbb{N}$.
$\Box$
% Introduce complete induction here.

\bigskip
\noindent
You know long ago that $n > 1$ has a prime factor. Do you ever envision a proof?

\section{Proof by Contradiction}
In A1 logic, you learn that $p \Rightarrow q$ is equivalent to its contrapositive: $\neg q \Rightarrow \neg p$ (A1 slide 23).
Instead of proving a theorem directly, you can establish its validity by showing that its contrapositive is true. This is a \emph{proof by contradiction}.

This is a famous example of a proof by contradiction.

\bigskip
\noindent
\textbf{Theorem:} There are infinitely many primes.\\
\textbf{Note:} There are $25$ primes under $100$, but only $20$ primes between $101$ and $200$. If you keep going higher and higher, the primes get rare, but never vanish.\\
\textbf{Proof:} We shall prove by contradication.
Assume there is a final, largest prime, say $q$.
Let $\displaystyle{n = \prod_{j=1}^{q} j}$, the product of all numbers from $1$ to $q$.
Consider $m = n + 1$. Every number from $1$ to $q$ divides the product $n$, but each cannot divide $m$, as each would leave a remainder $1$.
However, $m > 1$ must have a prime factor $p$ (we just proved this theorem, remember?).
Since $p$ divides $m$, $p$ cannot be any number from $1$ to $q$.
Therefore $p > q$, but this contradicts the assumption that $q$ is the largest prime.
Thus the assumption is false, and the theorem is true.
$\Box$

\newpage
To prove something wrong, you'll need a counter-example.
If the counter-example leads to contradiction, then the counter-example cannot exist.
Without any counter-example, the theorem must be true.
This is the idea and logic behind a proof by contradiction.
The mathematician G. H. Hardy (1877-1947) once remarked:

\begin{quote}
\begin{itshape}
[Proof by contradiction] is a far finer gambit than any chess gambit:
a chess player may offer the sacrifice of a pawn or even a piece,
but a mathematician offers the game.
\end{itshape}
\end{quote}

\section{Proof without words}
The renowned recreational mathematics columnist, Martin Gardner (1914-2010), pointed out that:

\begin{quote}
\begin{itshape}
In many cases a dull proof can be supplemented by\\
a geometric analogue so simple and beautiful that\\
the truth of a theorem is almost seen at a glance.
\end{itshape}
\end{quote}

\noindent
There are visual proofs, without words, as the following videos and links show.
Check the notation in the presentation: they can be wrong by the standard of our math course!

\begin{enumerate}
\item Sum of $n$ natural numbers - Visual proof\\
\url{https://www.youtube.com/watch?v=OJ_3QK7kck8} (2:10)\\
\begin{tabular}{l}
$1 + 2 + 3 + \dots + n = (n^{2} + n)/2$
\end{tabular}
\item Sum of $n$ even number - Visual proof\\
\url{https://www.youtube.com/watch?v=y6B_W22FKl4} (1:39)\\
\begin{tabular}{l}
$2 + 4 + 6 + \dots + 2n = n(n + 1)$
\end{tabular}
\item Sum of $n$ odd numbers - Visual proof\\
\url{https://www.youtube.com/watch?v=9YpnoRkr4oQ} (2:16)\\
\begin{tabular}{l}
$1 + 3 + 5 + \dots + (2n-1) = n^{2}$
\end{tabular}
\item Nicomachus's theorem (Visualisation, 3D animation)\\
\url{https://www.youtube.com/watch?v=glearwgR1Ls} (1:37)\\
\begin{tabular}{l}
$1^{3} + 2^{3} + 3^{3} + \dots + n^{3} = (1 + 2 + 3 + \dots + n)^{2}$
\end{tabular}
\item The Sum of Cubes and the Triangle Numbers | Animated Proof\\
\url{https://www.youtube.com/watch?v=6kvfvpatnn8} (6:19)\\
with cc (closed caption = subtitle on/off), about the same identity:\\
\begin{tabular}{l}
$1^{3} + 2^{3} + 3^{3} + \dots + n^{3} = (1 + 2 + 3 + \dots + n)^{2}$
\end{tabular}
\item Understanding some proofs-without-words\\
\url{https://math.stackexchange.com/questions/3081649/}\\
A discussion of various visual proofs, including this identity:\\
\begin{tabular}{l}
$3(1^{2} + 2^{2} + 3^{2} + \dots + n^{2}) = (2n + 1)(1 + 2 + 3 + \dots + n)$
\end{tabular}
\end{enumerate}

\noindent
Bear in mind that visual proofs are intended for understanding and exploration.
They are not accepted in assignments or examinations!

\end{document}

Extra: more visual proofs
=========================

Visual proofs of sums of natural numbers, squares and cubes
https://www.geogebra.org/m/Ka5xBBkH

On Proofs Without Words (Robin L. Miller, Whitman College, May 14th, 2012)
https://www.whitman.edu/Documents/Academics/Mathematics/Miller.pdf
a discussion about the philosophy behind.

Five Martin Gardner eye-openers involving squares and cubes
by Colm Mulcahy, submitted by Marianne on October 20, 2014
https://plus.maths.org/content/five-martin-gardner-eye-openers-involving-squares-and-cubes
quite amusing.

Proofs without words: Exercises in Visual Thinking
https://is.muni.cz/el/1441/podzim2013/MA2MP_SMR2/um/Nelsen--Proofs_without_Words.pdf
Whole book V1, no cover. There are V2 and V3.

The \emph{Mathematics Magazine} 
As noted by Professor Lynn Arthur Steen, co-editor of Mathematics Magazine when
PWWs began appearing,
For most people, visual memory is more powerful than linear memory of steps
in a proof. Morever, the various relationships embedded on a good diagram
represent real mathematics awaiting recognition and verbalization. So as a
device to help students learn and remember mathematics, proofs without words
are often more accurate than (mis-remembered) proofs with words[21].


% pdflatex info03.tex