\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes, shapes.gates.logic.US, calc, fit, positioning}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

\title{Assignment Topic: Relation, Function, Diagram, Graph, and Circuit}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Definitions}
Sometimes, you need to distill ideas from lecture slides.
\begin{itemize}
\item A \textbf{relation} is a set of ordered pairs (this is simpler, but equivalent, to say that a relation is a subset of $A \times B$, $A3$ slide $2$).
\item A \textbf{function} is a well-defined relation (well-defined captures what is said in $A3$ slide $7$).
\item An arrow \textbf{diagram} can represent a (small) relation or function on $A \times B$ ($A3$ slide $3$).
\item A directed \textbf{graph} is suitable for a relation or function on $A$ (that is, $A \times A$, $A3$ slide $5$).
\item A digital \textbf{circuit} consists of gates with logic inputs giving logic outputs ($A1$ slides from $36$).
\end{itemize}

\section{Pictures}
Diagram and graph are pictures of a relation or function.
A relation is just a set of ordered pairs, elements of $A \times B$, the Cartesian product of sets $A$, $B$.
Taking the last sentence of $A2$ slide $20$,
I draw a grid, with $(x,y)$ representing $xRy$ :

\begin{tikzpicture}[scale=1/2]
\tikzstyle{dot}=[circle,fill=black,minimum size=5pt,inner sep=0pt]
\tikzset{edge/.style = {->,> = latex',semithick}}
% the grid
\draw[step=1, color=gray] (0, 0) grid (3, 3);
% the labels
\node[anchor=west] at (3,0) {\small{$x \in A$}};
\node[anchor=south] at (0,3) {\small{$y \in B$}};
% the node
\node[dot] at (2,2) {};
\node[anchor=south] at (2,2) {$(x,y)$};

\node[draw] at (1,-1) {as grid};

% sets A & B
  \draw (7,1.5) node[ellipse, minimum height=40pt,minimum width=30pt,draw,label=above:$A$]{};
  \draw (10,1.5) node[ellipse, minimum height=40pt,minimum width=30pt,draw,label=above:$B$]{};
% nodes x, y
  \node (x) [dot,label=below:$x$] at (7,2) {};
  \node (y) [dot,label=above:$y$] at (10,1) {};
% arrow
  \draw [->, shorten >=2pt] (x) -- (y);

\node[draw] at (9,-1) {as arrow diagram};

% explain
  \node (x1) [dot,label=above:$x$] at (15,1) {};
  \node (y1) [dot,label=above:$y$] at (18,1) {};
  \draw[edge] (x1) to[bend left] (y1);

\node[draw,text width=3cm] at (17,-1) {as directed graph, assume $B = A$};

\end{tikzpicture}

\section{Example}
It's exam again. This time we meet Friday assignment $2, Q5$.
\begin{enumerate}[label=(\alph*)]
\item $x\; R_{1}\; y \Leftrightarrow x\mbox{ is between $5^{\circ}$ and $70^{\circ}$ North \textbf{of} }y$
\item $x\; R_{2}\; y \Leftrightarrow y\mbox{ is closest neighbour \textbf{to} }x$
\item $x\; R_{3}\; y \Leftrightarrow x\mbox{ is West \textbf{of} }y$
\end{enumerate}
Note the letter after \emph{of} or \emph{to}: that can be $x$ or $y$.
The arrow always goes from $x$ to $y$, but the relation description may not be in that order.
Depending on the problem, we investigate ``what is $y$ to a given $x$'' or ``what is $x$ of a given $y$''. We need the correct $(x,y)$ to get the node, or mark the arrow.

Let's pick $R_{2}$. Here $x, y$ are one of the cities $B, C, R, V$.
To get the picture, we ask: take $x = C$, what is $y$ closest to Canberra? That's Beijing, so $y = B$, or $(C,B) \in R_{2}$. In this case there is one answer (being the closest but not itself). A general relation may give more $y$'s, each $(C, y)$ gives an arrow from $C$ to $y$.

By going through all the possible $x$'s, $x = B, C, R, V$, we get this picture:

\begin{tikzpicture}[scale=1/2]
\tikzstyle{dot}=[circle,fill=black,minimum size=5pt,inner sep=0pt]
% the grid
\draw[step=1, color=gray] (0, 0) grid (3, 3);
% the labels
\node[anchor=east]  at (0,0) {\small{$B$}};
\node[anchor=east]  at (0,1) {\small{$C$}};
\node[anchor=east]  at (0,2) {\small{$R$}};
\node[anchor=east]  at (0,3) {\small{$V$}};
\node[anchor=north] at (0,0) {\small{$B$}};
\node[anchor=north] at (1,0) {\small{$C$}};
\node[anchor=north] at (2,0) {\small{$R$}};
\node[anchor=north] at (3,0) {\small{$V$}};
\node[anchor=west] at (3,0) {\small{$\rightarrow x$}};
\node[anchor=south] at (0,3) {\small{$\uparrow y$}};
% the node
\node[dot] at (0,3) {};  % (B,V)
\node[dot] at (1,0) {};  % (C,B)
\node[dot] at (2,3) {};  % (R,V)
\node[dot] at (3,0) {};  % (V,B)
% the text
\node[] at (12,2) {$R_{2} = \{ (B,V), (C,B), (R,V), (V,B) \}$};
\end{tikzpicture}

You can make a diagram or a graph from this. The question asks for a graph.
This relation $R_{2}$ is a function: every city as a well-defined closest neighbour.
However, this function is neither \emph{injective} (some $x$'s have the same $y$)
nor \emph{surjective} (some $y$ has no $x$). 

For the other relations, ask yourself:
\begin{itemize}
\item $R_{1}$, which $x$ is North of $y = C$, say. Each gives $(x,C)$, arrow $x \rightarrow C$.
\item $R_{3}$, which $x$ is West of $y = C$, say. Each gives $(x,C)$, arrow $x \rightarrow C$.
\end{itemize}
It helps to arrange cities by latitude for $R_{1}$,
and arrange by longitude for $R_{3}$.
Better, sketch a (world) map, and remember that the world is round!

\section{Patterns}
This Assignment $2, Q5$ is similar to that in Worksheet $2, Q5$.
I have shown the directed graphs when we discussed the solution.
Here are the grids:

\noindent
\begin{tikzpicture}[scale=1/2]
\tikzstyle{dot}=[circle,fill=black,minimum size=5pt,inner sep=0pt]
% the grid
\draw[step=1, color=gray] (0, 0) grid (3, 3);
% the labels
\node[anchor=east]  at (0,0) {\small{$A$}};
\node[anchor=east]  at (0,1) {\small{$C$}};
\node[anchor=east]  at (0,2) {\small{$M$}};
\node[anchor=east]  at (0,3) {\small{$W$}};
\node[anchor=north] at (0,0) {\small{$A$}};
\node[anchor=north] at (1,0) {\small{$C$}};
\node[anchor=north] at (2,0) {\small{$M$}};
\node[anchor=north] at (3,0) {\small{$W$}};
\node[anchor=west] at (3,0) {\small{$x$}};
\node[anchor=south] at (0,3) {\small{$y$}};
% the nodes
\node[dot] at (0,1) {}; % (A,C)
\node[dot] at (1,0) {}; % (C,A)
\node[dot] at (0,2) {}; % (A,M)
\node[dot] at (2,0) {}; % (M,A)
\node[dot] at (0,3) {}; % (A,W)
\node[dot] at (3,0) {}; % (W,A)
\node[dot] at (1,3) {}; % (C,W)
\node[dot] at (3,1) {}; % (W,C)
% the caption
\node[] at (1,-1.5) {\small{$(a)\; xR_{1}y \Leftrightarrow x\; \mbox{is Far from}\; y$}};

% another grid
\draw[step=1, color=gray] (8, 0) grid (11, 3);
% the labels
\node[anchor=east]  at (8,0) {\small{$A$}};
\node[anchor=east]  at (8,1) {\small{$C$}};
\node[anchor=east]  at (8,2) {\small{$M$}};
\node[anchor=east]  at (8,3) {\small{$W$}};
\node[anchor=north] at (8,0) {\small{$A$}};
\node[anchor=north] at (9,0) {\small{$C$}};
\node[anchor=north] at (10,0) {\small{$M$}};
\node[anchor=north] at (11,0) {\small{$W$}};
\node[anchor=west] at (11,0) {\small{$x$}};
\node[anchor=south] at (8,3) {\small{$y$}};
% the nodes
\node[dot] at (8,1) {}; % (A,C)
\node[dot] at (8,2) {}; % (A,M)
\node[dot] at (8,3) {}; % (A,W)
\node[dot] at (9,2) {}; % (C,M)
\node[dot] at (9,3) {}; % (C,W)
\node[dot] at (10,3) {}; % (M,W)
% the caption
\node[] at (9,-1.5) {\small{$(b)\; xR_{2}y \Leftrightarrow x\; \mbox{is South of}\; y$}};

% another grid
\draw[step=1, color=gray] (16, 0) grid (19, 3);
% the labels
\node[anchor=east]  at (16,0) {\small{$A$}};
\node[anchor=east]  at (16,1) {\small{$C$}};
\node[anchor=east]  at (16,2) {\small{$M$}};
\node[anchor=east]  at (16,3) {\small{$W$}};
\node[anchor=north] at (16,0) {\small{$A$}};
\node[anchor=north] at (17,0) {\small{$C$}};
\node[anchor=north] at (18,0) {\small{$M$}};
\node[anchor=north] at (19,0) {\small{$W$}};
\node[anchor=west] at (19,0) {\small{$x$}};
\node[anchor=south] at (16,3) {\small{$y$}};
% the nodes
\node[dot] at (16,1) {}; % (A,C)
\node[dot] at (19,0) {}; % (W,A)
\node[dot] at (18,3) {}; % (M,W)
\node[dot] at (17,2) {}; % (C,M)
% the caption
\node[] at (17,-1.5) {\small{$(c)\; xR_{3}y \Leftrightarrow x\; \mbox{is East of}\; y$}};

\end{tikzpicture}

You can recognize these patterns:
\begin{enumerate}[label=(\alph*)]
\item $R_{1}$ is a \textbf{symmetric} relation: $\forall{x,y}\; xRy \Rightarrow yRx$.
\item $R_{2}$ is an \textbf{antisymmetric} relation: $\forall{x,y}\; xRy \wedge yRx \Rightarrow x=y$.
\item $R_{3}$ is a function, both \emph{injective} (one-to-one) and \emph{surjective} (onto).
\end{enumerate}
It's pretty obvious that grid $(a)$ is symmetric.
If you think $(b)$ is antisymmetric due to its nodes being all on one side, you haven't dig into the definitions.

Symmetry is based on a mirror, in this case the diagonal $x = y$.
In $(a)$, the definition says, ``symmetric relation has all mirror pairs.''.
This means: if you find $(x,y)$, you'll always find $(y,x)$.
In $(b)$, the definition says, ``antisymmetric relation has no mirror pairs (unless on the mirror)''.
This means: if you find $(x,y)$, you don't find $(y,x)$ when $x \ne y$.
This makes the conjunction in the antisymmetric definition false, hence the implication is true (see $A1$ slide $20$).
Therefore the following patterns are also antisymmetric:

\begin{tikzpicture}[scale=1/2]
\tikzstyle{dot}=[circle,fill=black,minimum size=5pt,inner sep=0pt]
% the grid
\draw[step=1, color=gray] (0, 0) grid (3, 3);
% the nodes
\node[dot] at (0,1) {}; % (A,C)
\node[dot] at (2,0) {}; % (M,A)
\node[dot] at (0,3) {}; % (A,W)
\node[dot] at (3,1) {}; % (W,C)
\node[anchor=south] at (0,3) {};

% another grid
\draw[step=1, color=gray] (8, 0) grid (11, 3);
% the nodes
\node[dot] at (8,1) {}; % (A,C)
\node[dot] at (10,0) {}; % (M,A)
\node[dot] at (8,3) {}; % (A,W)
\node[dot] at (10,1) {}; % (M,C)
\node[dot] at (9,3) {}; % (C,W)
\node[dot] at (10,3) {}; % (M,W)
\node[dot] at (9,1) {}; % (C,C)
\node[dot] at (10,2) {}; % (M,M)
\node[dot] at (11,3) {}; % (W,W)

% another grid
\draw[step=1, color=gray] (16, 0) grid (19, 3);
% the nodes
\node[dot] at (16,1) {}; % (A,C)
\node[dot] at (19,0) {}; % (W,A)
\node[dot] at (18,3) {}; % (M,W)
\node[dot] at (17,2) {}; % (C,M)

\end{tikzpicture}

The grid patterns carry the same information as the diagrams and graphs.
Compare with the graphs we have discussed during Workshop 2, verify that:
\begin{itemize}[label=$\diamond$]
\item a \emph{symmetric} relation has all arrows double.
\item an \emph{antisymmetric} relation has all arrows single.
\end{itemize}
Grid nodes on the diagonal $x = y$ are loops in a directed graph (see $A3$ slide $6$).
They have no effect in the definitions of symmetric or antisymmetric relations (they can be absent or present, as shown above).
Therefore, a relation that is both symmetric and antisymmetric has no arrows, only loops (or vacuously true with no arrows, no loops: an \emph{empty} relation).

Try to re-draw the grids of Worksheet $2, Q5$ by putting the cities in random order, and find the same symmetric or antisymmetric pattern.

%  M   x       M x x        M x
%  W x x       W x x   x    W      x
%  A x   x x   A            A    x
%  C   x x     C   x        C  x 
%    C A W M     C A W M     C A W M

\section{Function}
A function is well-defined because given an $x$, there is only one value $y$,
making the expression $y = f(x)$ unambiguous.
The fits into the input-output picture of a function ($A3$ slide $9$), leading to:
\begin{itemize}
\item \emph{signature}: $f: A \rightarrow B$, with domain $A$ (inputs), codomain $B$ (possible outputs), and range $f(A)$ (actual outputs),
\item \emph{rule}: to get output from input: given $x$, what is $f(x)$ ($A3$ slide $11$).
\item \emph{composition}: $f \circ g$, when the output of $f$ is compatible with the input of $g$ ($A3$ slide $17$).
\end{itemize}
These terms apply to functions, not relations.
Furthermore, a function can be:
\begin{itemize}[label=$\diamond$] 
\item \emph{injective}: every $f(x)$ comes from only one $x$ ($A2$ slide $13$).
\item \emph{surjective}: every $y$ in range has some $x$ so that $y = f(x)$ ($A2$ slide $14$).
\item \emph{bijective}: both injective and surjective, hence having an \emph{inverse} function ($A2$ slide $16$).
\end{itemize}
We have classified the functions (a)(b)(c)(d) in Worksheet $2, Q6$.
Try using function grids to check those results, and find some patterns.

With function grids, you can easily see that, for $f: A \rightarrow A$ with set $A$ finite, a one-to-one function must be onto, and \emph{vice versa}.
Indeed, permutations are bijections ($B2$ slide $21$).
Can you figure out what is the inverse of, say,
$
\pi = 
 \begin{pmatrix}
    1 & 2 & 3 & 4\\
    4 & 1 & 3 & 2
 \end{pmatrix}
$ ?

\section{Circuit}
That $y = f(x)$ can be computed by electrons at lightning speed lies at the heart of modern computing technology. This is true because every computation can be broken down into steps performed by logic gates.
This marvelous concept was conceived, and proved on paper, by a very smart guy ($B1$ slide $28$).

Let's see this in action. Given $f: \mathbb{Z}_{4} \rightarrow \mathbb{Z}_{8}$, with $y = f(x) = x + 1$.
What is the circuit for this function $f$?

Note that $\mathbb{Z}_{10}$ and $\mathbb{Z}_{16}$ have been introduced in $B1$ slides $9$ and $11$. Hence:
\begin{itemize}
\item the domain $\mathbb{Z}_{4} = \{ 0,1,2,3 \} $, all the binary $2$-bits ($3 = 2^{2} - 1 = 11_{2}$).
\item the codomain $\mathbb{Z}_{8} = \{ 0, \dots, 7 \} $, all the binary $3$-bits ($7 = 2^{3} - 1 = 111_{2}$).
\end{itemize}

\paragraph{Step 1} Set up the input-output table.\\
Expressing $x = x_{1}x_{0}$ in binary, and similarly, $y = y_{2}y_{1}y_{0}$ in binary, then:
\[
\begin{array}{ll|l@{\qquad}lll|lll}
\mbox{as decimal:} &   x   & y = f(x) &
\mbox{as binary:} & x_{1} & x_{0} & y_{2} & y_{1} & y_{0}\\
\hline
   &  0   & 1   &   & 0 & 0 & 0 & 0 & 1 \\
   &  1   & 2   &   & 0 & 1 & 0 & 1 & 0 \\
   &  2   & 3   &   & 1 & 0 & 0 & 1 & 1 \\
   &  3   & 4   &   & 1 & 1 & 1 & 0 & 0 \\
\end{array}
\]

\paragraph{Step 2} Compute bit by bit.\\
Since $y = f(x)$, every bit of $y$ depends on every bit of $x$.
Looking through the $2$-input truth tables in $A1$, it is easy to discover:
\begin{itemize}[label=$\diamond$]
\item $y_{2} \equiv x_{1} \wedge x_{0}$
\item $y_{1} \equiv x_{1} \oplus x_{0}$
\item $y_{0} \equiv (\neg{x_{1}} \wedge \neg{x_{0}}) \vee (x_{1} \wedge \neg{x_{0}})
  \equiv (\neg{x_{1}} \vee x_{1}) \wedge \neg{x_{0}} \equiv \neg{x_{0}}$
\end{itemize}
You may be quick to spot $y_{0} \equiv \neg{x_{0}}$. I'm not that smart. I work it out the hard way ($A1$ slides $43, 44$).

\paragraph{Step 3} Build the circuit.\\
From the previous results of $y_{2}y_{1}y_{0}$, we need one AND-gate, one XOR-gate, and one NOT-gate.

The final circuit is completely equivalent to the function $f$.
Indeed, you can reverse these steps: given the final cirucit, what is the function $f: \mathbb{Z}_{4} \rightarrow \mathbb{Z}_{8}$ ?
This is Question Set for $B1, (QB1)\; Q6$, with solution given.

You can apply the same steps to build the circuit for Friday Assignment~$3$, $Q6$:
$f: \mathbb{Z}_{4} \rightarrow \mathbb{Z}_{4}$, $y = f(x) = (x - 1)\bmod{4}$.
The input-output table is straight-forward:
\[
\begin{array}{l|ll|l|ll|l}
x & f(x) = y \\
\hline
0 & f(0) = (0 - 1)\bmod{4} = 3\\
1 & f(1) = (1 - 1)\bmod{4} = 0\\
2 & f(2) = (2 - 1)\bmod{4} = 1\\
3 & f(3) = (3 - 1)\bmod{4} = 2\\
\end{array}
\]
Work out the bits to build a circuit with one XNOR-gate, and one NOT-gate.

It's fun: rework this $f: \mathbb{Z}_{4} \rightarrow \mathbb{Z}_{8}$ example with $f(x) = x + 2$, $f(x) = x + 4$,
even $f(x) = 2x$ --- just make sure that output is within the range $0 \dots 7$ ($3$-bits).


\section{Adders}
The previous circuit for $f(x) = x + 1$ is an example of an \emph{adder},
but just for $2$-bits ($\mathbb{Z}_{4}$) to $3$-bits ($\mathbb{Z}_{8}$).
How about more bits?
Well, we achieve this bit by bit. Let $z = x + y$.

\subsection{Half Adder}
If $x, y$ are $1$-bit ($\mathbb{Z}_{2}$), the sum $z$ is $2$-bit ($\mathbb{Z}_{4}$):
\[
\begin{array}{rr|l||ll}
x = x_{0} & y = y_{0} & z & z_{1} & z_{0} \\
\hline
0 & 0 & 0 & 0 & 0\\
0 & 1 & 1 & 0 & 1\\
1 & 0 & 1 & 0 & 1\\
1 & 1 & 2 & 1 & 0\\
\end{array}
\]
The bit $z_{1}$ is usually called the carry $c$,
with $c = z_{1} \equiv x_{0} \wedge y_{0}$, and $z_{0} \equiv x_{0} \oplus y_{0}$.
This is called a \emph{Half Adder}, with the circuit shown in $B1$ slide $18$ (or Worksheet $3,Q6$).
We can represent this by a block diagram (with input from above):

\begin{tikzpicture}
    \node[draw, minimum size=2cm] at (0,0) (a) {$HA$};
    \draw[<-] (a.115) --++(90:0.5cm) node [right] {$x_{0}$};
    \draw[<-] (a.65) --++(90:0.5cm) node [right] {$y_{0}$};
    \draw[->] (a.west) --++(180:0.5cm) node [left] {$c = z_{1}$};
    \draw[->] (a.south) --++(-90:0.5cm) node [below] {$z_{0}$};
    \draw[] (a.west) --++(180:0) node [right] {$\wedge$};
    \draw[] (a.south) --++(-90:0) node [above] {$\oplus$};
    \node[] at (0,2) {}; % dummy %
\end{tikzpicture}


\subsection{Full Adder}
With $x, y$ and carry $c$ all in $1$-bit ($\mathbb{Z}_{2}$), the sum $z = x + y + c$ is $2$-bit ($\mathbb{Z}_{4}$):
\[
\begin{array}{lll|l||ll}
x_{0} & y_{0} & c & z & z_{1} & z_{0} \\
\hline
0 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 1 & 1 & 0 & 1\\
0 & 1 & 0 & 1 & 0 & 1\\
0 & 1 & 1 & 2 & 1 & 0\\
1 & 0 & 0 & 1 & 0 & 1\\
1 & 0 & 1 & 2 & 1 & 0\\
1 & 1 & 0 & 2 & 1 & 0\\
1 & 1 & 1 & 3 & 1 & 1\\
\end{array}
\]
The bit $z_{1}$ is usually called out-carry $c_{out}$, to distinguish from in-carry $c_{in} = c$.
This is the same input-output table of $B1$ slide $19$.
To derive the circuit directly from the input-output table is fairly difficult,
so we employ a trick during the discussion of Worksheet $3, Q6$:
\begin{enumerate}[label={[\arabic*]}]  
\item First perform $x + y$ by a half-adder: input $x_{0}, y_{0}$, output $c_{1}, s_{1}$,
where $c_{1} \equiv x_{0} \wedge y_{0}$, and $s_{1} \equiv x_{0} \oplus y_{0}$.
\item Then we make use of another half-adder: input $s_{1}, c$, output $c_{2}, s_{2}$,
where $c_{2} \equiv s_{1} \wedge c$, and $s_{2} \equiv s_{1} \oplus c$.
\item We find $z_{0} \equiv s_{1}$, and work out $c_{out} \equiv c_{1} \vee c_{2}$.
Using an OR-gate, the task is complete.
\end{enumerate}
You can verify the above by extending the input-output table with columns for $c_{1}, s_{1}$, and $c_{2}, s_{2}$.

\begin{tikzpicture}
    % first half adder
    \node[draw, minimum size=1.5cm] at (0,2) (a) {\small{$HA$}};
    \draw[<-] (a.115) --++(90:1cm) node [right] {$x_{0}$};
    \draw[<-] (a.65) --++(90:1cm) node [right] {$y_{0}$};
    \draw[->] (a.south) --++(-90:0.5cm) node [right] {};
    \draw[] (a.south) --++(-90:0) node [above] {$\oplus$};
    \draw[] (a.west) --++(180:0cm) node [right] {$\wedge$};
    \draw[] (a.south) --++(-90:0.2) node [right] {$s_{1}$};
    \draw[] (a.west) --++(180:0.2) node [left] {$c_{1}$};
    \draw[->] (a.west) --++(180:0.24cm) -- (-1,0.3) -- (-1.5,0.3) node [left] {};

    % next half adder
    \node[draw, minimum size=1.5cm] at (0.5,0) (b) {\small{$HA$}};
    % \draw[<-] (b.115) --++(90:1.5cm) node [right] {$$};
    \draw[<-] (b.55) --++(90:1cm) -- (2.2,1.76) node [right] {$c_{in}$};
    \draw[->] (b.south) --++(-90:1cm) node [right] {$z_{0}$};
    \draw[] (b.south) --++(-90:0) node [above] {$\oplus$};
    \draw[] (b.west) --++(180:0) node [right] {$\wedge$};
    \draw[] (b.south) --++(-90:0.2) node [right] {$s_{2}$};
    \draw[] (b.west) --++(180:0.2) node [above] {$c_{2}$};
    \draw[->] (b.west) --++(180:1.25cm) node [left] {};

    % simulate OR gate
    \node[draw, minimum size=1cm] at (-2,0) (c) {\small{\;\; $OR$}};
    \draw[] (c.west) --++(180:0) node [right] {$\vee$};
    \draw[->] (c.west) --++(180:1cm) node [left] {$c_{out}$};

    % \node[] at (0,1.5) {}; % dummy %
    \node[draw, thick, dotted, rounded corners, inner xsep=1em, inner ysep=1em,
                fit=(a) (b) (c)] {};
\end{tikzpicture}

\subsection{Cascade Adders}
You can combine two full-adders (in block diagrams) like so:

\begin{tikzpicture}[
    fulladder/.style={draw, minimum size=1.5cm,
    label={[anchor=west]left:$c_{out}$},
    label={[anchor=east]right:$c_{in}$},
    }]

    \node[fulladder] (a) {};
    \node[fulladder, right = 1cm of a] (b) {};

    \draw[<-] (a.115) --++(90:0.5cm) node [above] {$x_{1}$};
    \draw[<-] (a.65) --++(90:0.5cm) node [above] {$y_{1}$};
    \draw[<-] (b.115) --++(90:0.5cm) node [above] {$x_{0}$};
    \draw[<-] (b.65) --++(90:0.5cm) node [above] {$y_{0}$};
    \draw[<-] (b.east) --++(0:0.5cm) node [right] {$0$};
    \draw[<-] (a.east) -- (b.west);
    \draw[->] (a.west) --++(180:0.5cm) node [left] {};
    \draw[->] (a.south) --++(-90:0.5cm) node [below] {$z_{1}$};
    \draw[->] (b.south) --++(-90:0.5cm) node [below] {$z_{0}$};

    \draw[] (0,0.2) node [above] {$FA$};
    \draw[] (2.5,0.2) node [above] {$FA$};

\end{tikzpicture}

The full-adder on the far right, with $c_{in} = 0$, is functionally just a half-adder.
You can expect this by reasoning from their block diagrams (or compare input-output tables when $c_{in} = 0$).
Thus the above combination is the same as connecting a full-adder and a half-adder: 
$\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{HA}.

This combination yields a $2$-bit adder for $z = x + y$ with final carry $c_{out}$,
where $x, y, z \in \mathbb{Z}_{4}$: $x = x_{1}x_{0}$, $y = y_{1}y_{0}$, $z = z_{1}z_{0}$ in binary.
You can verify this by constructing its input-output table.

I think you're smart enough to observe this pattern for adders ($x + y$):
\begin{itemize}[label=$\diamond$]   
\item $\leftarrow$ \framebox{HA}\; gives a $1$-bit adder, $x, y \in \mathbb{Z}_{2}, (2^{1} = 2)$,
\item $\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{HA}\; gives a $2$-bit adder, $x, y \in \mathbb{Z}_{4}, (2^{2} = 4)$,
\item $\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{HA}\; gives a $3$-bit adder, $x, y \in \mathbb{Z}_{8}, (2^{3} = 8)$,
\item $\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{FA} $\leftarrow$ \framebox{HA}\; gives a $4$-bit adder, $x, y \in \mathbb{Z}_{16}, (2^{4} = 16)$.
\end{itemize}

Compare the last result with $B1$ slide $20$!

\end{document}

% pdflatex topic02.tex