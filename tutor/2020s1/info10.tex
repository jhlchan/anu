\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams

\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
\usetikzlibrary{trees} % for tree diagram
\usetikzlibrary{automata, positioning} % for automata
\usetikzlibrary{matrix} % for matrix nodes
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/
% \usepackage{ifthen} % has \ifthenelse, but PGF Math Error: unknown o or of
\usepackage{todonotes} % has \ifthenelse !
\usepackage{titling} % for \droptitle
\usepackage[hang,flushmargin]{footmisc} % footnote no indent
\usepackage{bm}       % for bold math symbols
\DeclareSymbolFont{bbold}{U}{bbold}{m}{n}
\DeclareSymbolFontAlphabet{\mathbbold}{bbold}

% special marks
\usepackage{amssymb} % http://ctan.org/pkg/amssymb, for \because and \therefore
\usepackage{pifont}% http://ctan.org/pkg/pifont, for \ding
\newcommand{\cmark}{\ding{51}}% check tick
\newcommand{\xmark}{\ding{55}}% cross X

% oval around text
\newcommand{\ovaled}[1]{%
  \tikz[baseline=(char.base)]\node[anchor=south west, draw,rectangle, rounded corners, inner sep=2pt, minimum size=7mm,
    text height=2mm](char){\ensuremath{#1}} ;}
% circle around text
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
% \renewcommand{\theenumi}{\alph{enumi}}

% \setlength{\droptitle}{-10em}   % This is your set screw, more -ve higher
\title{Random Walk}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

% the text cross-mark
\newcommand{\txmark}{\text{\xmark}} % opposite of \checkmark, after begin{document}

The unit D3: Random walks on graphs has 25 slides. The main ideas are:
\begin{enumerate}[label=(\alph*),leftmargin=*]
\item Random Walk: apply Markov process.\\
From Graph (vertices) to Transitions (states).\\
Adjacency matrix to Transition matrix.\\
Each row becomes a probability vector.
\item PageRank: apply Random Walk.\\
From hyperlinks to graph.\\
Special case for no links (D3 slide 12).\\
Modification by damping factor (D3 slide 14).
\end{enumerate}


\section{Random Walks}
\label{sec:random-walks}
We encounter Markov Process in C3 or Worksheet 7.
In a Markov Process, there are several states.
The transition between states is given by a stochastic matrix,
and the system state is described by a probability vector.
The whole process is succinctly represented by a state-transition diagram.

For example, in an experiment with two urns and $4$ balls (Worksheet 7, Q5),

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
  shorten >=1pt,
  node distance = 1, auto]
  \node[state,accepting] (a) {$1$};
  \node[state,accepting] (b) [right = of a] {$2$};
  \node[state,accepting] (c) [right = of b] {$3$};
  \node[state,accepting] (d) [right = of c] {$4$};
  \node[state,accepting] (e) [right = of d] {$5$};
  \path[->,thick] (a) edge[bend left] node {$1$} (b)
                  (b) edge[bend left] node {$3/4$} (c)
                      edge[bend left] node {$1/4$} (a)
                  (c) edge[bend left] node {$1/2$} (b)    
                      edge[bend left] node {$1/2$} (d)
                  (d) edge[bend left] node {$3/4$} (c)
                      edge[bend left] node {$1/4$} (e)
                  (e) edge[bend left] node {$1$} (d);
\end{tikzpicture}

\noindent
In another experiment, we have $3$ urns but only $2$ balls (Worksheet 7, Q6):

\noindent
\begin{tikzpicture}[scale=0.8, % using a scale
  shorten >=1pt,
  node distance = 2, auto]
  \node[state,accepting] (a) {$1$};
  \node[state,accepting] (b) [right = of a] {$2$};
  \node[state,accepting] (c) [right = of b] {$3$};
  \path[->,thick] (a) edge[bend left] node {$1/2$} (b)
                      edge [loop above] node {$1/2$} ()
                  (b) edge[bend left] node {$1/2$} (a)
                      edge[bend left] node {$1/4$} (c)
                      edge [loop above] node {$1/4$} ()
                  (c) edge[bend left] node {$1$} (b);
\end{tikzpicture}

These are weighted digraphs with arrows denoting transition and weights for probabilities.

The idea for random walks is to turn this around:
given a directed graph $G$, treat the arrows as a walk, and assign probability weights,
this becomes a Markov process. What is the steady state vector of such a random walk?

While general Markov state transition diagrams may have loops,
for random walks on simple graphs (no loops), one must keep walking.
Thus a two-vertex walk is boring.
%   A <------> B
%     1      1
A triangle walk is better, and a square walk is common, see Section~\ref{sec:square-walks}.
In Prep12Sols.pdf, Q2, Anton the ant walk in 3D, an octahedron, see Section~\ref{sec:3d-walk}.


\subsection{Average Probability}
\label{sec:average-probability}
For a Markov process with state transition matrix $T$, the steady state vector $S$ satisfies (C3 slide 16):
\begin{equation}
\label{eqn:steady-state}
T' S = S\qquad\text{or}\qquad (T' - \mathbb{I})S = \mathbbold{0}
\end{equation}
The equivalent form can be solved by the short-cut method to eliminate a free parameter:
replacing the last row of the matrix by all $1$, and put the last entry of $\mathbbold{0}$ as $1$. This works because the steady state vector $S$ is a probability vector.

Starting with an initial probability vector $X$, we learn that $(T')^{n}X$ can converge towards the steady state vector $S$, but this is not always the case.
For example, in Worksheet 7 Q5, we find that $(T')^{k}$, the same for $T^{k}$, has the pattern of zeroes oscillating between odd and even diagonals, depending on the parity of $k$, hence there is no hope for $(T')^{k}X$ to converge if $X$ starts in a definite state, at one of the basis vectors (D3 slide 3). However, if we add the results of $T^{k}X$ for various powers $k$, sum them up and take the average, the long term average is indeed the steady state vector $S$ (D3 slide 4):
\begin{equation}
\label{eqn:steady-average}
\frac{1}{n} \sum_{k = 0}^{n-1} T^{k} X \rightarrow S  \quad\text{as }n\text{ gets large.} 
\end{equation}
As the slide points out, this is the idea for the Google algorithm: If $G$ is the Web graph, then, for some suitable transition matrix $T$, $q_{j}$ is the relative importance of the page $j$. This idea is called PageRank.

We learn how to use the short-cut method of Equation~\eqref{eqn:steady-state} to compute the steady-state vector $S$.
In most cases, we can deduce $S$ by the symmetry in the graph $G$ for the walk.
% Of course, we always verify the vector $S$ by Equation~\eqref{eqn:steady-state}.

\subsection{Square Walks}
\label{sec:square-walks}
Random walk over $K_{4}$ with probabilities in every path (QSet D3, Q1):

\bigskip
\noindent
\scalebox{1.0}{
\begin{tikzpicture}
  [ball/.style={circle,draw,minimum size=0.5em,inner sep=3pt]},
   edge/.style = {<->, > = latex',semithick}]
    \node[ball] (1) at (0,0) {1};
    \node[ball, right = 2 of 1] (2) {2};
    \node[ball, below = 2 of 1] (3) {3};
    \node[ball, below = 2 of 2] (4) {4};
% edges:
    \draw[edge] (1) to node[above right,pos=0.1] () {0.1}
                       node[above right,pos=0.6] () {0.2} (2);
    \draw[edge] (1) to node[below left,pos=0.1] () {0.1}
                       node[below left,pos=0.6] () {0.3} (3);
    \draw[edge] (1) to node[right,pos=0.1] () {0.5}
                       node[right,pos=0.7] () {0.5} (4);
    \draw[edge] (2) to node[left,pos=0.1] () {0.5}
                       node[left,pos=0.7] () {0.5} (3);
    \draw[edge] (2) to node[below right,pos=0.1] () {0.2}
                       node[below right,pos=0.6] () {0.4} (4);
    \draw[edge] (3) to node[below right,pos=0.1] () {0.3}
                       node[below right,pos=0.6] () {0.4} (4);
    % \draw[edge] (1) -- (2);
    % \draw[edge] (1) -- (3);
    % \draw[edge] (1) -- (4);
    % \draw[edge] (2) -- (3);
    % \draw[edge] (2) -- (4);
    % \draw[edge] (3) -- (4);
% label
    \node at (-1,0) {$K_{4}$:};
% matrix
\node at (7,-1) {$
\text{transition matrix }T = \frac{1}{10}
\left[
 \begin{array}{rrrr}
    0 & 2 & 3 & 5\\
    1 & 0 & 5 & 4\\
    1 & 5 & 0 & 4\\
    5 & 2 & 3 & 0\\
 \end{array}  
\right]
$};
\end{tikzpicture}
}

\noindent
Using a matrix calculator, we can find the probability that a walk of length $2$ that starts at vertex $j$ finishes at vertex $k$, where $j, k \in \{1, 2, 3, 4\}$:
\[
T^{2} = \frac{1}{20}
\left[
 \begin{array}{rrrr}
    6 & 5 & 5 & 4\\
    5 & 7 & 3 & 5\\
    5 & 2 & 8 & 5\\
    1 & 5 & 5 & 9\\
 \end{array}  
\right]
=
\left[
 \begin{array}{rrrr}
    0.30 & 0.25 & 0.25 & 0.20\\
    0.25 & 0.35 & 0.15 & 0.25\\
    0.25 & 0.10 & 0.40 & 0.25\\
    0.05 & 0.25 & 0.25 & 0.45\\
 \end{array}  
\right]
\]
% Wolfram Alpha:
% ((1/10) * [[0,2,3,5],[1,0,5,4],[1,5,0,4],[5,2,3,0]])^2
% 1/20 [[6,5,5,4],[5,7,3,5],[5,2,8,5],[1,5,5,9]]
and similar probabilities for a walk of length $3$:
\[
T^{3} = \frac{1}{40}
\left[
 \begin{array}{rrrr}
    6 &  9 & 11 & 14\\
    7 &  7 & 13 & 13\\
    7 & 12 &  8 & 13\\
   11 &  9 & 11 &  9\\
 \end{array}  
\right]
= 
\left[
 \begin{array}{rrrr}
    0.150 & 0.225 & 0.275 & 0.350\\
    0.175 & 0.175 & 0.325 & 0.325\\
    0.175 & 0.300 & 0.200 & 0.325\\
    0.275 & 0.225 & 0.275 & 0.225\\
 \end{array}  
\right]
\]
% Wolfram Alpha:
% ((1/10) * [[0,2,3,5],[1,0,5,4],[1,5,0,4],[5,2,3,0]])^3
% 1/40 [[6,9,11,14],[7,7,13,13],[7,12,8,13],[11,9,11,9]]
% Using Wolfram Alpha, 
We can solve for the steady state vector $S$ by short-cut method:
% \begin{verbatim}
% transpose (1/10)[[0,2,3,5],[1,0,5,4],[1,5,0,4],[5,2,3,0]]
% solve [[-10,1,1,5],[2,-10,5,2],[3,5,-10,3],[10,10,10,10]] . [s1, s2, s3, s4] = [0,0,0,10]
% \end{verbatim}
% The last command multiplies throughout by $10$ to avoid fractions. The result is:
% (T' - I) S = 0  C3 slide 31
% s1 = 1/5 and s2 = 7/30 and s3 = 4/15 and s4 = 3/10
\[
S =
\left[\begin{array}{r} 1/5\\ 7/30\\ 4/15\\ 3/10\end{array}\right]
= \frac{1}{30}
\left[\begin{array}{r} 6\\ 7\\ 8\\ 9\end{array}\right]
\]
which can be verified by $T'S = S$.


\subsection{3D Walk}
\label{sec:3d-walk}
Random walk of an ant over a regular octahedron (QSet D3, Q2):

% https://tex.stackexchange.com/questions/17204/drawing-polyhedra-using-tikz-with-semi-transparent-and-shading-effect
\definecolor{cof}{RGB}{219,144,71}
\definecolor{pur}{RGB}{186,146,162}
\definecolor{greeo}{RGB}{91,173,69}
\definecolor{greet}{RGB}{52,111,72}
\bigskip
\noindent
\begin{tikzpicture}[scale=0.8,
   edge/.style = {<->, > = latex',ultra thick}]
\coordinate (A1) at (0,0);
\coordinate (A2) at (3,1);
\coordinate (A3) at (5,0);
\coordinate (A4) at (2,-1);
\coordinate (A5) at (2.5,2.5);
\coordinate (A6) at (2.5,-2.5);
% figure
\begin{scope}[thick,dashed,,opacity=0.6]
\draw (A1) -- (A2) -- (A3);
\draw (A5) -- (A2) -- (A6);
\draw[edge] (A1) -- (A2);
\draw[edge] (A2) -- (A3);
\draw[edge] (A5) -- (A2);
\draw[edge] (A2) -- (A6);
\end{scope}
\draw[fill=cof,opacity=0.6] (A1) -- (A4) -- (A5);
\draw[fill=pur,opacity=0.6] (A1) -- (A4) -- (A6);
\draw[fill=greeo,opacity=0.6] (A3) -- (A4) -- (A5);
\draw[fill=greet,opacity=0.6] (A3) -- (A4) -- (A6);
\draw (A5) -- (A1) -- (A6) -- (A3) --cycle;
\draw[edge] (A1) -- (A4);
\draw[edge] (A4) -- (A5);
\draw[edge] (A4) -- (A6);
\draw[edge] (A3) -- (A4);
\draw[edge] (A5) -- (A1);
\draw[edge] (A1) -- (A6);
\draw[edge] (A6) -- (A3);
\draw[edge] (A3) -- (A5);
% nodes
\node[above = 0.1 of A5] (5) {1};
\node[left = 0.1 of A1] (1) {2};
\node[below left = 0.05 of A4] (4) {3};
\node[right = 0.05 of A3] (3) {4};
\node[above right = 0.05 of A2] (2) {5};
\node[below = 0.1 of A6] (6) {6};
\end{tikzpicture}

\noindent
Every direction has the same probability $p$.
Since each vertex has degree $4$, so $p = 1/4$.
By symmetry, the steady state vector has all entries equal, say $q$. For a probability vector, $6q = 1$, or $q = 1/6$.
The transition matrix is:
\[
T = \frac{1}{4}
\left[
 \begin{array}{cccccc}
    0 & 1 & 1 & 1 & 1 & 0\\
    1 & 0 & 1 & 0 & 1 & 1\\
    1 & 1 & 0 & 1 & 0 & 1\\
    1 & 0 & 1 & 0 & 1 & 1\\
    1 & 1 & 0 & 1 & 0 & 1\\
    0 & 1 & 1 & 1 & 1 & 0\\
 \end{array}  
\right],
\qquad\text{with }
S = 
\frac{1}{6}\left[\begin{array}{r} 1\\ 1\\ 1\\ 1\\ 1\\ 1\end{array}\right]
\]
which can be verified by $T'S = S$.
Of course, one can also solve for $S$ by the short-cut method using Equation~\eqref{eqn:steady-state}.


\section{PageRank}
\label{sec:pagerank}
The notion of PageRank, first introduced by Brin and Page\footnote{S. Brin and L. Page, The anatomy of a large-scale hypertextual Web search engine, Computer Networks and ISDN Systems, 30(1-7) (1998), pp. 107–117.}, forms the basis for their Web search algorithms.

\subsection{Small World}
\label{sec:small-world}
This example is taken from QSet D3, Q3. % Prep12Sols.pdf
% Tiny webgraph with 4 pages. Draw, compile, compute page ranks by symmetry (2 unknowns).
Assume the `world' has only $4$ wepages ABCD, with hyperlinks to each other, as shown.
The adjacency matrix can be constructed.

\bigskip
\noindent
\scalebox{1.0}{
\begin{tikzpicture}
  [ball/.style={circle,draw,minimum size=0.5em,inner sep=3pt]},
   edge/.style = {<->,> = latex',semithick}]
    \node[ball] (A) at (0,0) {A};
    \node[ball, right = 1 of A] (C) {C};
    \node[ball, below = 1 of C] (B) {B};
    \node[ball, below = 1 of A] (D) {D};
% edges
    \draw[edge] (A) -- (C);
    \draw[edge] (A) -- (B);
    \draw[edge] (A) -- (D);
    \draw[edge] (B) -- (D);
    \draw[edge] (B) -- (C);
% matrix
  \node at (5,-0.8)
{$
M =
\begin{array}{c|rrrr}
      & A & B & C & D\\
\hline
   A  & 0 & 1 & 1 & 1\\
   B  & 1 & 0 & 1 & 1\\
   C  & 1 & 1 & 0 & 0\\
   D  & 1 & 1 & 0 & 0\\
\end{array}
$};   
\end{tikzpicture}
}

\bigskip
\noindent
To find the PageRank of each page,
apply the usual rule to obtain transition probability (D3 slide 12):
\begin{equation}
\label{eqn:probability-links}
\text{probability for page with links} = 1/\text{(number of hyperlinks)}\\
\end{equation}
The number of hyperlinks for each page is the row sum of the adjacency matrix,
hence the transition matrix of the random walk for PageRank is:
\[ T =
\begin{array}{c|rrrr}
      & A & B & C & D\\
\hline
   A  &   0 & 1/3 & 1/3 & 1/3\\
   B  & 1/3 &   0 & 1/3 & 1/3\\
   C  & 1/2 & 1/2 &   0 & 0\\
   D  & 1/2 & 1/2 &   0 & 0\\
\end{array}
= \frac{1}{6}
\left[
 \begin{array}{cccc}
    0 & 2 & 2 & 2\\
    2 & 0 & 2 & 2\\
    3 & 3 & 0 & 0\\
    3 & 3 & 0 & 0\\
 \end{array}  
\right]
\]
The page ranks form the steady state vector $PR$, the PageRank vector.
By symmetry, pages A and B will have the same rank, say $p$,
as will pages C and D, say rank $q$.
Hence
$PR = \left[\begin{array}{r} p\\ p\\ q\\ q\end{array}\right]$
We can solve $(T' - I)PR = 0$ with shortcut to determine $p$ and $q$,
but we can also use this criterion for the steady state:
\[
\mathbb{P}(A_{\text{in}}) = \mathbb{P}(A_{\text{out}})
\Longrightarrow  p(1/3) + q(1/2) + q(1/2) = 3 p(1/3)
\Longrightarrow 2p = 3q
\]
% p/3 + q = p, p + 3q = 3p, 2p = 3q.
Since $PR$ is a probability vector, we have $2(p + q) = 1$, so $q = 1/5$ and $p = 3/10$.
% 3q + 2q = 1, 5q = 1, q = 1/5, p = 3q/2 = 1/10.
\[
PR =
\frac{1}{10}
\left[
\begin{array}{c}
A: 3\\
B: 3\\
C: 2\\
D: 2\\
\end{array}
\right] =
\left[
\begin{array}{c}
A: 0.3\\
B: 0.3\\
C: 0.2\\
D: 0.2\\
\end{array}
\right]
\]
% Wolfram Alpha -- to correct
% solve [[-6,2,3,3],[2,-6,3,3],[2,2,-6,0],[6,6,6,6]] . [s1,s2,s3,s4] = [0,0,0,6]
% s1 = 3/10 and s2 = 3/10 and s3 = 1/5 and s4 = 1/5
% s1 = 3/10 and s2 = 3/10 and s3 = 2/10 and s4 = 2/10
% 3 + 3 + 2 + 2 = 10

\subsection{Teleporting}
\label{sec:teleporting}
To compile the page rank matrix, we rely on Equation~\eqref{eqn:probability-links}.
What happens when a page has no hyperlinks?
This is the case for QSet D3, Q5, % Prep12Sols.pdf
with graph and adjacency matrix shown:

\bigskip
\noindent
\scalebox{1.0}{
\begin{tikzpicture}
  [ball/.style={circle,draw,minimum size=0.5em,inner sep=3pt]},
   edge/.style = {> = latex',semithick}]
    \node[ball] (A) at (0,0) {A};
    \node[ball, right = 1 of A] (C) {C};
    \node[ball, below = 1 of C] (B) {B};
    \node[ball, below = 1 of A] (D) {D};
% edges
    \draw[->, edge] (A) -- (C);
    \draw[<->,edge] (A) -- (B);
    \draw[<->,edge] (A) -- (D);
    \draw[<->,edge] (B) -- (D);
    \draw[->, edge] (B) -- (C);
% matrix
  \node at (5,-0.8)
{$
M =
\begin{array}{c|rrrr}
      & A & B & C & D\\
\hline
   A  & 0 & 1 & 1 & 1\\
   B  & 1 & 0 & 1 & 1\\
   C  & 0 & 0 & 0 & 0\\
   D  & 1 & 1 & 0 & 0\\
\end{array}
$};
\end{tikzpicture}
}

\noindent
Given that page C has no hyperlinks, what is the next page for someone visiting page C?
Well, this is a `world' with only $n = 4$ webpages. The visitor simply types an URL to go to anyone of these. This action is called teleporting (not following hyperlinks),
and the probability assignment rule of Equation~\eqref{eqn:probability-links} is amended (D3 slide 12):
\begin{equation}
\label{eqn:probability-no-links}
\text{probability for page with no links} =
\begin{cases}
 1/(n - 1) & \text{small }n < 10\\
 1/n       & \text{large }n \ge 10
\end{cases}
\end{equation}
where $n =$ total number of webpages, and $n \ge 10$ is large (D3 slide 20).
To keep all rows of the transition matrix a probability vector, with probabilities sum to $1$, the $1/(n-1)$ assignment excludes the diagonal entry, but the $1/n$ assignment inlcudes the diagonal entry.

\bigskip
This example has $n = 4$, so $1/(n-1) = 1/3$. Therefore, the transition matrix of the random walk for PageRank is:
\[ T =
\begin{array}{c|rrrr}
      & A & B & C & D\\
\hline
   A  &   0 & 1/3 & 1/3 & 1/3\\
   B  & 1/3 &   0 & 1/3 & 1/3\\
   C  & 1/3 & 1/3 &   0 & 1/3\\
   D  & 1/2 & 1/2 &   0 & 0\\
\end{array}
= \frac{1}{6}
\left[
 \begin{array}{cccc}
    0 & 2 & 2 & 2\\
    2 & 0 & 2 & 2\\
    2 & 2 & 0 & 2\\
    3 & 3 & 0 & 0\\
 \end{array}  
\right]
\]
The page ranks form the steady state vector $PR$.
By symmetry, pages A and B will have the same rank, say $p$,
but ranks for pages C and D are different, say $q$ and $r$ respectively.
Hence
$PR = \left[\begin{array}{r} p\\ p\\ q\\ r\end{array}\right]$
We can solve $(T' - I)PR = \mathbbold{0}$ with shortcut to determine $p, q$ and $r$,
but we can also make use of the steady state:
\[
\begin{array}{l}
\mathbb{P}(A_{\text{in}}) = \mathbb{P}(A_{\text{out}})
\Longrightarrow  p(1/3) + q(1/3) + r(1/2) = 3 p(1/3)
\Longrightarrow 4p = 2q + 3r\\
% 2p + 2q + 3r = 6p, 4p = 2q + 3r.
\mathbb{P}(C_{\text{in}}) = \mathbb{P}(C_{\text{out}})
\Longrightarrow  p(1/3) + p(1/3) = 3 q(1/3)
\Longrightarrow 2p = 3q\\
% 2p/3 = q ==> 2p = 3q.
\end{array}
\]
For the probability vector $PR$, $2p + q + r = 1$, so $p = 9/32, q = 3/16$ and $r = 1/4$.
% Since q = 2p/3, so 3r = 2p + q = 2p + 2p/3 = 8p/3 ==> r = 8p/9
% 2p + q + r = 2p + 2p/3 + 8p/9 = 1 ==>
% p(18 + 6 + 8)/9 = 1 ==> p = 9/32, q = 3/16, r = 1/4.
\[
PR =
\frac{1}{32}
\left[
\begin{array}{c}
A: 9\\
B: 9\\
C: 6\\
D: 8\\
\end{array}
\right] =
\left[
\begin{array}{c}
A: 0.28125\\
B: 0.28125\\
C: 0.18750\\
D: 0.25000\\
\end{array}
\right]
\]
% Wolfram Alpha -- to correct
% solve [[-6,2,2,3],[2,-6,2,3],[2,2,-6,0],[6,6,6,6]] . [s1,s2,s3,s4] = [0,0,0,6]
% s1 = 9/32 and s2 = 9/32 and s3 = 3/16 and s4 = 1/4
% s1 = 9/32 and s2 = 9/32 and s3 = 6/32 and s4 = 8/32
% 9 + 9 + 6 + 8 = 32

\section{Real Surfing}
\label{sec:real-surfing}
Although teleporting (ignore hyperlinks) must be used when a webpage has no hyperlinks,
some surfers on the internet will teleport even in the presence of hyperlinks.
In real-life PageRank, the probability of teleporting is denoted by $\alpha$,
and this introduces a damping factor $\beta = 1 - \alpha$ for the PageRank algorithm.

Basically, the presence of teleporting in real-life changes the assignment of probabilities for the random walk of PageRank, and modifies Equation~\eqref{eqn:probability-links} yet again (D3 slide 15):
\begin{equation}
\label{eqn:probability-links-damp}
\text{probability for page} = \alpha\left(\frac{1}{n}\right) + \beta(\text{probability for page with links})\\
\end{equation}
This gives a transition matrix (D3 slide 15):
\[
T_{\text{modified}} = \frac{\alpha}{n}\mathbb{U} + \beta{T}
\]
where $T$ is the original transition matrix, and $\mathbb{U}$ the matrix with all unity, \emph{i.e.}, all ones.
To obtain the PageRank vector $PR$, we need to solve (D3 slide 16):
\[
\begin{array}{r@{\qquad}l}
 ((T_{\text{modified}})' - \mathbb{I})PR = \mathbbold{0}  & \text{for steady state vector}\\[1em]
 \left(\displaystyle\frac{\alpha}{n}\mathbb{U} + \beta{T}' - \mathbb{I}\right)PR = \mathbbold{0}  & \text{since }\mathbb{U}' = \mathbb{U}\\[1em]
 \displaystyle\frac{\alpha}{n}\mathbb{U}(PR) = (\mathbb{I} - \beta{T}')PR  & \text{by rearrangement}\\
\end{array}
\]
Note that $\mathbb{U}(PR) = \mathbbold{1}$, a vector with all $1$'s, since the sum of entries of $PR$, a probability vector, is $1$.
Thus we have this modified equation (D3 slide 16) for PageRank with damping:
\begin{equation}
\label{eqn:pagerank-damp}
(\mathbb{I} - \beta{T}')PR = \displaystyle\frac{\alpha}{n}\mathbbold{1},
\qquad \alpha + \beta = 1.
\end{equation}
Since we have invoked the sum of $PR$ entries to be $1$, this equation must be solved by itself, without any short-cut (\emph{i.e.}, no replacing of last matrix row by $1$'s),
see Notes in D3 slide 16.

A rework of Example 1 with damping factor $\beta = 90\% = 0.9$ is given in QSet D3, Q6,
so teleport $\alpha = 1 - \beta = 0.1$.
In Worksheet 10, Q6, there is an example with the same values for damping factor and teleport.
Google is reported to take $\alpha = 0.15$, or $\beta = 85\%$ (D3 slide 14).

Note that damping factor $\beta = 100\%$ means no damping, so $80\%$ is slight damping, and $60\%$ is heavy damping.
% Prep12Sols.pdf (last part)
% As expected, the heavy damping factor of 60% has increased the lower ranks but significantly reduced the high rank of page 12.
Heavy damping increases the lower ranks but reduces the higher ranks.
Indeed, when damping factor $\beta = 1$, teleport $\alpha = 0$, that is, everybody uses hyperlinks.
In this case, Equation~\eqref{eqn:pagerank-damp} reduces to
$(\mathbb{I} - T')PR = \mathbbold{0}$,
or $(T' - \mathbb{I})PR = \mathbbold{0}$, which is
Equation~\eqref{eqn:steady-state}, the usual equation for the computation of a steady-state vector.


\subsection{Iterative Approximation}
\label{sec:iterative-approximation}
Recall from Section~\ref{sec:average-probability} that
for a Markov process with transition matrix $T$,
if we start from an initial state vector $v_{0}$,
we can compute sucessively $v_{1} = T'v_{0}$, $v_{2} = T'v_{1}$, \emph{etc}.
In favorable situations, we have:
\[
v_{n} = (T')^{n}v_{0} \rightarrow S,\text{ the steady state vector, as $n$ gets large.}
\]
Something similar happens for the PageRank vector, giving this iterative scheme (D3 slide 23):
\[
\begin{cases}
    P_{0} = \frac{1}{n}\mathbbold{1}  & \text{initial}\\
    P_{j} = T_{\text{modified}}P_{j-1} = \alpha{P_{0}} + \beta{T'}P_{j-1} & \text{for }j\in\mathbb{N}
\end{cases}
\]
Each iteration takes a weighted average of teleporting and hyperlinking, and
\[
P_{n} = ({T_{\text{modified}}}')^{n}P_{0} \rightarrow PR,\text{ the PageRank vector, as $n$ gets large.}
\]
Examples of using such iterations to find the PageRank vector can be found in Example 4C (D3 slide 24), and QSet D3, Q8 (Prep12Sols.pdf).


% Random Walk Online Simulation
% http://e.sci.osaka-cu.ac.jp/yoshino/download/rw/

% Random Walk Simulator (by David Reed)
% http://www.dave-reed.com/csc121.S16/Labs/Lab3/Walk.html
% This is a 2D walk simulator.

% PageRank and random walks on graphs (by Fan Chung and Wenbo Zhao)
% http://www.math.ucsd.edu/~fan/wp/lov.pdf

% Random Walks & Pagerank - MIT OpenCourseWareocw.mit.edu (May 18, 2015 - 20.2)
% Random Walks on Graphs
% https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/readings/MIT6_042JS15_Session35.pdf


% http://www.cs.bu.edu/~best/courses/cs109/modules/randomwalks/
% Download and run the PageRank Simulator. A zipped directory containing the program, tutorials, and examples used in this module is available here.
% This simulator requires that you have Java installed on your PC.
% http://www.cs.bu.edu/~best/courses/cs109/modules/randomwalks/code/PageRank%20Simulator/pageranksim.jar
% http://www.cs.bu.edu/~best/courses/cs109/modules/randomwalks/code/PageRank%20Simulator.zip

% Page Rank Simulator (online)
% http://faculty.chemeketa.edu/ascholer/cs160/WebApps/PageRank/

% PAGERANK SIMULATOR
% http://www.earnedmarketing.com/pagerank-simulator/
% This is a simple PageRank (Link Juice) flow simulator,

% Google: pagerank simulator
% has YouTube results too.

% The Flow of Link Juice, a Quick Guide.
% https://www.woorank.com/en/edu/seo-guides/link-juice
% has links to YouTube video.

\end{document}

\section{Some Thoughts}
\begin{itemize}
\item How many examples? Can I trace a simple/typical example?
\item How different is Random Walk different from Markov Process?
\item What is the key idea of PageRank?
\item What is the use of damping factor, and the some-name rule?
\end{itemize}

\begin{itemize}
\item D3 slide 5: Example 1 graph with 3 nodes.
\item D3 slide 6: Example 2 graph with 3 nodes.
\item D3 slide 7: Example 3 graph with 3 nodes.
\end{itemize}

\begin{itemize}
\item D3 slide 13: Example 4A graph with 4 nodes.
\item D3 slide 17: Example 4B graph with 4 nodes (4A) with dampling factor.
\item D3 slide 19: Example 5 graph with 11 nodes, iterative approximation method.
\end{itemize}

\section{More Examples}
These examples are from Prep12Sols.pdf
\begin{itemize}[leftmargin=*]
\item Q1: random walk with 4 nodes (square). List walks of length 2 and 3, verify by $T^{2}$ and $T^{3}$. Claim a formula for $S$, verify it.
\item Q2: Anton the ant, walk with 6 nodes (octahedron). List walks of length 2 and 3, again verify by $T^{2}$ and $T^{3}$. Go on with $T^{4}$, estimate $S$, verify.
\item Q3: Tiny webgraph with 4 pages. Draw, compile, compute page ranks by symmetry (2 unknowns).
\item Q4: Repeat Q3 with AB, AC removed, now 3 unknowns.
\item Q5: Repeat Q3 with CA, CB, CD? removed, now 3 unknowns, but symmetry gives only 2.
\item Q6: Repeat Q3 with 90\% damping. Symmetry still applies, 4 unknowns to only 2.
\item Q7: Repeat Q6 with 80\% damping.
\item Q8: Q7, but use 2 steps of iterative method.
\item Q9: webgraph of 12 pages, compile, without damping, and with 60\% damping. Heavy damping increases lower ranks but reduces higher ranks.
\end{itemize}

\section{P{\'o}lya and Random Walks}
\label{sec:polya-and-random-walks}
One possibly apocryphal story told about P{\'o}lya is that he really liked to walk and, while on one of his walks, kept bumping into the same couple. The couple finally got annoyed and asked him if he was stalking them. P{\'o}lya assured them he wasn't \dots and started thinking about the mathematical implications of this question. He subsequently discovered that a random walk in dimensions $1$ and $2$ --- but not higher --- is recurrent, \emph{i.e.}, keeps coming back to the same point with probability $1$.

% My Random Walks with Pólya and Szegő
% The Making of a Mathematician
% https://www.ias.edu/ideas/2014/holtz-random (use Firefox)
% By Olga Holtz · Published 2014 


% pdflatex info10.tex