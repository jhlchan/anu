\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amssymb} % for therefore
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams
% \usepackage{graphicx}
\usepackage{caption}   % for caption options
\usepackage{pgfplots}  % to plot graphs
\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\usepgfplotslibrary{fillbetween} % for filling
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
% \renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}
% \usepackage[a4paper, total={6in, 8in}]{geometry} % for margin adjustment
\usepackage{titling} % for \droptitle

% encircle text
\newcommand\encircle[1]{%
  \tikz[baseline=(X.base)] 
    \node (X) [draw, shape=circle, inner sep=0] {\strut #1};}

\setlength{\droptitle}{-7em}   % This is your set screw, more -ve higher
\title{Math Topics: Animation, Vectors and Calculus}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Animation}
\label{sec:animation}
Animation begins with drawing: say a flying dragon.
You draw using a pencil, or using a computer software with mouse controlling a cursor, even a smartphone app with your finger. The pencil tip, cursor tip, or finger tip, marks a \textbf{point}. The movement of the point traces out a \textbf{path}. Connecting the paths gives you the shape -- a dragon. Repeat the drawing, either by hand or by computer, by varying the shape: translate, rotate, enlarge, shrink, even shear or reflect, called transformtions. Finally, put the drawings into frames, and you have an animation!



% Summary of lectures
% Linear Algebra
% L1: linear systems (linear objects, solution of linear system, consistent/inconsistent)
% L2: solving linear systems (augmented matrix, row reduction)
% L3: solution of linear systems (row echelon form, parametric solution, no solution)
% L4: vectors (real number field as scalars, vector addition, scalar multiplication)
%             (unit vectors, linear combination)
% L5: homogeneous and nonhomogeneious linear systems
%             (homogeneous solution = span, particular solution for nonhomogenous)
%     applications (Leontief model with d = 0, network flow)
% L6: parametric solution (3 equations, 4 unknowns, consistency condition)
% L7: spanning set of parametric equations (vector equation of line and plane)
% L8: linear dependency and independency of a set of vectors (basis, dimension)
% L9: linear transformations (representation by matrix transformation, rotation)
% L10: linear transform of the plane (O-property, scale, shear, reflect, rotate)
% L11: applications of linear algebra (computer graphics, economics, population dynamics)
% L12: matrix operations (multiplication not commutative, example: reflect + rotate)
% L13: matrix inverse (unique, 2x2, determinant, invertibility, singular) Leontief d.ne.0
% L14: matrix inverse (by augmented inversion, elementary matrices, left = right for inv)
% L15: matrix invertibility (one-to-one, onto, check by parametric solution)
% L16: inverse characterisation and LU decomposition (triangular matrices, E1E2...A = U)
% L17: linear transform spaces (subspace, span space, column space, null space, rank, IMT)
% L18: determinants (compute, Cramer's Rule for solution, for inverse, adjugate matrix)
% L19: complex numbers (conjugate, inverse, conjugation)
% L20: complex numbers (Argand diagram, polar form, De Moivre's Theorem, factors/roots)
% L21: complex roots of unity (polar form of complex numbers)
% L22: ???
% L23: Geometric interpretation of complex numbers
%
% Calculus
% L1: background (sets, number sets, intervals, subsets, inequalities, absolute values)
% L2: functions (rule, domain, range, maximal domain, graph, types, compose, transform)
% L3: limits (definition by approach, one-sided: left, right, existence, rules, asymptote)
% L4: more limits (squeeze theorem, indeterminate forms, computational tricks)
% L5: continuity (definition by limit, discontinuity, types, continuous function, IVT)
% L6: rate of change and derivative (limit by average, secant/tangent, differentiability)
% L7: differentiation (formulae, sum, difference, product rule, quotient rule, chain rule)
% L8: implicit differentiation (related rates of change, word problems) has HEART!
% L9: linear approximation, maxima and minima (differentials, critical points, EVT)
% L10: mean value theorem, graph sketching (Rolle's theorem, mean value, sign of dy/dx)
% L11: more graph sketching (derivatives, concativity, optimisation)
% L12: antiderivative, Riemann sums, definite integral (formulae, area under curve)
% L13: definite integrals, fundamental theorem of calculus (signs, properties, traps)
% L14: function average, integration techniques (MVT for integrals, substitution, parity)
% L15: inverse function (one-to-one for inverse, derivative of inverse function: IFT)
% L16: logarithm and exponential (ln by integral, exp by inv ln, ln differentiation)
% L17: exponential and logarithm (properties, a^x, log_a x, change of base)
% L18: exponential, inverse trig, hyperbolic (growth/decay, unit circle, half exps)
% L19: indeterminate forms and L'Hopital's rule (ratio, product, difference, powers)
% L20: integration techniques (by substitution, by parts, trigo forms, )
% L21: integration techniques (by trigo forms, table of substitutions. )
% L22: integration techniques (by partial fraction. )
% L23: review of calculus topics
%

\section{Next Topics}
\label{sec:next-topics}

\section{Matrix}
\label{sec:matrix}
We may even recast~(\ref{eqn:line-vector}) as:
\begin{equation}
\label{eqn:line-matrix}
\left[\begin{array}{r}x\\y\end{array}\right] =
\left[\begin{array}{rr}2 & 3\\1 & 2\end{array}\right] 
\left[\begin{array}{r}1\\t\end{array}\right]
\end{equation}
This represents the transform of line $\left[\begin{array}{r}1\\t\end{array}\right]$
to the line $\left[\begin{array}{r}x\\y\end{array}\right]$.


\end{document}

% pdflatex topic02.tex