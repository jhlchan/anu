\documentclass[8pt,a4paper]{article}
% \documentclass[11pt,oneside,a4paper]{article}
\usepackage{hyperref}
\usepackage{amsmath} % for bmatrix
\usepackage{amssymb} % for therefore
\usepackage{amsfonts} % for mathbb
\usepackage{enumitem} % for theenumi
% \usepackage{circuitikz} % for circuit diagrams
% \usepackage{graphicx}
\usepackage{caption}   % for caption options
\usepackage{pgfplots}  % to plot graphs
\usepackage{tikz}
\usetikzlibrary{arrows, shapes.gates.logic.US, calc}
\tikzstyle{branch}=[fill, shape=circle, minimum size=3pt, inner sep=0pt]
% or, https://notgnoshi.github.io/drawing-logic-gates-with-tikz/

% default subsection numbering
% \newcommand{\thesubsection}{\thesection.\arabic{subsection}}
% remove section number in subsection numbering
\renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}
\usepackage[a4paper, total={6in, 8in}]{geometry} % for margin adjustment
\usepackage{titling} % for \droptitle

% Vector 3D plotting
\usepackage{tikz-3dplot}

% encircle text
\newcommand\encircle[1]{%
  \tikz[baseline=(X.base)] 
    \node (X) [draw, shape=circle, inner sep=0] {\strut #1};}

\setlength{\droptitle}{-7em}   % This is your set screw, more -ve higher
\title{Week 4 Hints}
\date{\vspace{-5ex}} % remove Date

\begin{document}
\maketitle

\section{Quiz}

\paragraph{Q1}
Differentiate $F$ with respect to $y$, where
$F(y) = \displaystyle{\left(\frac{1}{y^{2}} - \frac{9}{y^{4}}\right)(y + 5y^{3})}$.

\paragraph{Q2}
Let $y = \sqrt{8 + 5x}$.
\begin{enumerate}[label=\alph*)]
\item In order to conveniently apply the chain rule to compute the derivative,
write the composite function $y$ in the form $f(g(x))$
by identifying the inner function $u = g(x)$ and the outer function $y = f(u)$.
\item Find the derivative $dy/dx$.
\end{enumerate}

\paragraph{Q3}
Find $dy/dx$ by implicit differentiation: $x^{2}y^{2} + x \sin y = 5$.

\paragraph{Q4}
The length of a rectangle is increasing at a rate of $6$ cm/s
and its width is increasing at a rate of $7$ cm/s.
When the length is $5$ cm and the width is $4$ cm,
how fast is the area of the rectangle increasing?

\paragraph{Q5}
Find the linear approximation $L(x)$ of the function $f(x) = \sqrt{4 - x}$
when $x$ is near $0$ (that is, find the equation of the tangent line $L(x)$
to the graph of $f$ at $0$)
and use it to give an approximate value of the numbers $\sqrt{3.9}$ and $\sqrt{3.99}$.

\paragraph{Q6}
Find the general solution of the simple homogeneous "system" below,
which consists of a single linear equation.
\[
5x_{1} - 6x_{2} + 3x_{3} = 0
\]
Give your answer as a linear combination of vectors.
Let $x_{2}$ and $x_{3}$ be the free variables.

\paragraph{Q7}
Find the general solution of the homogeneous system below.
Give your answer as a vector. Let $x_{3}$ be a free variable.
\[
\begin{array}{rrrrrr}
     x_{1} & - & x_{2} & + & x_{3} & = 0\\
     x_{1} & + & x_{2} & + & 3x_{3} & = 0\\
           &   & 5x_{2} & + & 5x_{3} & = 0
\end{array}
\]

\paragraph{Q8}
Let $ \mathbf{A} = 
\left[
\begin{array}{rrr}
   2 & -4 & 5 \\
  -7 & 14 & 4 \\
   3 & -6 & 1
\end{array}
\right]
$ and $\mathbf{b} = \left[\begin{array}{r}5\\-39\\14\end{array}\right]$.
Describe the general solution of $\mathbf{Ax = b}$ in parametric vector form.
Use $x_{2}$ as the free variable.

\paragraph{Q9}
How many solutions does the following system have?
\[
\begin{array}{rrrrrr}
     -3x_{1} & - & 2x_{2} & - & 6x_{3} & = 1\\
      2x_{1} & + &  x_{2} & + & 3x_{3} & = 6
\end{array}
\]

\paragraph{Q10}
Suppose that
$ \mathbf{A} = 
\left[
\begin{array}{rrr}
   4 & 2 & 2 \\
  -7 & 9 & 8 \\
   6 & 3 & 8
\end{array}
\right]
$.
Determine if the columns of $\mathbf{A}$ are linearly independent.
Note that the columns of $\mathbf{A}$ are linearly independent if and only if the
equation $\mathbf{Ax = 0}$ has only the trivial solution.


\section{Worksheet}

\paragraph{1}
\textbf{Linear Algebra}
\begin{enumerate}[label=\alph*)]
\item Let
$\mathbf{v_{1}} = \left[\begin{array}{r}1\\-5\\-3\end{array}\right]$,
$\mathbf{v_{2}} = \left[\begin{array}{r}-2\\10\\6\end{array}\right]$,
$\mathbf{v_{3}} = \left[\begin{array}{r}2\\-9\\h\end{array}\right]$.
\begin{enumerate}[label=\roman*)]
\item For what values of $h$ is $\mathbf{v_{3}}$ in $\mathbf{Span\{v_{1}, v_{2}\}}$?
\item For what values of $h$ is the set $\mathbf{\{v_{1}, v_{2}, v_{3}\}}$ linearly dependent?
\end{enumerate}
\item Are the following vectors linearly independent,
\[
\left[\begin{array}{r}4\\4\end{array}\right],
\left[\begin{array}{r}-1\\3\end{array}\right],
\left[\begin{array}{r}2\\5\end{array}\right],
\left[\begin{array}{r}8\\1\end{array}\right]?
\]
Justify your answer.
\item How many pivot columns must a $5\times{7}$ matrix have
if its columns span $\mathbb{R}^{5}$? Why?
\end{enumerate}
\emph{Solution}
\begin{enumerate}[label=\alph*)]
\item If $\mathbf{v_{3}}$ is a linear combination of
$\mathbf{v_{1}}$ and $\mathbf{v_{2}}$,
then $\mathbf{v_{3}}$ is in $\mathbf{Span\{v_{1}, v_{2}\}}$,
otherwise the vectors are linearly independent.
Both concern the solution of the following matrix equation:
\[
\left[
\begin{array}{rrr}
1 & -2 & 2\\
-5 & 10 & -9\\
-3 & 6 & h
\end{array}
\right]
\left[\begin{array}{r}a\\b\\c\end{array}\right] =
\left[\begin{array}{r}0\\0\\0\end{array}\right]
\]
Using row reduction by augmented matrix,
\[
\left[
\begin{array}{rrr|r}
1 & -2 & 2 & 0\\
-5 & 10 & -9 & 0\\
-3 & 6 & h & 0
\end{array}
\right]
\underset{R_{3} + 3R_{1}}{\overset{R_{2} + 5R_{1}}{\longrightarrow}}
\left[
\begin{array}{rrr|r}
1 & -2 & 2 & 0\\
0 &  0 & 1 & 0\\
0 &  0 & 6 + h & 0
\end{array}
\right]
\]
\begin{enumerate}[label=\roman*)]
\item The solution from the augmented matrix gives $c = 0$, that is, $\mathbf{v_{3}}$ cannot be expressed as a linear combination of $\mathbf{v_{1}}$ and $\mathbf{v_{2}}$. Thus no value of $h$. In fact, $\mathbf{v_{2}} = -2 \mathbf{v_{1}}$, so they span only a line, but $\mathbf{v_{3}}$ is not lying on that line.
\item Since $\mathbf{v_{2}} = -2 \mathbf{v_{1}}$, they are linearly dependent. Thus no value of $h$ can make the three vectors linearly independent.
\end{enumerate}
\item In $\mathbf{R}^{2}$, the maximum number of linearly independent vectors is $2$.
Thus the four vectors cannot be linearly independent. In fact, solving for
\[
\left[
\begin{array}{rrrr}
4 & -1 & 2 & 8\\
4 &  3 & 5 & 1
\end{array}
\right]
\left[\begin{array}{r}a\\b\\c\\d\end{array}\right] =
\left[\begin{array}{r}0\\0\end{array}\right]
\]
Applying row reduction to augmented matrix,
\[
\left[
\begin{array}{rrrr|r}
4 & -1 & 2 & 8 & 0\\
4 &  3 & 5 & 1 & 0
\end{array}
\right]
\overset{R_{2} - R_{1}}{\longrightarrow}
\left[
\begin{array}{rrrr|r}
4 & -1 & 2 & 8 & 0\\
0 &  4 & 3 & -7 & 0
\end{array}
\right]
\underset{R_{2}/4}{\overset{R_{1}/4}{\longrightarrow}}
\left[
\begin{array}{rrrr|r}
1 & -1/4 & 1/2 & 2 & 0\\
0 &    1 & 3/4 & -7/4 & 0
\end{array}
\right]
\overset{R_{1} + R_{2}/4}{\longrightarrow}
\left[
\begin{array}{rrrr|r}
1 &    0 & 11/16 & 25/16 & 0\\
0 &    1 &   3/4 & -7/4 & 0
\end{array}
\right]
\]
This gives two free parameters $c$ and $d$, and $b = -3/4c +7/4d$, and $a = -11/16c - 25/16 d$.
Taking $c = d = 4$, $ b = -3 + 7 = 4$, and $a = (-36/16)4 = -9$.
This gives,
\[
  -9 \left[\begin{array}{r}4\\4\end{array}\right] +
   4 \left[\begin{array}{r}-1\\3\end{array}\right] +
   4 \left[\begin{array}{r}2\\5\end{array}\right] +
   4 \left[\begin{array}{r}8\\1\end{array}\right] = 
  \left[\begin{array}{r}0\\0\end{array}\right]
\]
showing that the vectors are linearly dependent.
\item For $7$ column vectors to span $\mathbb{R}^{5}$,
there must be $5$ linearly independent vectors among the seven.
That implies there are $5$ pivot columns to give $5$ coefficients.
\end{enumerate}

\paragraph{2}
\textbf{Calculus}
\begin{enumerate}[label=\alph*)]
\item The circumference of a sphere was measured to be $84$cm to the nearest centimetre.
\begin{enumerate}[label=\roman*)]
\item Use the differential approximation to estimate the maximum error in the calculated surface area. What is the relative error?
\item Use the differential approximation to estimate the maximum error in the calculated volume. What is the relative error?
\end{enumerate}
\item Use the linear approximation of the function $g(x) = \sqrt[3]{1 + x}$ at $x = 0$ to approximate the numbers $\sqrt[3]{0.95}$ and $\sqrt[3]{1.1}$.
\item A plane flying horizontally at an altitude of $1$ km and a speed of $500$ km/h passes directly over a radar station.
Find the rate at which the distance from the plane to the station is increasing when it is $2$ km away from the station.
\end{enumerate}
\emph{Solution}
\begin{enumerate}[label=\alph*)]
\item For the sphere, let $p = (84\pm{0.5})$cm be its circumference,
then its radius $r = \displaystyle\frac{p}{2\pi}$.
Its surface area $A = 4\pi r^{2} = \displaystyle\frac{p^{2}}{\pi}$, and
its volume $V = \displaystyle\frac{4}{3}\pi r^{3} = \displaystyle\frac{p^{3}}{6{\pi}^{2}}$.
\begin{enumerate}[label=\roman*)]
\item Since $\displaystyle\frac{dA}{dp} = \displaystyle\frac{d(\frac{p^{2}}{\pi})}{dp}
= \displaystyle\frac{2}{\pi}p$,
Thus maximum error in $A$ is $\displaystyle\frac{2}{\pi}{(84\text{ cm})}\lvert\pm{0.5}\text{ cm}\rvert \approx 26.8$ cm$^{2}$.
The relative error is 
$\displaystyle\frac{\Delta{A}}{A} \approx \displaystyle\frac{\frac{2}{\pi}p\Delta{p}}{\frac{p^{2}}{\pi}}
= 2\displaystyle\frac{\Delta{p}}{p} = \displaystyle\frac{1}{84} \approx 1.2\%$.
\item Since $\displaystyle\frac{dV}{dp} = \displaystyle\frac{d(\frac{p^{3}}{6{\pi}^{2}})}{dp}
= \displaystyle\frac{1}{2}{\left(\frac{p}{\pi}\right)}^{2}$,
Thus maximum error in $V$ is $\displaystyle\frac{1}{2}{\left(\frac{84\text{ cm}}{\pi}\right)}^{2}\lvert\pm{0.5}\text{ cm}\rvert \approx 178.73$ cm$^{3}$.
The relative error is 
$\displaystyle\frac{\Delta{V}}{V} \approx \displaystyle\frac{\frac{1}{2}{(\frac{p}{\pi})}^{2}\Delta{p}}{\frac{p^{3}}{6{\pi}^{2}}}
= 3\displaystyle\frac{\Delta{p}}{p} = 3\displaystyle\frac{0.5}{84} \approx 1.8\%$.
\end{enumerate}
\item Since
\[
\displaystyle{
   \left.\frac{dg}{dx}\right\rvert_{x = 0}
 = \left.\frac{d\sqrt[3]{1 + x}}{dx}\right\rvert_{x = 0}
 = \left.\frac{d((1 + x)^{\frac{1}{3}})}{dx}\right\rvert_{x = 0}
 = \left.\frac{1}{3}(1 + x)^{\frac{-2}{3}}\frac{d(1 + x)}{dx}\right\rvert_{x = 0}
 = \frac{1}{3}
}
\]
Thus
$\sqrt[3]{0.95} = \sqrt[3]{1 - 0.05} = g(-0.05) \approx g(0) + (-0.05)\left.\frac{dg}{dx}\right\rvert_{x = 0} = 1 - 0.05/3 = 0.9833$.
Using calculator, $\sqrt[3]{0.95} \approx 0.98305$.\\
Also
$\sqrt[3]{1.1} = \sqrt[3]{1 + 0.1} = g(0.1) \approx g(0) + (0.1)\left.\frac{dg}{dx}\right\rvert_{x = 0} = 1 + 0.1/3 = 1.0333$.
Using calculator, $\sqrt[3]{1.1} \approx 1.0323$.
\item Draw a picture.
%          t = 0  vt
% plane ----+----+------->  v = 500 km/h
%           |   /
%  1 km = h |  / r
%           | /
%           |/
%         radar
%
Let time $t = 0$ when the plane is directly over the radar station, at a height $h - 1$ km. At time $t$, the horizontal distance covered by the plane is $vt$, where $v = 500$ km/h.
The distance be $r$ between the plane and the radar station is
\[
    r(t) = \sqrt{h^{2} + (vt)^{2}}
\]
Squaring, and differentiate with respect to $t$,
$2r\displaystyle\frac{dr}{dt} = 2v^{2}t$, or
\[
\displaystyle{
  \frac{dr}{dt} = v^{2}\frac{t}{r}
                = \frac{v^{2}}{r}\sqrt{\frac{r^{2} - h^{2}}{v^{2}}}
                = v\sqrt{1 - \left(\frac{h}{r}\right)^{2}}
}
\]
Hence the rate of increase of $r$ is 
$500 \sqrt{1 - \left(\frac{1}{2}\right)^{2}} = 250\sqrt{3} \approx 433.01$ km/h.
\end{enumerate}

\end{document}

% pdflatex week04.tex